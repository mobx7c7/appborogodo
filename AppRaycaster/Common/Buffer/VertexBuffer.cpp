#include "VertexBuffer.h"
#include <memory>

VertexBuffer::VertexBuffer(unsigned size, unsigned stride)
	: m_buffer(new float[size])
	, m_size(size)
	, m_stride(stride)
{
	memset(m_buffer, 0, sizeof(float)*size);
}

VertexBuffer::~VertexBuffer(void)
{
	if(m_buffer)
		delete[] m_buffer;
}

unsigned VertexBuffer::Size()
{
	return m_size;
}

unsigned VertexBuffer::Stride()
{
	return m_stride;
}

float* VertexBuffer::FirstPtr()
{
	return m_buffer;
}

float* VertexBuffer::LastPtr()
{
	return m_buffer + m_size;
}
