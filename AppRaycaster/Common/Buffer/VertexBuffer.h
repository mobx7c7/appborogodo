#pragma once
#include "VertexRef.h"

class VertexBuffer
{
private:

	float*		m_buffer;
	unsigned	m_size;
	unsigned	m_stride;

public:

	VertexBuffer(unsigned, unsigned);
	~VertexBuffer();
	unsigned	Size();
	unsigned	Stride();
	float*		FirstPtr();
	float*		LastPtr();

};


