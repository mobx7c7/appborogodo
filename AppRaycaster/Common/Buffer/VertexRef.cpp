#include "VertexRef.h"
#include <memory>

/*
Vertex::Vertex()
: m_data(new float[3])
, x(_xyz[0][0])
, y(_xyz[0][1])
, z(_xyz[0][2])
{
_xyz[0] = &m_data[0];
_xyz[1] = &m_data[1];
_xyz[2] = &m_data[2];
memset(m_data, 0, 12);
m_refCount = new unsigned;
*m_refCount = 0;
}
*/

Vertex::Vertex(const Vertex& v)
{
	m_data = v.m_data;
	m_ref = v.m_ref;
	m_refCount = v.m_refCount;
	*m_refCount +=1;
}

/*
Vertex::Vertex(float x, float y, float z)
: m_data(new float[3])
, x(m_data[0])
, y(m_data[1])
, z(m_data[2])
{
m_refCount = new unsigned;
*m_refCount = 0;

this->x = x;
this->y = y;
this->z = z;
}
*/

Vertex::Vertex(float* offset, bool ref)
{
	if(m_ref)
	{
		m_data	= offset;
		m_ref = true;
	}
	else
	{
		m_data = new float[4];
		m_ref = false;
		std::copy(m_data, m_data+4, offset);
		//memcpy(m_data, offset, sizeof(float)*4);
	}
	m_refCount = new unsigned;
	*m_refCount = 0;
}

Vertex::Vertex()
{
	m_data = new float[4];
	m_ref = false;
	std::fill(m_data, m_data+4, 0);
	m_refCount = new unsigned;
	*m_refCount = 0;
}

Vertex::Vertex(float x, float y, float z, float w)
{
	m_data = new float[4];
	m_ref = false;
	m_data[0] = x;
	m_data[1] = y;
	m_data[2] = z;
	m_data[3] = w;
	m_refCount = new unsigned;
	*m_refCount = 0;
}

Vertex::~Vertex(void)
{
	if(m_refCount)
	{
		if(*m_refCount != 0)
		{
			*m_refCount -= 1;
		}
		else
		{
			if(!m_ref)
			{
				delete m_data;
			}
			delete m_refCount;
		}
	}
}

float& Vertex::X()
{
	return m_data[0];
};

float& Vertex::Y()
{
	return m_data[1];
};

float& Vertex::Z()
{
	return m_data[2];
};

float& Vertex::W()
{
	return m_data[3];
};

float& Vertex::operator[](char c)
{
	switch(c)
	{
	case 0:
	case 'x':
	case 'X':
	case 'a':
	case 'A':
		return m_data[0];
	case 1:
	case 'y':
	case 'Y':
	case 'b':
	case 'B':
		return m_data[1];
	case 2:
	case 'z':
	case 'Z':
	case 'c':
	case 'C':
		return m_data[2];
	case 3:
	case 'w':
	case 'W':
	case 'd':
	case 'D':
		return m_data[3];
	}
}

Vertex& Vertex::operator=(const Vertex& v)
{
	if(this != &v)
	{
		if(!v.m_ref)
		{
			std::copy(m_data, m_data+4, v.m_data);
		}
		else
		{
			if(m_refCount)
			{
				if(*m_refCount != 0)
				{
					*m_refCount -= 1;
				}
				else
				{
					if(!m_ref)
					{
						delete m_data;
					}
					delete m_refCount;
				}
			}

			m_data = v.m_data;
			m_ref = v.m_ref;
			m_refCount = v.m_refCount;
			*m_refCount += 1;
		}
	}
	return *this;
}

#if defined VEC_SIMD_SSE
// TODO: Implementar operadores com calculos via intrinsics
Vertex& operator+=(const Vertex& v)
{
	__m128 m1, m2, m3;
	_aligned_
		m1 = _mm_load_ps(m_data);
	m2 = _mm_load_ps(v.m_data);
	m3 = _mm_add
		/*
		asm
		{
		movupd xmm0, [x]
		movupd xmm1, [y]
		addpd xmm0, xmm1
		movupd [retval], xmm0  
		}
		*/
		return *this;
}
Vertex& operator-=(const Vertex& v)
{
	return *this;
}
#else

Vertex& operator+(Vertex& a, Vertex& b)
{
	Vertex v(a);
	v += b;
	return v;
}

Vertex& operator-(const Vertex& a, const Vertex& b)
{
	Vertex v(a);
	v -= b;
	return v;
}

Vertex& operator*(const Vertex& a, const Vertex& b)
{
	Vertex v(a);
	v *= b;
	return v;
}

Vertex& operator/(const Vertex& a, const Vertex& b)
{
	Vertex v(a);
	v /= b;
	return v;
}
/*
Vertex& Vertex::operator+(Matrix& b)
{
	Vertex v(*this);
	v += b;
	return v;
}

Vertex& Vertex::operator-(Matrix& b)
{
	Vertex v(*this);
	v -= b;
	return v;
}

Vertex& Vertex::operator*(Matrix& b)
{
	Vertex v(*this);
	v *= b;
	return v;
}

Vertex& Vertex::operator/(Matrix& b)
{
	Vertex v(*this);
	v /= b;
	return v;
}
*/
Vertex& Vertex::operator+=(const Vertex& v)
{
	m_data[0] += v.m_data[0];
	m_data[1] += v.m_data[1];
	m_data[2] += v.m_data[2];
	m_data[3] += v.m_data[3];
	return *this;
}

Vertex& Vertex::operator-=(const Vertex& v)
{
	m_data[0] -= v.m_data[0];
	m_data[1] -= v.m_data[1];
	m_data[2] -= v.m_data[2];
	m_data[3] -= v.m_data[3];
	return *this;
}

Vertex& Vertex::operator*=(const Vertex& v)
{
	m_data[0] *= v.m_data[0];
	m_data[1] *= v.m_data[1];
	m_data[2] *= v.m_data[2];
	m_data[3] *= v.m_data[3];
	return *this;
}

Vertex& Vertex::operator/=(const Vertex& v)
{
	m_data[0] /= v.m_data[0];
	m_data[1] /= v.m_data[1];
	m_data[2] /= v.m_data[2];
	m_data[3] /= v.m_data[3];
	return *this;
}

/*
Vertex& Vertex::operator+=(Matrix& m)
{
	m_data[0] += (m_data[0] * m[0][0]) + (m_data[1] * m[1][0]) + (m_data[2] * m[2][0]);
	m_data[1] += (m_data[0] * m[0][1]) + (m_data[1] * m[1][1]) + (m_data[2] * m[2][1]);
	m_data[2] += (m_data[0] * m[0][2]) + (m_data[1] * m[1][2]) + (m_data[2] * m[2][2]);
	return *this;
}

Vertex& Vertex::operator-=(Matrix& m)
{
	m_data[0] -= (m_data[0] * m[0][0]) + (m_data[1] * m[1][0]) + (m_data[2] * m[2][0]);
	m_data[1] -= (m_data[0] * m[0][1]) + (m_data[1] * m[1][1]) + (m_data[2] * m[2][1]);
	m_data[2] -= (m_data[0] * m[0][2]) + (m_data[1] * m[1][2]) + (m_data[2] * m[2][2]);
	return *this;
}

Vertex& Vertex::operator*=(Matrix& m)
{
	m_data[0] *= (m_data[0] * m[0][0]) + (m_data[1] * m[1][0]) + (m_data[2] * m[2][0]);
	m_data[1] *= (m_data[0] * m[0][1]) + (m_data[1] * m[1][1]) + (m_data[2] * m[2][1]);
	m_data[2] *= (m_data[0] * m[0][2]) + (m_data[1] * m[1][2]) + (m_data[2] * m[2][2]);
	return *this;
}

Vertex& Vertex::operator/=(Matrix& m)
{
	m_data[0] /= (m_data[0] * m[0][0]) + (m_data[1] * m[1][0]) + (m_data[2] * m[2][0]);
	m_data[1] /= (m_data[0] * m[0][1]) + (m_data[1] * m[1][1]) + (m_data[2] * m[2][1]);
	m_data[2] /= (m_data[0] * m[0][2]) + (m_data[1] * m[1][2]) + (m_data[2] * m[2][2]);
	return *this;
}
*/
#endif







VertexRef::VertexRef(float* offset)
	: Vertex(offset, true)
{
}

VertexRef::VertexRef(const VertexRef& ref)
	: Vertex(ref.m_data, true)
{
}

VertexRef::~VertexRef()
{
}
