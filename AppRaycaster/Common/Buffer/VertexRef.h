#pragma once
#ifndef VERTEXREF_H
#define VERTEXREF_H

//#define VEC_SIMD_SSE
#if defined VEC_SIMD_SSE
#include <xmmintrin.h>
#endif

class Vertex
{
protected:

	unsigned*	m_refCount;
	bool		m_ref;
	float*		m_data;
	Vertex(float* offset, bool ref);

public:
	
	Vertex(const Vertex&);
	Vertex(float x, float y, float z, float w);
	Vertex();
	~Vertex();
	float& X();
	float& Y();
	float& Z();
	float& W();

	float& operator[](char c);
	Vertex& operator=(const Vertex& v);

	friend Vertex& operator+(const Vertex& a, const Vertex& b);
	friend Vertex& operator-(const Vertex& a, const Vertex& b);
	friend Vertex& operator*(const Vertex& a, const Vertex& b);
	friend Vertex& operator/(const Vertex& a, const Vertex& b);

	/*
	Vertex& operator+(Matrix& b);
	Vertex& operator-(Matrix& b);
	Vertex& operator*(Matrix& b);
	Vertex& operator/(Matrix& b);
	*/

	Vertex& operator+=(const Vertex& v);
	Vertex& operator-=(const Vertex& v);
	Vertex& operator*=(const Vertex& v);
	Vertex& operator/=(const Vertex& v);

	/*
	Vertex& operator+=(Matrix& m);
	Vertex& operator-=(Matrix& m);
	Vertex& operator*=(Matrix& m);
	Vertex& operator/=(Matrix& m);
	*/
};

class VertexRef : public Vertex
{
public:
	VertexRef(float* offset);
	VertexRef(const VertexRef& ref);
	~VertexRef();
};

#endif // VERTEXREF_H