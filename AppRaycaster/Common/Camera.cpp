#include "Camera.h"

Camera* Camera::current = 0;

Camera::Camera(void)
: pos()
, rot()
, posx(pos[0])
, posy(pos[1])
, posz(pos[2])
, rotx(rot[0])
, roty(rot[1])
, rotz(rot[2])
{
	if(current == 0)
		current = this;

	fov = 60.0f;
	aspect = 1.0f;
	zMin = 0.001f;
	zMax = 100.0f;
}

Camera::~Camera(void)
{
	if(current == this)
		current = 0;
}

Matrix Camera::GetMatrix()
{
	Matrix m;
	Matrix::Perspective(m, fov, aspect, zMin, zMax);
	return m;
}

void Camera::SetCurrent()
{
	current = this;
}

Camera& Camera::Current()
{
	return *current;
}