#pragma once

#include "Math\Matrix.h"
#include "Math\Vec3.h"

class Camera
{
private:

	static Camera* current;

	Matrix m;
	
public:

	__Vec3__ _pos, _rot;

	float pos[3], rot[3];
	float &posx, &posy, &posz;
	float &rotx, &roty, &rotz;
	float fov, aspect, zMin, zMax;

	Camera(void);
	~Camera(void);

	Matrix GetMatrix();

	void SetCurrent();
	static Camera& Current();

};

