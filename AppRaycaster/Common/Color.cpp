#include "Color.h"
#include <memory>

Color::Color(void)
{
	m_color = 0;
}

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	SetColor(r,g,b,a);
}

Color::~Color(void)
{

}

void Color::SetColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	m_color = (r << 24) | (g << 16) | (b << 8) | a;
}

void Color::SetColor(uint32_t color)
{
	m_color = color;
}

uint32_t Color::toRGB()
{
	return m_color >> 8 & 0xFFFFFF;
}

uint32_t Color::toRGBA()
{
	return m_color;
}

uint32_t Color::toBGR()
{
	return (m_color >> 8 & 0xFF) << 16 | (m_color >> 16 & 0xFF) << 8 | (m_color >> 24 & 0xFF);
}

uint32_t Color::toBGRA()
{
	return (m_color >> 8 & 0xFF) << 16 | (m_color >> 16 & 0xFF) << 8 | (m_color >> 24 & 0xFF) | (m_color & 0xFF);
}

uint32_t Color::toARGB()
{
	return (m_color & 0xFF) << 24 | (m_color >> 24 & 0xFF) << 16 | (m_color >> 16 & 0xFF) << 8 | (m_color >> 8 & 0xFF);
}
