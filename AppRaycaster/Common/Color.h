#pragma once

#include <cstdint>

class Color
{
protected:

	unsigned m_color;

public:

	Color(void);
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
	~Color(void);
	void SetColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
	void SetColor(uint32_t color);
	uint32_t toRGB();
	uint32_t toRGBA();
	uint32_t toBGR();
	uint32_t toBGRA();
	uint32_t toARGB();

};

