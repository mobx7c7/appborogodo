#include "Framebuffer.h"
#include "..\Engine\App.h"

Framebuffer* Framebuffer::m_current = 0;

Framebuffer::Framebuffer(HWND hWnd, int width, int height)
{
	this->hWnd = hWnd;

	BITMAPINFO bmi;
	memset(&bmi, 0, sizeof(bmi));
	bmi.bmiHeader.biSize			= sizeof(BITMAPINFO);
	bmi.bmiHeader.biWidth			= width;
	bmi.bmiHeader.biHeight			= -height; // Order pixels from top to bottom
	bmi.bmiHeader.biPlanes			= 1;
	bmi.bmiHeader.biBitCount		= 32; // last byte not used, 32 bit for alignment
	bmi.bmiHeader.biCompression		= BI_RGB;

	HDC hDC = GetDC(hWnd);
	//hBm = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, &m_buffer, NULL, 0);
	hBm = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, NULL, NULL, 0);
	DeleteDC(hDC);

	GetObject(hBm, sizeof(bm), &bm);

	if(!m_current)
		SetCurrent(*this);
}

Framebuffer::~Framebuffer(void)
{
	DeleteObject(hBm);
}

void Framebuffer::SetPixel(int x, int y, int pixel)
{
	//((unsigned*)m_buffer)[y*m_width+x] = pixel;
	((unsigned*)bm.bmBits)[y*bm.bmWidth+x] = pixel;
}

void Framebuffer::GetPixel(int x, int y, int& pixel)
{
	//pixel = ((unsigned*)m_buffer)[y*m_width+x];
	pixel = ((unsigned*)bm.bmBits)[y*bm.bmWidth+x];
}

void Framebuffer::SetBuffer(const char* buffer, int size)
{
	//memcpy(m_buffer, buffer, size);
	memcpy(bm.bmBits, buffer, size);
}

void Framebuffer::GetBuffer(char* buffer, int size)
{
	//memcpy(buffer, m_buffer, size);
	memcpy(buffer, bm.bmBits, size);
}

int Framebuffer::GetWidth()
{
	// return m_width
	return bm.bmWidth;
}

int Framebuffer::GetHeight()
{
	// return m_height
	return bm.bmHeight;
}

void Framebuffer::SetCurrent(Framebuffer& fbuf)
{
	m_current = (Framebuffer*)&fbuf;
}

Framebuffer& Framebuffer::GetCurrent()
{
	return *m_current;
}


void* Framebuffer::GetData()
{
	return bm.bmBits;
}

unsigned Framebuffer::GetOffset()
{
	return bm.bmBitsPixel/8;
}

unsigned Framebuffer::GetSize()
{
	return bm.bmWidthBytes * bm.bmHeight;
}

void* Framebuffer::GetHandler()
{
	return hBm;
}