#pragma once

#include <Windows.h>

// TODO: Aceitar mais formatos (ex: RGB888, RGB111...)
// TODO: Implementar em OpenGL
// TODO: Implementar em TextMode

// NOTA: Unica implementação: Win32 GDI
// NOTA: Unico formato: 32 bits

class Framebuffer // Win32, GDI
{
protected:

	static Framebuffer* m_current;

	HWND hWnd;
	HBITMAP hBm;
	BITMAP bm;

	//void* m_buffer;
	//int m_width, m_height;
	//int m_size;

	//HBITMAP hBmpColor;
	//HBITMAP hBmpDepth;
	//void** m_buffer;

private:

	Framebuffer();
	Framebuffer(const Framebuffer&);

public:

	//void SetPixel(int x, int y, int value, int buffer);
	//void GetPixel(int x, int y, int* value, int buffer);
	//void GetData(const void* data, int size, int buffer);
	//void SetData(void* data, int size, int buffer);

	Framebuffer(HWND hWnd, int width, int height);
	~Framebuffer();

	int	GetWidth();
	int	GetHeight();
	
	//void SetFramebuffer(int w, int h);

	void SetPixel(int x, int y, int pixel);
	void GetPixel(int x, int y, int& pixel);
	void SetBuffer(const char* data, int size);
	void GetBuffer(char* data, int size);

	static void	SetCurrent(Framebuffer&);
	static void SetDefault();
	static Framebuffer& GetCurrent();

	void*		GetData();
	unsigned	GetOffset();
	unsigned	GetSize();
	void*		GetHandler(); // Win32

	// TODO: Deslocar para uma classe tipo Win32Framebuffer
};


