#include "Buffer.h"

namespace GL
{
	void Buffer::CreateObject()
	{
		glGenBuffers(1, &m_obj);
	}

	void Buffer::DeleteObject()
	{
		glDeleteBuffers(1, &m_obj);
	}

	bool Buffer::IsObject()
	{
		return glIsBuffer(m_obj);
	}

	Buffer::Buffer(GLenum target) 
		: Object(target)
	{
		CheckObject();
	}

	Buffer::~Buffer()
	{
		if(IsObject())
			DeleteObject();
	}

	void Buffer::Initialize(BufferParams& params)
	{
		Bind();
		SetBufferData(params);
		Unbind();
	}

	void Buffer::SetBufferData(BufferParams& params)
	{
		SetBufferData(params.size, params.data, params.usage);
	}

	void Buffer::SetBufferData(GLsizeiptr size, const GLvoid* data, GLenum usage)
	{
		glBufferData(m_target, size, data, usage);
	}

	void* Buffer::CreateBufferMap(BufferParams& params)
	{
		return glMapBuffer(m_target, params.access);
	}

	void Buffer::ReleaseBufferMap()
	{
		glUnmapBuffer(m_target);
	}

	void Buffer::Bind()
	{
		glBindBuffer(m_target, m_obj);
	}

	void Buffer::Unbind()
	{
		glBindBuffer(m_target, 0);
	}

}