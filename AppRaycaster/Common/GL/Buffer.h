#pragma once
#include "Object.h"

namespace GL
{
	struct BufferParams
	{
		GLenum		target;
		GLenum		usage;
		GLenum		access;
		GLsizeiptr	size;
		GLvoid*		data;
	};

	class Buffer : public Object
	{
	protected:

		void CreateObject();
		void DeleteObject();
		bool IsObject();
		
	public:

		// Op��es de "target":
		// - GL_PIXEL_PACK_BUFFER
		// - GL_PIXEL_UNPACK_BUFFER
		// - GL_ARRAY_BUFFER
		// - GL_ELEMENT_ARRAY_BUFFER
		Buffer(GLenum target);

		virtual ~Buffer();

		void	Initialize(BufferParams& params);
		
		void	SetBufferData(BufferParams& params);
		void	SetBufferData(GLsizeiptr size, const GLvoid* data, GLenum usage);

		void*	CreateBufferMap(BufferParams& params);

		void	ReleaseBufferMap();

		void	Bind();

		void	Unbind();

	};
}