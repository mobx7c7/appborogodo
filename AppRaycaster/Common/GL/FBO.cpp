#include "FBO.h"

namespace GL
{
	FBO* FBO::m_current = 0;

	void FBO::CreateObject()
	{
		glGenFramebuffers(1, &m_obj);
	}

	void FBO::DeleteObject()
	{
		glDeleteFramebuffers(1, &m_obj);
	}

	bool FBO::IsObject()
	{
		return glIsFramebuffer(m_obj);
	}

	FBO::FBO(GLenum target) 
		: Object(target)
	{
		CheckObject();
	}

	FBO::~FBO()
	{
		if(IsObject())
			DeleteObject();
	}

	void FBO::Initialize(FBOParams& params)
	{
		Bind();
		SetTex(params);
		SetRBO(params);
		Unbind();
	}

	void FBO::SetTex(FBOParams& params)
	{
		if(params.texTarget == GL_TEXTURE_2D)
			glFramebufferTexture2D(m_current->GetTarget(), params.texAttachment, params.texTarget, params.texObject, params.texLevel); // "0" � o n�vel da textura.
	}

	void FBO::SetRBO(FBOParams& params) //(GLenum attachment, GLenum target, GLuint name)
	{   
		glFramebufferRenderbuffer(m_current->GetTarget(), params.rboAttachment, params.rboTarget, params.rboObject);
	}

	void FBO::Bind()
	{
		m_current = this;
		glBindFramebuffer(m_current->GetTarget(), m_current->GetName());
	}

	void FBO::Unbind()
	{
		glBindFramebuffer(m_current->GetTarget(), 0);
		m_current = nullptr;
	}
}