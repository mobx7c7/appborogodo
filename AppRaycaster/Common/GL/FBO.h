#pragma once
#include "Object.h"

namespace GL
{
	struct FBOParams
	{
		GLuint target;
		GLuint texAttachment;
		GLuint texTarget;
		GLuint texLevel;
		GLuint texObject;
		GLuint rboAttachment;
		GLuint rboTarget;
		GLuint rboObject;
	};

	class FBO : public Object
	{
	private:

		static FBO* m_current;

	protected:

		void CreateObject();

		void DeleteObject();

		bool IsObject();

	public:

		FBO(GLenum target);

		~FBO();

		void Initialize(FBOParams& params);

		static void SetTex(FBOParams& params);

		static void SetRBO(FBOParams& params);

		void Bind();

		void Unbind();

	};
}