#include "Object.h"

namespace GL
{
	/*
	void Object::CreateObject()
	{}

	void Object::DeleteObject()
	{}

	bool Object::IsObject()
	{
		return false;
	}
	*/

	bool Object::CheckObject()
	{
		if(!IsObject())
		{
			CreateObject();
			return IsObject();
		}
		return true;
	}

	Object::Object()
		: m_target(0)
		, m_obj(0)
	{}

	Object::Object(const Object& obj)
		: m_obj(obj.m_obj)
		, m_target(obj.m_target)
	{}

	Object::Object(GLenum target) 
		: m_target(target)
		, m_obj(0)
	{}

	Object::~Object()
	{}

	GLuint Object::GetName()
	{
		return m_obj;
	}

	GLenum Object::GetTarget()
	{
		return m_target;
	}

	void Object::Bind()
	{

	}

	void Object::Unbind()
	{

	}

}