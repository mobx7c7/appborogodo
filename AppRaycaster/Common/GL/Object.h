#pragma once
//#include "..\..\Libs\glew-1.13.0\include\GL\glew.h"

//#define GL_VERSION_4_5 // for�ando para usar somente core(?) da vers�o 4.5
//#include <GL/glew.h>
#include "GL\glew.h"

//#define GLM_FORCE_RADIANS
//#include "glm/glm.hpp"
//#include "glm/gtc/matrix_transform.hpp"
//#include "glm/gtc/type_ptr.hpp"
//#include "FreeImage.h"

//#include <iostream>
//#include <sstream>
//#include <string>
//#include <vector>
//#include <fstream>

namespace GL
{
	class Object
	{
	protected:

		virtual void CreateObject() = 0;

		virtual void DeleteObject() = 0;

		virtual bool IsObject() = 0;

		virtual bool CheckObject();

	protected:

		GLuint m_obj;

		GLenum m_target;

		Object(GLenum target);

		Object();

	public:

		Object(const Object& obj);

		virtual ~Object();

		GLuint GetName();

		GLenum GetTarget();

		virtual void Bind();
		
		virtual void Unbind();

	};
}
