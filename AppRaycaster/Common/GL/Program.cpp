#include "Program.h"
#include <fstream>
#include <vector>

namespace GL
{
	struct ShaderFileToken
	{
		std::string token;
		GLenum type;
		bool loaded;
	};

	void Program::CreateObject()
	{
		m_obj = glCreateProgram();
	}

	void Program::DeleteObject()
	{
		glDeleteProgram(m_obj);
	}

	bool Program::IsObject()
	{
		return glIsProgram(m_obj);
	}

	/*
	int Program::LoadFile(Program& program, std::string file)
	{
		std::ifstream ifs(file);

		if(!ifs.is_open())
		{
			return 0;
		}

		// Extrai caminho relativo ao local do arquivo
		std::string path = "";
		{
			size_t pos = file.find_last_of("\\");
			if(pos != std::string::npos)
			{
				path = file.substr(0, pos+1);
			}
		}

		std::vector<ShaderFileToken> src_tokens =
		{
			{"vert", GL_VERTEX_SHADER, false},
			{"geom", GL_GEOMETRY_SHADER, false},
			{"frag", GL_FRAGMENT_SHADER, false},
		};

		std::string line;

		size_t pos, str_begin, str_end;

		while(std::getline(ifs, line))
		{
			pos = str_begin = str_end = 0;

			for(ShaderFileToken& src : src_tokens)
			{
				if(src.loaded)
				{
					continue;
				}

				if((pos = line.find(src.token)) != std::string::npos) // nome da variavel
				{
					if((pos = line.find("=")) != std::string::npos) // atribuicao de valor
					{
						str_begin = line.find_first_of("\""); // come�o de string
						str_end = line.find_last_of("\"");  // termino de string

						std::string shader_file = path + line.substr(str_begin+1, str_end-str_begin-1);

						//std::cout << "Carregando " << src.token << " " << shader_file << "" << std::endl;

						Shader source(Shader::FromFile(shader_file, src.type));

						if(!source.IsCompiled())
						{
							continue;
							//std::cerr << "Compile error" << std::endl;
						}

						program.AttachShader(source);
						
						src.loaded = true;

						break;
					}
				}
			}
		}

		program.Link();

		ifs.close();

		return 1;
	}
	*/

	Program::Program()
		: Object()
	{
		CheckObject();
	}

	Program::~Program()
	{
		Clear();
		if(IsObject())
			DeleteObject();
	}

	bool Program::AttachShader(Shader& src)
	{
		if(CheckObject())
		{
			glAttachShader(m_obj, src.GetName());
			return true;
		}
		return false;
	}

	bool Program::Link()
	{
		if(CheckObject()) 
		{
			glLinkProgram(m_obj);
			return IsLinked();
		}
		return false;
	}

	bool Program::IsLinked()
	{
		GLint result = 0;
		glGetProgramiv(m_obj, GL_LINK_STATUS, &result);
		return result;
	}

	void Program::Clear()
	{
		if(IsObject())
		{
			/*
			GLint currentProgram = 0;

			glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);

			if(glGetError() != GL_INVALID_VALUE && currentProgram == m_obj)
			{
				Unbind();
			}
			*/

			GLint count = 0;
			glGetProgramiv(m_obj, GL_ATTACHED_SHADERS, &count); // Query the number of attached shaders

			GLuint* names = new GLuint[count];
			glGetAttachedShaders(m_obj, count, NULL, names); // Get the shader names
			
			for(int i=0; i<count; i++) // Delete the shaders
			{
				if(glIsShader(names[i]))
				{
					glDeleteShader(names[i]);
				}
			}

			delete[] names;

			glDeleteProgram(m_obj);
		}
	}

	void Program::Bind()
	{
		glUseProgram(m_obj);
	}

	void Program::Unbind()
	{
		glUseProgram(0);
	}

	std::string Program::Log()
	{
		GLint value = 0;

		glGetProgramiv(m_obj, GL_INFO_LOG_LENGTH, &value);

		if(value > 0)
		{
			char* log = new char[value];
			GLint written = 0;
			glGetProgramInfoLog(m_obj, value, &written, log);
			std::string _log(log, written);
			delete[] log;
			return _log;
		}

		return "";
	}

	GLuint Program::FragDataLocation(std::string name)
	{
		return glGetAttribLocation(m_obj, name.c_str());
	}

	GLuint Program::AtributeLocation(std::string name)
	{
		return glGetAttribLocation(m_obj, name.c_str());
	}

	GLuint Program::UniformLocation(std::string name)
	{
		return glGetUniformLocation(m_obj, name.c_str());
	}

	void Program::Uniform1i(GLuint loc, GLint v1)
	{
		glUniform1i(loc, v1);
	}

	void Program::Uniform2i(GLuint loc, GLint v1, GLint v2)
	{
		glUniform2i(loc, v1, v2);
	}

	void Program::Uniform3i(GLuint loc, GLint v1, GLint v2, GLint v3)
	{
		glUniform3i(loc, v1, v2, v3);
	}

	void Program::Uniform4i(GLuint loc, GLint v1, GLint v2, GLint v3, GLint v4)
	{
		glUniform4i(loc, v1, v2, v3, v4);
	}

	void Program::Uniform1f(GLuint loc, GLfloat v1)
	{
		glUniform1f(loc, v1);
	}

	void Program::Uniform2f(GLuint loc, GLfloat v1, GLfloat v2)
	{
		glUniform2f(loc, v1, v2);
	}

	void Program::Uniform3f(GLuint loc, GLfloat v1, GLfloat v2, GLfloat v3)
	{
		glUniform3f(loc, v1, v2, v3);
	}

	void Program::Uniform4f(GLuint loc, GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4)
	{
		glUniform4f(loc, v1, v2, v3, v4);
	}

	void Program::Uniform1d(GLuint loc, GLdouble v1)
	{
		glUniform1d(loc, v1);
	}

	void Program::Uniform2d(GLuint loc, GLdouble v1, GLdouble v2)
	{
		glUniform2d(loc, v1, v2);
	}

	void Program::Uniform3d(GLuint loc, GLdouble v1, GLdouble v2, GLdouble v3)
	{
		glUniform3d(loc, v1, v2, v3);
	}

	void Program::Uniform4d(GLuint loc, GLdouble v1, GLdouble v2, GLdouble v3, GLdouble v4)
	{
		glUniform4d(loc, v1, v2, v3, v4);
	}

	void Program::Uniform1iv(GLuint loc, GLint count, GLint* ptr)
	{
		glUniform1iv(loc, count, ptr);
	}

	void Program::Uniform2iv(GLuint loc, GLint count, GLint* ptr)
	{
		glUniform2iv(loc, count, ptr);
	}

	void Program::Uniform3iv(GLuint loc, GLint count, GLint* ptr)
	{
		glUniform3iv(loc, count, ptr);
	}

	void Program::Uniform4iv(GLuint loc, GLint count, GLint* ptr)
	{
		glUniform4iv(loc, count, ptr);
	}

	void Program::Uniform1fv(GLuint loc, GLint count, GLfloat* ptr)
	{
		glUniform1fv(loc, count, ptr);
	}

	void Program::Uniform2fv(GLuint loc, GLint count, GLfloat* ptr)
	{
		glUniform2fv(loc, count, ptr);
	}

	void Program::Uniform3fv(GLuint loc, GLint count, GLfloat* ptr)
	{
		glUniform3fv(loc, count, ptr);
	}

	void Program::Uniform4fv(GLuint loc, GLint count, GLfloat* ptr)
	{
		glUniform4fv(loc, count, ptr);
	}

	void Program::Uniform1dv(GLuint loc, GLint count, GLdouble* ptr)
	{
		glUniform1dv(loc, count, ptr);
	}

	void Program::Uniform2dv(GLuint loc, GLint count, GLdouble* ptr)
	{
		glUniform2dv(loc, count, ptr);
	}

	void Program::Uniform3dv(GLuint loc, GLint count, GLdouble* ptr)
	{
		glUniform3dv(loc, count, ptr);
	}

	void Program::Uniform4dv(GLuint loc, GLint count, GLdouble* ptr)
	{
		glUniform4dv(loc, count, ptr);
	}

	void Program::UniformMatrix2f(GLuint loc, GLint count, GLboolean transpose, GLfloat* ptr)
	{
		glUniformMatrix2fv(loc, count, transpose, ptr);
	}

	void Program::UniformMatrix3f(GLuint loc, GLint count, GLboolean transpose, GLfloat* ptr)
	{
		glUniformMatrix3fv(loc, count, transpose, ptr);
	}

	void Program::UniformMatrix4f(GLuint loc, GLint count, GLboolean transpose, GLfloat* ptr)
	{
		glUniformMatrix4fv(loc, count, transpose, ptr);
	}

}