#pragma once
#include "Object.h"
#include "Shader.h"

namespace GL
{
	class Program : public Object
	{ 
	protected:

		void CreateObject();
		void DeleteObject();
		bool IsObject();

	public:

		Program();
		~Program();
		//int		LoadFile(Program& program, std::string file);
		void		Clear();
		std::string Log();
		bool		Link();
		bool		IsLinked();
		bool		AttachShader(Shader& src);
		void		Bind();
		void		Unbind();

	public:

		GLuint		FragDataLocation(std::string name); // Program Query
		GLuint		AtributeLocation(std::string name); // Program Query
		GLuint		UniformLocation(std::string name); // Program Query

	public: // Uniforms com location

		void Uniform1i(GLuint, GLint);
		void Uniform2i(GLuint, GLint, GLint);
		void Uniform3i(GLuint, GLint, GLint, GLint);
		void Uniform4i(GLuint, GLint, GLint, GLint, GLint);
		void Uniform1f(GLuint, GLfloat);
		void Uniform2f(GLuint, GLfloat, GLfloat);
		void Uniform3f(GLuint, GLfloat, GLfloat, GLfloat);
		void Uniform4f(GLuint, GLfloat, GLfloat, GLfloat, GLfloat);
		void Uniform1d(GLuint, GLdouble);
		void Uniform2d(GLuint, GLdouble, GLdouble);
		void Uniform3d(GLuint, GLdouble, GLdouble, GLdouble);
		void Uniform4d(GLuint, GLdouble, GLdouble, GLdouble, GLdouble);
		void Uniform1iv(GLuint, GLint, GLint*);
		void Uniform2iv(GLuint, GLint, GLint*);
		void Uniform3iv(GLuint, GLint, GLint*);
		void Uniform4iv(GLuint, GLint, GLint*);
		void Uniform1fv(GLuint, GLint, GLfloat*);
		void Uniform2fv(GLuint, GLint, GLfloat*);
		void Uniform3fv(GLuint, GLint, GLfloat*);
		void Uniform4fv(GLuint, GLint, GLfloat*);
		void Uniform1dv(GLuint, GLint, GLdouble*);
		void Uniform2dv(GLuint, GLint, GLdouble*);
		void Uniform3dv(GLuint, GLint, GLdouble*);
		void Uniform4dv(GLuint, GLint, GLdouble*);
		void UniformMatrix2f(GLuint, GLint, GLboolean, GLfloat*);
		void UniformMatrix3f(GLuint, GLint, GLboolean, GLfloat*);
		void UniformMatrix4f(GLuint, GLint, GLboolean, GLfloat*);

	};
}