#include "RBO.h"

namespace GL
{
	void RBO::CreateObject()
	{
		glGenRenderbuffers(1, &m_obj);
	}

	void RBO::DeleteObject()
	{
		glDeleteRenderbuffers(1, &m_obj);
	}

	bool RBO::IsObject()
	{
		return glIsRenderbuffer(m_obj);
	}

	bool RBO::CheckObject()
	{
		return true;
	}

	RBO::RBO(const RBO& rbo) 
		: Object(GL_RENDERBUFFER)
	{
		CheckObject();
	}

	RBO::~RBO()
	{
		if(IsObject())
			DeleteObject();
	}

	void RBO::Initialize(RBOParams& params)
	{
		Bind();
		SetRBO(params);
		Unbind();
	}

	void RBO::SetRBO(RBOParams& params)
	{
		glRenderbufferStorage(m_target, params.internalFormat, params.width, params.height);
	}

	void RBO::Bind()
	{
		glBindRenderbuffer(m_target, m_obj);
	}

	void RBO::Unbind()
	{
		glBindRenderbuffer(m_target, 0);
	}
}