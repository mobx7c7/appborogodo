#pragma once
#include "Object.h"

namespace GL
{
	struct RBOParams
	{
		GLenum  target;
		GLuint  width;
		GLuint  height;
		GLuint  depth;
		GLuint  internalFormat;
	};

	class RBO : public Object
	{
	protected:

		void CreateObject();

		void DeleteObject();

		bool CheckObject();

		bool IsObject();

	public:

		RBO(const RBO& rbo);

		~RBO();

		void Initialize(RBOParams& params);

		void SetRBO(RBOParams& params);

		void Bind();

		void Unbind();

	};
}