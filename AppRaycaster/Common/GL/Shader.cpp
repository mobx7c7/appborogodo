#include "Shader.h"
#include <fstream>
#include <sstream>

namespace GL
{
	void Shader::CreateObject()
	{
		m_obj = glCreateShader(m_type);
	}

	void Shader::DeleteObject()
	{
		glDeleteShader(m_obj);
	}

	bool Shader::IsObject()
	{
		return glIsShader(m_obj);
	}

	int Shader::GetShaderiv(GLenum param)
	{
		GLint value = 0; // length
		glGetShaderiv(m_obj, param, &value);
		return static_cast<bool>(value != GL_INVALID_VALUE ? value : 0);
	}

	Shader::Shader(GLenum type) 
		: Object()
		, m_type(type)
	{
		CheckObject();
	}

	Shader::Shader(const Shader& shader)
		: Object(shader)
		, m_type(shader.m_type)
	{}

	Shader::Shader(std::string src, GLenum type) 
		: Object()
		, m_type(type)
	{
		Compile(src);
	}

	Shader::~Shader()
	{
		if(IsObject())
			DeleteObject();
	}

	/*
	bool Shader::LoadFile(std::string file)
	{
		std::ifstream ifs(file);

		if(!ifs.is_open())
		{
			return false;
		}

		std::stringstream ss;
		ss << ifs.rdbuf();
		ifs.close();
		Compile(ss.str());
		//this = Shader(ss.str(), this->Type());
		return true;
	}
	*/

	Shader& Shader::FromFile(std::string file, GLenum type)
	{
		std::ifstream ifs(file);

		if(ifs.is_open())
		{
			std::stringstream ss;
			ss << ifs.rdbuf();
			ifs.close();
			return Shader(ss.str(), type);
		}
		else
		{
			// TODO: Throw
			return Shader(type);
		}
	}

	bool Shader::Compile(const std::string src)
	{
		if(CheckObject()) 
		{
			const char* data = src.c_str();
			glShaderSource(m_obj, 1, &data, NULL);
			glCompileShader(m_obj);
			return IsCompiled();
		}
		else
		{
			// TODO: Throw
			return false;
		}
	}

	bool Shader::IsCompiled()
	{
		return GetShaderiv(GL_COMPILE_STATUS);
	}

	GLenum Shader::Type()
	{
		return GetShaderiv(GL_SHADER_TYPE);
	}

	std::string Shader::Log()
	{
		GLint value = GetShaderiv(GL_INFO_LOG_LENGTH);

		if(value != GL_INVALID_VALUE && value > 0)
		{
			char* text = new char[value];
			GLint written = 0;
			glGetShaderInfoLog(m_obj, value, &written, text);
			std::string _text(text, written);
			delete[] text;
			return _text;
		}
		else
		{
			return "";
		}
	}

	std::string Shader::Source()
	{
		GLint value = GetShaderiv(GL_SHADER_SOURCE_LENGTH);

		if(value != GL_INVALID_VALUE && value > 0)
		{
			char* text = new char[value];
			GLint written = 0;
			glGetShaderSource(m_obj, value, &written, text);
			std::string _text(text, written);
			delete[] text;
			return _text;
		}
		else
		{
			return "";
		}
	}

}