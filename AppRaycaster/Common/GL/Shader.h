#pragma once
#include "Object.h"
#include <string>

namespace GL
{
	class Shader : public Object
	{
	private:

		void CreateObject();
		void DeleteObject();
		bool IsObject();

	protected:
		
		GLenum m_type;

	public:

		Shader(GLenum type);

		Shader(const Shader& shader);

		Shader(std::string src, GLenum type);

		~Shader();

		bool            Compile(const std::string src);

		bool            IsCompiled();

		int				GetShaderiv(GLenum param);

		GLenum          Type();

		std::string     Log();

		std::string     Source();

		//bool			LoadFile(std::string file);

		static Shader&  FromFile(std::string file, GLenum type);

	};
}