#include "Texture.h"

namespace GL
{
	Texture* Texture::m_current = 0;

	void Texture::CreateObject()
	{
		glGenTextures(1, &m_obj);
	}

	void Texture::DeleteObject()
	{
		glDeleteTextures(1, &m_obj);
	}

	bool Texture::IsObject()
	{
		return glIsTexture(m_obj);
	}

	Texture::Texture(GLenum target) 
		: Object(target)
	{
		CheckObject();
	}

	Texture::~Texture(void)
	{
		if(IsObject())
			DeleteObject();
	}

	void Texture::Initialize(TextureParams& params)
	{
		Bind();
		SetParameteri(GL_TEXTURE_MIN_FILTER, params.minFilter);
		SetParameteri(GL_TEXTURE_MAG_FILTER, params.magFilter);
		SetParameteri(GL_TEXTURE_WRAP_S, params.wrapS);
		SetParameteri(GL_TEXTURE_WRAP_T, params.wrapT);
		SetTexture(params);
		Unbind();
	}

	void Texture::SetParameteri(unsigned param, int value)
	{
		glTexParameteri(m_current->GetTarget(), param, value);
	}

	void Texture::SetTexture(TextureParams& params)
	{
		glTexImage2D(m_current->GetTarget(), params.level, params.internalFormat, params.width, params.height, params.border, params.dataFormat, params.dataType, params.data);
	}

	void Texture::SetSubImage(TextureParams& params)
	{
		glTexSubImage2D(m_current->GetTarget(), params.level, params.offsetX, params.offsetY, params.width, params.height, params.dataFormat, params.dataType, params.data);
	}

	void Texture::Bind()
	{
		m_current = this;
		glBindTexture(m_target, m_obj);
	}

	void Texture::Unbind()
	{
		glBindTexture(m_target, 0);
	}
}