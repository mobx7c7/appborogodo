#pragma once
#include "Object.h"

namespace GL
{
	struct TextureParams
    {
        GLenum  target;
        GLuint  level;
        GLuint  offsetX;
        GLuint  offsetY;
        GLuint  offsetZ;
        GLuint  width;
        GLuint  height;
        GLuint  depth;
        GLuint  internalFormat;
        GLuint  dataFormat;
        GLuint  dataType;
        GLuint  border;
        GLuint  wrapR;
        GLuint  wrapS;
        GLuint  wrapT;
        GLuint  minFilter;
        GLuint  magFilter;
        GLvoid* data;
    };
    
	class Texture : public Object
	{
	private:

		static Texture* m_current;

	protected:

		void CreateObject();

		void DeleteObject();

		bool IsObject();

	public:

		Texture(GLenum target);

		~Texture(void);

		void Initialize(TextureParams& params);

		static void SetParameteri(unsigned param, int value);

		static void SetTexture(TextureParams& params);

		static void SetSubImage(TextureParams& params);

		void Bind();

		void Unbind();

	};
}