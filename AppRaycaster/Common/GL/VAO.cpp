#include "VAO.h"

namespace GL
{
	void VAO::CreateObject()
	{
		glGenVertexArrays(1, &m_obj);
	}

	void VAO::DeleteObject()
	{
		glDeleteVertexArrays(1, &m_obj);
	}

	bool VAO::IsObject()
	{
		return glIsVertexArray(m_obj);
	}

	VAO::VAO()
		: Object()
	{
		CheckObject();
	}

	VAO::~VAO()
	{
		if(IsObject())
			DeleteObject();
	}

	void VAO::SetVertexAttribState(GLuint loc, GLboolean status)
	{
		switch(status)
		{
		case GL_TRUE:
			glEnableVertexAttribArray(loc);
			break;
		case GL_FALSE:
			glDisableVertexAttribArray(loc);
			break;
		}
	}

	void VAO::SetVertexAttribPointer(GLuint loc, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* data)
	{
		glVertexAttribPointer(loc, size, type, normalized, stride, data);
	}

	void VAO::Bind()
	{
		glBindVertexArray(m_obj);
	}

	void VAO::Unbind()
	{
		glBindVertexArray(0);
	}
}