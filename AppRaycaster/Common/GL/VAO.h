#pragma once
#include "Object.h"

namespace GL
{
	class VAO : public Object
	{
	protected:

		void CreateObject();

		void DeleteObject();

		bool IsObject();

	public:

		VAO();

		~VAO();

		void SetVertexAttribState(GLuint loc, GLboolean status);

		void SetVertexAttribPointer(GLuint loc, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* data);

		void Bind();

		void Unbind();

	};
}