#include "Graphics.h"
#include <random>
#include <iostream>
#include <exception>
#include <cmath>
#include "Math\Matrix.h"

/*
float __FOV;
float __clip_near;
float __clip_far;
float __aspect;
float __depth;
float __depthStep;

void Graphics:: __VertexToCoord__(const float* vert, int* coord)
{
	// FIXME: Transformar em opera��o por matriz
	Matrix::__VertexToPersp__(__FOV, __aspect, __clip_near, __clip_far, vert);
	Matrix::__VertexToOrtho__(m_fbuf->GetWidth(), m_fbuf->GetHeight(), vert);

	coord[0] = (int)vert[0];
	coord[1] = (int)vert[1];
}

*/




Graphics::Graphics(void)
{
	m_color = 0;
	m_blend_mode = BlendMode::BLEND_FUNC_NORMAL;
	m_fbuf = nullptr;
}

Graphics::Graphics(Framebuffer* fbuf)
{
	Graphics();
	SetFramebuffer(fbuf);
}

Graphics::~Graphics(void)
{

}

unsigned Graphics::__PixelFormatDec__(unsigned p)
{
	// Converte pixel formato tal para RGBA
	// NOTA: Usando ARGB8888 > RGBA8888
	return ((p >> 16 & 0xff) << 24) | ((p >> 8 & 0xff) << 16) | ((p & 0xff) << 8) | (p >> 24 & 0xff);
}

unsigned Graphics::__PixelFormatEnc__(unsigned p)
{
	// Converte pixel RGBA para tal formato
	// NOTA: Usando RGBA8888 > ARGB8888
	return ((p & 0xff) << 24) | ((p >> 24 & 0xff) << 16) | ((p >> 16 & 0xff) << 8) | (p >> 8 & 0xff);
}

unsigned Graphics::__PixelBlend__(unsigned src, unsigned dst)
{
	unsigned r1, g1, b1, a1;
	unsigned r2, g2, b2, a2;
	unsigned rx, gx, bx, ax;

	/*
	r1 = src >> 16 & 0xff;
	g1 = src >> 8 & 0xff;
	b1 = src & 0xff;
	a1 = src >> 24 & 0xff;

	r2 = dst >> 16 & 0xff;
	g2 = dst >> 8 & 0xff;
	b2 = dst & 0xff;
	a2 = dst >> 24 & 0xff;
	*/
	
	r1 = src >> 24 & 0xff;
	g1 = src >> 16 & 0xff;
	b1 = src >> 8 & 0xff;
	a1 = src & 0xff;

	r2 = dst >> 24 & 0xff;
	g2 = dst >> 16 & 0xff;
	b2 = dst >> 8 & 0xff;
	a2 = dst & 0xff;

	switch(m_blend_mode)
	{
	case 1: // adi��o
		rx = r1 + r2;
		gx = g1 + g2;
		bx = b1 + b2;
		ax = a1 + a2;
		break;
	default: // normal
		rx = r2;
		gx = g2;
		bx = b2;
		ax = a2;
	}

	rx = CLAMP(0, 255, rx);
	gx = CLAMP(0, 255, gx);
	bx = CLAMP(0, 255, bx);
	ax = CLAMP(0, 255, ax);

	//return (ax << 24) | (rx << 16) | (gx << 8) | b;
	return (rx << 24) | (gx << 16) | (bx << 8) | ax;
}

void Graphics::__PixelWrite__(int x, int y)
{
	if(x >= 0 && x < m_fbuf->GetWidth() && y >= 0 && y < m_fbuf->GetHeight())
	{
		unsigned src, dst;
		m_fbuf->GetPixel(x, y, (int&)src);
		src = __PixelFormatDec__(src);
		dst = __PixelBlend__(src, m_color);
		dst = __PixelFormatEnc__(dst);
		m_fbuf->SetPixel(x, y, dst);
	}
}

void Graphics::__GenerateNoise__()
{
	if(!m_fbuf)
		return;

	std::random_device rd;
	std::default_random_engine e1(rd());
	std::uniform_int_distribution<unsigned> uniform_dist(0, 255);

	unsigned* buf = (unsigned*)m_fbuf->GetData();

	for(int y=0; y<m_fbuf->GetHeight(); y++)
	{
		for(int x=0; x<m_fbuf->GetWidth(); x++)
		{
			*buf++ += COLOR_ARGB(uniform_dist(e1), uniform_dist(e1), uniform_dist(e1), 255);
		}
	}
}

void Graphics::__DrawPoint__(float x, float y)
{
	// TODO: Desenhar ponto com largura ajust�vel;
	__PixelWrite__((int)x,(int)y);
}

void Graphics::__DrawLine__(float x1, float y1, float x2, float y2)
{
	// Baseado de:
	// http://joshbeam.com/articles/simple_line_drawing/

	float x, y, xdelta, ydelta, min, max, slope;

	xdelta = x2 - x1;
	ydelta = y2 - y1;

	if(xdelta == 0.0f && ydelta == 0.0f)
	{
		__PixelWrite__(x1, y1);
		return;
	}

	if(fabs(xdelta) > fabs(ydelta))
	{
		if(x1 < x2)
		{
			min = x1;
			max = x2;
		}
		else
		{
			min = x2;
			max = x1;
		}

		slope = ydelta / xdelta;

		for(x=min; x<=max; x+=1.0f)
		{
			y = (x - x1) * slope + y1;
			__PixelWrite__((int)x,(int)y);
		}
	}
	else
	{
		if(y1 < y2)
		{
			min = y1;
			max = y2;
		}
		else
		{
			min = y2;
			max = y1;
		}

		slope = xdelta / ydelta;

		for(y=min; y<=max; y+=1.0f)
		{
			x = (y - y1) * slope + x1;
			__PixelWrite__((int)x,(int)y);
		}
	}
}

void Graphics::__DrawRect__(float x1, float y1, float x2, float y2)
{
	for(int y=y1; y<y2; y++)
	{
		for(int x=x1; x<x2; x++)
		{
			__PixelWrite__(x,y);
		}
	}
}

void Graphics::SetFramebuffer(Framebuffer* fbuf)
{
	this->m_fbuf = fbuf;
}

void Graphics::Clear()
{
	if(!m_fbuf)
		return;

	memset(m_fbuf->GetData(), 0, m_fbuf->GetSize());
}

void Graphics::SetColor(unsigned color)
{
	m_color = color;
}

void Graphics::SetBlendMode(unsigned mode)
{
	m_blend_mode = mode;
}

void Graphics::DrawPoint(float x, float y)
{
	if(!m_fbuf) 
		return;

	__DrawPoint__(x,y);
}

void Graphics::DrawLine(float x1, float y1, float x2, float y2)
{
	if(!m_fbuf) 
		return;

	__DrawLine__(x1,y1,x2,y2);

}

void Graphics::DrawRect(float x1, float y1, float x2, float y2)
{
	if(!m_fbuf)
		return;

	__DrawRect__(x1,y1,x2,y2);
}