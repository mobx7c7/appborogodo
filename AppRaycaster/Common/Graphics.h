#pragma once

#include "Framebuffer.h"
#include "Math\MathEx.h"
#include "Buffer\VertexBuffer.h"

#define COLOR_RGB(r,g,b) r << 24 | g << 16 | b << 8
#define COLOR_ARGB(r,g,b,a) a << 24 | r << 16 | g << 8 | b
#define COLOR_RGBA(r,g,b,a) r << 24 | g << 16 | b << 8 | a
#define CLAMP(a,b,c) (c>=a?(c<b?c:b):a)

// FIXME: Mover para classe de matematica


// TODO: ajuste do tamanho dos pontos e anti-aliasing

enum BlendMode
{
	BLEND_FUNC_NORMAL,
	BLEND_FUNC_ADD
};

enum DrawMode
{
	DRAW_MODE_POINT,
	DRAW_MODE_LINE,
	DRAW_MODE_TRIANGLE,
};

class Graphics
{
protected:

	Framebuffer*	m_fbuf;
	unsigned		m_color;
	unsigned		m_blend_mode;

	// NOTA: Calculo de cor sem ponto flutuante
	unsigned	__PixelFormatDec__(unsigned p);
	unsigned	__PixelFormatEnc__(unsigned p);
	unsigned	__PixelBlend__(unsigned src, unsigned dst);
	void		__PixelWrite__(int x, int y);

	
	void		__DrawLine__(float x1, float y1, float x2, float y2);
	void		__DrawRect__(float x1, float y1, float x2, float y2);
	
public:

	void		__DrawPoint__(float x, float y);

	//void		__VertexToCoord__(const float* vert, int* coord);
	// FIXME: Colocar em uma classe separada, tipo GraphicsPlus
	void		__GenerateNoise__();

	Graphics(void);
	Graphics(Framebuffer* fbuf);
	~Graphics(void);

	void Clear();
	void SetFramebuffer(Framebuffer* fbuf);
	void SetBlendMode(unsigned mode);
	void SetColor(unsigned color);
	void DrawPoint(float x, float y);
	void DrawLine(float x1, float y1, float x2, float y2);
	void DrawRect(float x1, float y1, float x2, float y2);


	struct Attrib
	{
		unsigned m_state;
		unsigned m_size;
		unsigned m_align;
	};
	Attrib attribs[2];

	void __SetAttributeState__(unsigned attrib, unsigned state)
	{
		Attrib& atb = attribs[attrib];
		atb.m_state = state;
	}
	void __SetAttributeParam__(unsigned attrib, unsigned size, unsigned align)
	{
		Attrib& atb = attribs[attrib];
		atb.m_size	= size;
		atb.m_align = align;
	}
	// FIXME: colocar suporte para cor, normal, textura e outros.
	void __DrawBuffer__(VertexBuffer* buffer)
	{
		if(!m_fbuf) return;

		float* cur = buffer->FirstPtr();
		float* end = buffer->LastPtr();

		float x,y;

		Vertex input;

		while(cur != end)
		{
			if(cur[2] > 0.001f && cur[2] < 100.0f)
			{
				x = cur[0] / cur[2];
				y = cur[1] / cur[2];
				__DrawPoint__(x, y);
			}
			cur += buffer->Stride();
		}
	}
};