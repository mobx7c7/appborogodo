#include "MathEx.h"

float uSin(float x)
{
	return 0.5f+0.5f*sin(x);
}

float uCos(float x)
{
	return 0.5f+0.5f*cos(x);
}

float Sinc(float x)
{
	return sin(x)/x;
}

float Wrap(float x, float k)
{
	return x-(k*floor(x*(1.0f/k)));
}

float Lerp(float a, float b, float x)
{
	return a+(b-a)*x;
}

float Cosine(float a, float b, float x)
{
	return Lerp(a,b,uCos(M_PI+M_PI*x));
}

/*
float Bilinear(float a, float b, float c, float d, float x)
{
	float k1 = Lerp(a,b,x);
	float k2 = Lerp(c,d,x);
	return Lerp(k1,k2,x);
}
*/