#pragma once
#include <cmath>

const float M_PI		= 3.14159265f;
const float M_PI2		= M_PI*2.0f;
const float M_PI_180	= M_PI/180.0f;
const float M_PI_360	= M_PI2/360.0f;

/*
namespace Math
	namespace Tool
	namespace Interp;
*/

float uSin(float x);

float uCos(float x);

float Sinc(float x);

float Wrap(float x, float k);

float Lerp(float a, float b, float x);

float Coserp(float a, float b, float x);

//float Bilinear(float a, float b, float c, float d, float x);