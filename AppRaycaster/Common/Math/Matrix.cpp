#include "Matrix.h"

Matrix::Matrix(void)
	: m_data(new float[16])
{
	Zero();
}

Matrix::Matrix(const Matrix& m)
	: m_data(new float[16])
{
	std::copy(m.m_data, m.m_data+16, m_data);
}

Matrix::~Matrix(void)
{
	delete[] m_data;
}

void Matrix::Zero()
{
	std::fill(m_data, m_data+16, 0);
}

void Matrix::Identity()
{
	std::fill(m_data, m_data+16, 0);
	m_data[0] = 1.0f;
	m_data[5] = 1.0f;
	m_data[10] = 1.0f;
	m_data[15] = 1.0f;
}

float* Matrix::operator[](int row)
{
	return m_data + (row * 4);
}

Matrix& Matrix::operator=(const Matrix& m)
{
	m_data[0] = m.m_data[0];
	m_data[1] = m.m_data[1];
	m_data[2] = m.m_data[2];
	m_data[3] = m.m_data[3];
	m_data[4] = m.m_data[4];
	m_data[5] = m.m_data[5];
	m_data[6] = m.m_data[6];
	m_data[7] = m.m_data[7];
	m_data[8] = m.m_data[8];
	m_data[9] = m.m_data[9];
	m_data[10] = m.m_data[10];
	m_data[11] = m.m_data[11];
	m_data[12] = m.m_data[12];
	m_data[13] = m.m_data[13];
	m_data[14] = m.m_data[14];
	m_data[15] = m.m_data[15];
	return *this;
}

Matrix& operator+(const Matrix& a, const Matrix& b)
{
	Matrix m(a);
	m += b;
	return m;
}

Matrix& operator-(const Matrix& a, const Matrix& b)
{
	Matrix m(a);
	m -= b;
	return m;
}

Matrix& operator*(const Matrix& a, const Matrix& b)
{
	Matrix m(a);
	m *= b;
	return m;
}

Matrix& operator/(const Matrix& a, const Matrix& b)
{
	Matrix m(a);
	m /= b;
	return m;
}

Matrix& Matrix::operator+=(const Matrix& m)
{
	m_data[0] += m.m_data[0];
	m_data[1] += m.m_data[1];
	m_data[2] += m.m_data[2];
	m_data[3] += m.m_data[3];
	m_data[4] += m.m_data[4];
	m_data[5] += m.m_data[5];
	m_data[6] += m.m_data[6];
	m_data[7] += m.m_data[7];
	m_data[8] += m.m_data[8];
	m_data[9] += m.m_data[9];
	m_data[10] += m.m_data[10];
	m_data[11] += m.m_data[11];
	m_data[12] += m.m_data[12];
	m_data[13] += m.m_data[13];
	m_data[14] += m.m_data[14];
	m_data[15] += m.m_data[15];
	return *this;
}

Matrix& Matrix::operator-=(const Matrix& m)
{
	m_data[0] -= m.m_data[0];
	m_data[1] -= m.m_data[1];
	m_data[2] -= m.m_data[2];
	m_data[3] -= m.m_data[3];
	m_data[4] -= m.m_data[4];
	m_data[5] -= m.m_data[5];
	m_data[6] -= m.m_data[6];
	m_data[7] -= m.m_data[7];
	m_data[8] -= m.m_data[8];
	m_data[9] -= m.m_data[9];
	m_data[10] -= m.m_data[10];
	m_data[11] -= m.m_data[11];
	m_data[12] -= m.m_data[12];
	m_data[13] -= m.m_data[13];
	m_data[14] -= m.m_data[14];
	m_data[15] -= m.m_data[15];
	return *this;
}

Matrix& Matrix::operator*=(const Matrix& m)
{
	m_data[0] *= m.m_data[0];
	m_data[1] *= m.m_data[1];
	m_data[2] *= m.m_data[2];
	m_data[3] *= m.m_data[3];
	m_data[4] *= m.m_data[4];
	m_data[5] *= m.m_data[5];
	m_data[6] *= m.m_data[6];
	m_data[7] *= m.m_data[7];
	m_data[8] *= m.m_data[8];
	m_data[9] *= m.m_data[9];
	m_data[10] *= m.m_data[10];
	m_data[11] *= m.m_data[11];
	m_data[12] *= m.m_data[12];
	m_data[13] *= m.m_data[13];
	m_data[14] *= m.m_data[14];
	m_data[15] *= m.m_data[15];
	return *this;
}

Matrix& Matrix::operator/=(const Matrix& m)
{
	m_data[0] /= m.m_data[0];
	m_data[1] /= m.m_data[1];
	m_data[2] /= m.m_data[2];
	m_data[3] /= m.m_data[3];
	m_data[4] /= m.m_data[4];
	m_data[5] /= m.m_data[5];
	m_data[6] /= m.m_data[6];
	m_data[7] /= m.m_data[7];
	m_data[8] /= m.m_data[8];
	m_data[9] /= m.m_data[9];
	m_data[10] /= m.m_data[10];
	m_data[11] /= m.m_data[11];
	m_data[12] /= m.m_data[12];
	m_data[13] /= m.m_data[13];
	m_data[14] /= m.m_data[14];
	m_data[15] /= m.m_data[15];
	return *this;
}

void Matrix::Perspective(Matrix& m, float fov, float aspect, float minZ, float maxZ)
{
	std::fill(m.m_data, m.m_data+16, 0);//memset(m.m_data, 0, sizeof(float)*16);
	float cot = 1.0f/tan((fov/2.0f) * M_PI_180);
	m[0][0] = cot / aspect;
	m[1][1] = cot;
	m[2][2] = (minZ + maxZ) / (minZ - maxZ);
	m[2][3] = (2.0f * minZ * maxZ) / (minZ - maxZ);
	//m[2][2] = maxZ / (maxZ - minZ);
	//m[2][3] = (minZ * maxZ) / (maxZ - minZ);
	m[3][2] = -1.0f;
}

void Matrix::Orthographic(Matrix& m, float left, float right, float top, float bottom, float zMin, float zMax)
{
	// Retirado de:
	// http://in2gpu.com/wp-content/uploads/2015/03/orthographic-matrix-1.jpg
	std::fill(m.m_data, m.m_data+16, 0);
	m[0][0] = 2.0f / (right - left);
	m[1][1] = 2.0f / (top - bottom);
	m[2][2] = 2.0f / (zMin - zMax);
	m[0][2] = (right + left) / (right - left);
	m[1][2] = (top + bottom) / (top - bottom);
	m[2][2] = (zMin + zMax) / (zMin - zMax);
	m[3][2] = -1.0f;
	m[3][3] = (2.0f * zMin  * zMax) / (zMin - zMax);
}

void Matrix::__VertexToOrtho__(int width, int height, Vec3& v)
{
	float _v[2];
	_v[0] = v.x;
	_v[1] = v.y;
	Matrix::__VertexToOrtho__(width, height, _v);
	v.x = _v[0];
	v.y = _v[1];
}

void Matrix::__VertexToOrtho__(int width, int height, float* v)
{
	v[0] = (v[0] * width) + width/2.0f;
	v[1] = (v[1] * height) + height/2.0f;
}

void Matrix::__VertexToPersp__(float fov, float aspect, float min, float max, float* vert)
{
	float focal = 1.0f/tan(fov*M_PI_180);
	float length = focal/vert[2]; // z
	vert[0] *= length * aspect;
	vert[1] *= length;
}
