#pragma once
#ifndef MATRIX_H
#define MATRIX_H

#include "Vec3.h"
#include "Buffer\VertexRef.h"
#include "MathEx.h"

class Matrix
{
private:

	float* m_data;

public:

	Matrix(void);
	Matrix(const Matrix&);
	~Matrix(void);
	void Zero();
	void Identity();

	float* operator[](int row);
	Matrix& operator=(const Matrix& m);

	friend Matrix& operator+(const Matrix& a, const Matrix& b);
	friend Matrix& operator-(const Matrix& a, const Matrix& b);
	friend Matrix& operator*(const Matrix& a, const Matrix& b);
	friend Matrix& operator/(const Matrix& a, const Matrix& b);

	Matrix& operator+=(const Matrix& m);
	Matrix& operator-=(const Matrix& m);
	Matrix& operator*=(const Matrix& m);
	Matrix& operator/=(const Matrix& m);


	static void Orthographic(Matrix& m, float left, float right, float top, float bottom, float minZ, float maxZ);
	static void Perspective(Matrix& m, float fov, float aspect, float minZ, float maxZ);
	
	static void Translate(Matrix& m, float x, float y, float z)
	{
		m[0][0] = 
		m[1][1] = 
		m[2][2] = 
		m[3][3] = 1.0f;
		m[0][3] = x;
		m[1][3] = y;
		m[2][3] = z;
	}


	static void		__VertexToOrtho__(int width, int height, Vec3& v);
	static void		__VertexToOrtho__(int width, int height, float* v);
	static void		__VertexToPersp__(float fov, float aspect, float minZ, float maxZ, float* dst);

	static void		__Perspective__(float fov, float aspect, float zMin, float zMax, Vertex& v)
	{
		float cot = 1.0f/tan((fov/2.0f) * M_PI_180);
		v.X() *= cot / aspect;
		v.Y() *= cot;
		//v.z *= -(zMin/(zMax-zMin));
		//v.z += (zMin*zMax)/(zMax-zMin);
		//v.z *= -1.0f;
	}

	//static void __Viewport__(float w, float h, const Vec3& v)
	static void __Viewport__(float w, float h, Vertex& v)
	{
		w /= 2.0f;
		h /= 2.0f;

		//////////////////////////////////////////
		//v.X() /= v.Z();
		//v.Y() /= v.Z();
		v.X() *= w;
		v.Y() *= -h;
		v.X() += w;
		v.Y() += h;
	}
};

#endif // MATRIX_H