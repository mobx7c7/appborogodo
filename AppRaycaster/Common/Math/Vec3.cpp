#include "Vec3.h"
#include <memory>

Vec3::Vec3(void) 
	: data(new float[3])
	, x(data[0])
	, y(data[1])
	, z(data[2])
{
	memset(data, 0, 12);
}

Vec3::Vec3(const Vec3& b)
	: data(new float[3])
	, x(data[0])
	, y(data[1])
	, z(data[2])
{
	memcpy(data, b.data, 12);
}

Vec3::Vec3(float x, float y, float z)
	: data(new float[3])
	, x(data[0])
	, y(data[1])
	, z(data[2])
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vec3::~Vec3(void)
{
	delete[] data;
}

// operators com Vec3

Vec3& Vec3::operator=(const Vec3& b)
{
	x = b.x;
	y = b.y;
	z = b.z;
	return *this;
}

Vec3 Vec3::operator+(const Vec3 &b) const
{
	return Vec3(x + b.x, y + b.y, z + b.z);
}

Vec3 Vec3::operator-(const Vec3 &b) const
{
	return Vec3(x - b.x, y - b.y, z - b.z);
}

Vec3 Vec3::operator*(const Vec3 &b) const
{
	return Vec3(x * b.x, y * b.y, z * b.z);
}

Vec3 Vec3::operator/(const Vec3 &b) const
{
	return Vec3(x / b.x, y / b.y, z / b.z);
}

Vec3& Vec3::operator+=(const Vec3 &b)
{
	x += b.x;
	y += b.y;
	z += b.z;
	return *this;
}

Vec3& Vec3::operator-=(const Vec3 &b)
{
	x -= b.x;
	y -= b.y;
	z -= b.z;
	return *this;
}

Vec3& Vec3::operator*=(const Vec3 &b)
{
	x *= b.x;
	y *= b.y;
	z *= b.z;
	return *this;
}

Vec3& Vec3::operator/=(const Vec3 &b)
{
	x /= b.x;
	y /= b.y;
	z /= b.z;
	return *this;
}

// operators com Float

Vec3& Vec3::operator=(const float k)
{
	x = k;
	y = k;
	z = k;
	return *this;
}

Vec3 Vec3::operator+(const float k) const
{
	return Vec3(x + k, y + k, z + k);
}

Vec3 Vec3::operator-(const float k) const
{
	return Vec3(x - k, y - k, z - k);
}

Vec3 Vec3::operator*(const float k) const
{
	return Vec3(x * k, y * k, z * k);
}

Vec3 Vec3::operator/(const float k) const
{
	return Vec3(x / k, y / k, z / k);
}

Vec3& Vec3::operator+=(const float k)
{
	x += k;
	y += k;
	z += k;
	return *this;
}

Vec3& Vec3::operator-=(const float k)
{
	x -= k;
	y -= k;
	z -= k;
	return *this;
}

Vec3& Vec3::operator*=(const float k)
{
	x *= k;
	y *= k;
	z *= k;
	return *this;
}

Vec3& Vec3::operator/=(const float k)
{
	x /= k;
	y /= k;
	z /= k;
	return *this;
}

// Operatos Geral

std::ostream& operator<<(std::ostream& os, const Vec3& v)
{
	return os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
}