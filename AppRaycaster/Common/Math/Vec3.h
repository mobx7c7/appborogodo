#pragma once
#include <ostream>

class Vec3
{
protected:

	float* data;

public:

	float &x, &y, &z;
	Vec3(void);
	Vec3(const Vec3& b);
	Vec3(float x, float y, float z);
	~Vec3(void);



	Vec3& operator=(const Vec3& b);
	Vec3 operator+(const Vec3 &b) const;
	Vec3 operator-(const Vec3 &b) const;
	Vec3 operator*(const Vec3 &b) const;
	Vec3 operator/(const Vec3 &b) const;
	Vec3& operator+=(const Vec3 &b);
	Vec3& operator-=(const Vec3 &b);
	Vec3& operator*=(const Vec3 &b);
	Vec3& operator/=(const Vec3 &b);

	Vec3& operator=(const float k);
	Vec3 operator+(const float k) const;
	Vec3 operator-(const float k) const;
	Vec3 operator*(const float k) const;
	Vec3 operator/(const float k) const;
	Vec3& operator+=(const float k);
	Vec3& operator-=(const float k);
	Vec3& operator*=(const float k);
	Vec3& operator/=(const float k);
	
	friend std::ostream& operator<<(std::ostream& os, const Vec3& v);

};


/*
	T T::operator+(const T2 &b) const;
	T T::operator-(const T2 &b) const;
	T T::operator*(const T2 &b) const;
	T T::operator/(const T2 &b) const;

	T& T::operator =(const T2& b);
	T& T::operator+=(const T2 &b) const;
	T& T::operator-=(const T2 &b) const;
	T& T::operator*=(const T2 &b) const;
	T& T::operator/=(const T2 &b) const;
*/

struct __Vec3__
{
	float x, y, z;
	__Vec3__(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
	__Vec3__()
	{
		x = y = z = 0.0f;
	}
};


inline void vec3_set(float* a, float* b)
{
	memcpy(b, a, 12);
}

inline void vec3_add(float* a, float* b, float* c)
{
	*c++ = *a++ + *b++;
	*c++ = *a++ + *b++;
	*c++ = *a++ + *b++;
}

inline void vec3_sub(float* a, float* b, float* c)
{
	*c++ = *a++ - *b++;
	*c++ = *a++ - *b++;
	*c++ = *a++ - *b++;
}

inline void vec3_mul(float* a, float* b, float* c)
{
	*c++ = *a++ * *b++;
	*c++ = *a++ * *b++;
	*c++ = *a++ * *b++;
}

inline void vec3_div(float* a, float* b, float* c)
{
	*c++ = *a++ / *b++;
	*c++ = *a++ / *b++;
	*c++ = *a++ / *b++;
}
