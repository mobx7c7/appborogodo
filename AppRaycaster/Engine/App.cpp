#include "App.h"
#include "Globals.h"
#include "SoundDevice.h"
#include "SerialDevice.h"
#include "MIDIDevice.h"
#include "NetworkDevice.h"
#include <iostream>
#include <iomanip>

App* App::m_current = 0;

App::App(int argc, char** argv) : m_status(AppStatus::STATUS_IDLE), m_app(0)
{
	// TODO: Carregador de arquivo de configuração
	m_deviceCount	= 4;
	m_devices		= new Device*[m_deviceCount];
	m_devices[0]	= SoundDevice::CreateCompatibleDevice();
	m_devices[1]	= SerialDevice::CreateCompatibleDevice();
	m_devices[2]	= MIDIDevice::CreateCompatibleDevice();
	m_devices[3]	= NetworkDevice::CreateCompatibleDevice();
	m_window		= (GDIWindow*)WindowDevice::CreateCompatibleDevice();
	m_current		= this;
}

App::~App()
{
	m_current = 0;
}

void App::__CreateThreads()
{
	m_hDrawThread = CreateThread(NULL, 0, __DrawCallback__, this, 0, NULL);  
}

void App::__DestroyThreads()
{
	WaitForMultipleObjects(1, &m_hDrawThread, TRUE, INFINITE);
	CloseHandle(m_hDrawThread);
}

void App::__CreateDevices()
{
	int cols = 0, rows = 0;

#if defined _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
    if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi))
	{
		cols = csbi.srWindow.Right - csbi.srWindow.Left + 1;
		rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	}
#endif

	//////////////////////////////////////////////////////////////

	m_app->OnCreate();

	//////////////////////////////////////////////////////////////
	// Devices (Sound, Serial, Network...)
	//////////////////////////////////////////////////////////////

	std::cout << "// " << APP_NAME << std::endl;
	std::cout << std::setfill('-') << std::setw(cols) << "";

	for(int i=0; i<m_deviceCount; i++)
	{	
		try
		{
			Device* dev = m_devices[i];
			if(dev)
			{
				std::cout << "Inicializacao " << dev->ClassName() << "\t";// << std::setfill('.') << std::setw(10) ;
				dev->SetInterface(m_app);
				dev->Initialize();
				dev->Start();
				std::cout << "OK" << " (" << dev->SystemName() << ")" << std::endl;
			}
		}
		catch(DeviceException& ex)
		{
			std::cout << "Merda: " << ex.what() << std::endl;
		}
	}

	//////////////////////////////////////////////////////////////
	// Window
	//////////////////////////////////////////////////////////////
	
	try
	{
		std::cout << "Agora criando a janela\t";
		m_window->SetInterface(m_app);
		m_window->Create(WND_WIDTH, WND_HEIGHT, WND_TiTLE);
		std::cout << "OK" << " (" << m_window->SystemName() << ")" << std::endl;
	}
	catch(std::exception& ex)
	{
		std::cout << "Merda: " << ex.what() << std::endl;
	}

	//////////////////////////////////////////////////////////////

	std::cout << "\nAparentemente tudo OK!" << std::endl;
	std::cout << std::setfill('-') << std::setw(cols) << "";
}

void App::__DestroyDevices()
{
	int cols = 0, rows = 0;
#if defined _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
    if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi))
	{
		cols = csbi.srWindow.Right - csbi.srWindow.Left + 1;
		rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	}
#endif

	std::cout << std::setfill('-') << std::setw(cols) << "";

	delete m_window;

	Device** cur = m_devices;
	Device** end = m_devices + m_deviceCount;

	while(cur < end)
	{
		Device* dev = *cur++;

		if(dev)
		{
			std::cout << "Destruindo " << dev->ClassName() << "...\n";
			delete dev;
			dev = nullptr;
		}
	}

	delete[] m_devices;
}

DWORD WINAPI App::__DrawCallback__(LPVOID lpParam)
{
	App* app = (App*)lpParam;

	while(app->m_status == AppStatus::STATUS_RUNNING)
	{
		app->m_app->OnRender();
		app->m_window->Update();
		Sleep(1);
	}

	return 0;
}

int App::Execute(IApp* app)
{
	m_app = app;

	m_status = AppStatus::STATUS_RUNNING;
	
	__CreateDevices();

	m_app->OnStart();

	//__CreateThreads();
	
	m_window->Show();

	//////////////////////////////////////////////////////////////

	MSG msg;
	
	/*
	LARGE_INTEGER timerFreq, timerBegin, timerLast;
	double tFreq, tBegin, tLast, tCorr;

	QueryPerformanceFrequency(&timerFreq);
	tFreq = (double)timerFreq.QuadPart;

	QueryPerformanceCounter(&timerBegin);
	tBegin = (double)timerBegin.QuadPart;

	QueryPerformanceCounter(&timerLast);
	tLast = (double)timerLast.QuadPart;

	tCorr = (double)(timerLast.QuadPart-timerBegin.QuadPart);
	tBegin = tBegin;
	*/

	//while(GetMessage(&msg, m_window->GetHandler(), 0, 0) > 0)
	for(;;)
	{
		if(PeekMessage(&msg, m_window->GetHandler(), 0, 0, TRUE))
		{
			if(msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		m_app->OnUpdate();

		m_app->OnRender();

		m_window->Update();

		Sleep(1);		
	}

	//////////////////////////////////////////////////////////////

	//__DestroyThreads();

	app->OnTerminate();

	__DestroyDevices();
	
	return 0;
}

void App::Terminate()
{
	if(m_status == AppStatus::STATUS_RUNNING)
	{
		m_status = AppStatus::STATUS_TERMINATE;

		PostMessage(m_window->GetHandler(), WM_QUIT, 0, 0);
	}
}

int App::GetStatus()
{
	return m_status;
}

Device& App::GetDevice(int device)
{
	switch(device)
	{
		case DeviceType::DEVICE_SOUND:
			return *m_devices[0];
		case DeviceType::DEVICE_MIDI:
			return *m_devices[1];
		case DeviceType::DEVICE_SERIAL:
			return *m_devices[2];
		case DeviceType::DEVICE_NETWORk:
			return *m_devices[3];
		default:
			return *((Device*)nullptr);
	}
}

GDIWindow& App::GetWindow()
{
	return *m_window;
}

IApp& App::GetInterface()
{
	return *m_app;
}

App& App::GetCurrent()
{
	return *m_current;
}
