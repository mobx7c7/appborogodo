#pragma once

#include "IApp.h"
#include "Device.h"
#include "Window\GDIWindow.h"

#if !defined _WIN32
#error "Somente pode ser compilado em plataforma Windows"
#endif

enum AppStatus
{
	STATUS_IDLE,
	STATUS_RUNNING,
	STATUS_TERMINATE,
	STATUS_PAUSE,
	STATUS_RESTART
};

class App
{
private:

	static App* m_current;
	IApp*		m_app;
	GDIWindow*		m_window;
	Device**	m_devices;
	int			m_deviceCount;
	int			m_status;
	void		__CreateThreads();
	void		__DestroyThreads();
	void		__CreateDevices();
	void		__DestroyDevices();
	static DWORD WINAPI __DrawCallback__(LPVOID lpParam);
	HANDLE		m_hDrawThread;
	
public:
	
	// TODO: Carregador de arquivo de configuração
	App(int argc, char** argv);
	~App();

	int			Execute(IApp* app);
	void		Terminate();
	int 		GetStatus();
	IApp&		GetInterface();
	Device&		GetDevice(int device);
	GDIWindow&		GetWindow();
	static App& GetCurrent();

};
