#include "Device.h"

Device::Device(std::string class_name, std::string system_name)
	: m_status(DeviceStatus::DEVICE_IDLE)
	, m_class_name(class_name)
	, m_system_name(system_name)
	, m_app(nullptr)
{}

Device::Device(const Device&)
{}

Device::~Device(void)
{}

void Device::SetInterface(IApp* app)
{
	m_app = app;
}

int Device::Status()
{
	return m_status;
}

const std::string& Device::SystemName()
{
	return m_system_name;
}

const std::string& Device::ClassName()
{
	return m_class_name;
}

void Device::SetStatus(int status)
{
	m_status = status;
}

void Device::Initialize()
{}

void Device::Terminate()
{}

void Device::Start()
{}

void Device::Stop()
{}


/*
void Device::operator<<(std::ostream& os)
{
	std::cout << "[" << m_name << "] " << os << std::endl;
}
*/