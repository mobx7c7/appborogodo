#pragma once

#include "IApp.h"
#include "DeviceException.h"
#include <string>
#include <ostream>

enum DeviceType
{
	DEVICE_SOUND,
	DEVICE_MIDI,
	DEVICE_SERIAL,
	DEVICE_NETWORk,
};

enum DeviceStatus
{
	DEVICE_IDLE,
	DEVICE_RUNNING,
	DEVICE_STOPPED,
};

class Device
{
private:

	Device(const Device&);
	int			m_status;
	std::string m_class_name;
	std::string m_system_name;

protected:

	Device(std::string class_name, std::string system_name);
	IApp* m_app;
	void			SetStatus(int status);

public:

	virtual	~Device(void);
	void				SetInterface(IApp* app);
	int					Status();
	const std::string&	SystemName();
	const std::string&	ClassName();
	virtual void		Initialize();
	virtual void		Terminate();
	virtual void		Start();
	virtual void		Stop();
	
	static Device* CreateCompatibleDevice(int device);
	/*
	friend std::ostream& operator<<(std::ostream& os, const Device& device)
	{
		return os << 
	}
	*/

	// TODO: carregar parametros atrav�s de uma string carregada de um arquivo de config.
	// virtual void SetParameters(const char* params) = 0;
	//const std::string&	GetInfo(int id);

};

/*
class DeviceHandler
{
private:
	Device* device;
public:
	DeviceHandler(Device* device) : device(device){}
};
*/



