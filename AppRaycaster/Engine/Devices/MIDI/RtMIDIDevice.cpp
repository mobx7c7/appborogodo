#include "RtMIDIDevice.h"
#include "Globals.h"
#include "App.h"

void RtMIDIDevice::__Callback__(double dt, std::vector<unsigned char> *msg, void *userData)
{
	RtMIDIDevice& device = *(RtMIDIDevice*)userData;

	if(App::GetCurrent().GetStatus() == AppStatus::STATUS_RUNNING)
	{
		IApp* m_app = device.m_app;

		if(m_app)
		{
			m_app->MIDIListener(msg->data(), msg->size());
		}
	}
}

RtMIDIDevice::RtMIDIDevice(void)
	: MIDIDevice("RtMidi " + RtMidi::getVersion())
{
	midiIn = midiOut = nullptr;
}

RtMIDIDevice::~RtMIDIDevice(void)
{}

void RtMIDIDevice::__PrintPorts__()
{
	unsigned int i, nPorts;
	std::string portName;

	///////////////////////////////////////////////////////////

	nPorts = midiIn->getPortCount();
	std::cout << nPorts << " MIDI input sources available.\n";

	for (i=0; i<nPorts; i++) 
	{
		try 
		{
			portName = midiIn->getPortName(i);
			std::cout << "MIDI Input Port #" << i+1 << ": " << portName << '\n';
		}
		catch(RtMidiError &error) 
		{
			throw DeviceException(error.getMessage().c_str());
		}
	}

	nPorts = midiOut->getPortCount();
	std::cout << nPorts << " MIDI output sources available.\n";

	for (i=0; i<nPorts; i++) 
	{
		try 
		{
			portName = midiOut->getPortName(i);
			std::cout << "MIDI Output Port #" << i+1 << ": " << portName << '\n';
		}
		catch(RtMidiError &error) 
		{
			throw DeviceException(error.getMessage().c_str());
		}
	}
}

void RtMIDIDevice::Initialize()
{
	if(Status() == DeviceStatus::DEVICE_IDLE)
	{
		try
		{
			midiIn = new RtMidiIn();
			midiIn->setCallback(&__Callback__, this);
			midiIn->ignoreTypes(false, false, false); // Don't ignore sysex, timing, or active sensing messages.
			SetStatus(DeviceStatus::DEVICE_STOPPED);
		}
		catch (RtMidiError &error) 
		{
			throw DeviceException(error.getMessage().c_str());
		}
	}
}

void RtMIDIDevice::Terminate()
{
	if(Status() != DeviceStatus::DEVICE_IDLE)
	{
		if(midiIn)	
			delete midiIn;
		if(midiOut) 
			delete midiOut;

		midiIn = midiOut = 0;

		SetStatus(DeviceStatus::DEVICE_IDLE);
	}
}

void RtMIDIDevice::Start()
{
	if(Status() != DeviceStatus::DEVICE_IDLE)
	{
		try
		{
			midiIn->openPort(MIDI_DEVICE_INDEX);
			SetStatus(DeviceStatus::DEVICE_RUNNING);
		}
		catch (RtMidiError &error) 
		{
			throw DeviceException(error.getMessage().c_str());
		}
	}
}

void RtMIDIDevice::Stop()
{
	if(Status() != DeviceStatus::DEVICE_IDLE)
	{
		try
		{
			midiIn->closePort();
			SetStatus(DeviceStatus::DEVICE_STOPPED);
		}
		catch (RtMidiError &error) 
		{
			throw DeviceException(error.getMessage().c_str());
		}
	}
}
