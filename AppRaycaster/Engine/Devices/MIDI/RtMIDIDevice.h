#pragma once
#include "MIDIDevice.h"
#include "RtMidi.h"

class RtMIDIDevice : public MIDIDevice
{
private:
	
	static void __Callback__(double dt, std::vector<unsigned char> *msg, void *userData);
	void __PrintPorts__();
	RtMidiIn *midiIn, *midiOut;

public:

	RtMIDIDevice(void);
	~RtMIDIDevice(void);
	void	Initialize();
	void	Terminate();
	void	Start();
	void	Stop();

};
