#include "MIDIDevice.h"
#include "Globals.h"
#include "MIDI\RtMIDIDevice.h"
#include "MIDI\NullMIDIDevice.h"

MIDIDevice::MIDIDevice(std::string system_name) 
	: Device("MIDI", system_name)
{}

MIDIDevice::~MIDIDevice()
{}

Device* MIDIDevice::CreateCompatibleDevice()
{
	// Nota: RtMIDIDevice (RtMIDI) � multi-plataforma!
#if defined NULL_DEVICE_MIDI
	return new NullMIDIDevice();
#else
	return new RtMIDIDevice();
#endif
}