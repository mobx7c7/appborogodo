#pragma once
#include "Device.h"

class MIDIDevice : public Device
{
protected:
	MIDIDevice(std::string system_name);
public:
	virtual ~MIDIDevice();
	static Device* CreateCompatibleDevice();
};