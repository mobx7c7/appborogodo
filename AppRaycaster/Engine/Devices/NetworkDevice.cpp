#include "NetworkDevice.h"
#include "Globals.h"
#include "NullNetworkDevice.h"
#include "WinNetworkDevice.h"

NetworkDevice::NetworkDevice(std::string system_name)
	: Device("Network", system_name)
{
}

NetworkDevice::~NetworkDevice(void)
{
}

Device* NetworkDevice::CreateCompatibleDevice()
{
#if !defined _WIN32 || defined NULL_DEVICE_NETWORK
	return new NullNetworkDevice();
#else
	return new WinNetworkDevice();
#endif
}