#pragma once
#include "Device.h"

class NetworkDevice : public Device
{
protected:
	NetworkDevice(std::string system_name);
public:
	virtual ~NetworkDevice(void);
	static Device* CreateCompatibleDevice();
};

