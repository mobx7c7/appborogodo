#pragma once
#include "SerialDevice.h"

class NullSerialDevice : public SerialDevice
{
public:
	NullSerialDevice();
	~NullSerialDevice();
};
