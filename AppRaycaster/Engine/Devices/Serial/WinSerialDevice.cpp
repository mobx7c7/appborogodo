#include "WinSerialDevice.h"
#include "Globals.h"
#include <iostream>

enum SerialErrorMsg
{
	SERIAL_ERROR_UNKNOWN,
	SERIAL_ERROR_READ,
	SERIAL_ERROR_WRITE,
	SERIAL_ERROR_PORT_OPEN,
	SERIAL_ERROR_PORT_CLOSE,
	SERIAL_ERROR_PORT_UNAVAIBLE,
	SERIAL_ERROR_THREAD_CREATE,
	SERIAL_ERROR_THREAD_CLOSE,
	SERIAL_ERROR_GET_PARAMS,
	SERIAL_ERROR_SET_PARAMS
};

DWORD WINAPI WinSerialDevice::__Callback__(LPVOID lpParam)
{
	WinSerialDevice& dev = *(WinSerialDevice*)lpParam;

	IApp* iapp = dev.m_app;

	unsigned char buffer[SERIAL_BUFSIZE];

	DWORD bytesRead = 0;

	for(;;)
	{
		memset(buffer, 0, SERIAL_BUFSIZE);

		bytesRead = dev.Read(buffer, SERIAL_BUFSIZE);

		if(iapp && bytesRead != -1)
		{
			iapp->SerialListener(buffer, bytesRead);
		}
	}

	return 0;
}

WinSerialDevice::WinSerialDevice(void)
	: SerialDevice("WinAPI")
{
	m_hSerial = NULL;
	m_hReadThread = NULL;
}

WinSerialDevice::~WinSerialDevice(void)
{}

DWORD WinSerialDevice::Write(const void* data, unsigned size)
{
	DWORD bytesSent;

	try
	{
		if(!WriteFile(m_hSerial, data, size, &bytesSent, NULL))
		{
			ClearCommError(m_hSerial, &m_errors, &m_status);
			throw DeviceException("Send data error");
		}
	}
	catch(DeviceException& ex)
	{
		throw;
	}

	return bytesSent;
}

DWORD WinSerialDevice::Read(void* data, unsigned size)
{
	DWORD bytesRead;

	unsigned bytesToRead;

	ClearCommError(m_hSerial, &m_errors, &m_status);

	if(m_status.cbInQue > 0)
	{
		if(m_status.cbInQue > size)
		{
			bytesToRead = size;
		}
		else
		{
			bytesToRead = m_status.cbInQue;
		}
	}

	if(!ReadFile(m_hSerial, data, bytesToRead, &bytesRead, NULL))
	{
		return -1;
	}

	return bytesRead;
}

DWORD WinSerialDevice::Peek()
{
	DWORD bytesAvailable = 0;

	if(m_hSerial != NULL)
	{
		ClearCommError(m_hSerial, &m_errors, &m_status);
		bytesAvailable = m_status.cbInQue;
	}

	return bytesAvailable;
}

void WinSerialDevice::Flush()
{
	if(m_hSerial != NULL)
	{
		PurgeComm(m_hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR);
	}
}

void WinSerialDevice::Initialize()
{
	Create(SERIAL_PORTNAME, SERIAL_BAUDRATE, SERIAL_BYTESIZE, SERIAL_PARITY, SERIAL_STOPBITS, SERIAL_FLOWCTRL);
}

void WinSerialDevice::Terminate()
{
	Stop();
	Close();
}

void WinSerialDevice::Start()
{
	try
	{
		if(m_hSerial == NULL)
		{
			throw DeviceException("m_hSerial == NULL");
		}

		if(m_hReadThread == NULL)
		{
			HANDLE hReadThread = CreateThread(NULL, 0, __Callback__, this, 0, NULL);  
			if (hReadThread == NULL)
			{
				throw DeviceException("Creating thread error");
			}
			m_hReadThread = hReadThread;
		}
	}
	catch(DeviceException& ex)
	{
		throw;
	}
}

void WinSerialDevice::Stop()
{
	try
	{
		if(m_hSerial == NULL)
		{
			throw DeviceException("m_hSerial == NULL");
		}

		if(m_hReadThread != NULL)
		{
			if(!CloseHandle(m_hReadThread))
			{
				throw DeviceException("Closing thread error");
			}
			// aka: join()
			WaitForMultipleObjects(1, &m_hReadThread, TRUE, INFINITE);
			m_hReadThread = NULL;
		}
	}
	catch(DeviceException& ex)
	{
		throw;
	}
}

void WinSerialDevice::Create(const char* portName, int baudRate, int byteSize, int parity, int stopBits, int flowControl)
{
	try
	{
		if(m_hSerial != NULL)
		{
			throw DeviceException("m_hSerial != NULL");
		}

		/*
		if(m_hReadThread != NULL)
		{
			if(!CloseHandle(m_hReadThread))
			{
				throw DeviceException("Closing thread error");
			}
			// aka: join()
			WaitForMultipleObjects(1, &m_hReadThread, TRUE, INFINITE);
			m_hReadThread = NULL;
		}*/

		HANDLE hSerial = CreateFile(portName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if(hSerial == INVALID_HANDLE_VALUE)
		{
			if(GetLastError() == ERROR_FILE_NOT_FOUND)
			{
				throw DeviceException("Port unavailable");
			}
			else
			{
				throw DeviceException("Opening serial port error");
				//throw DeviceException("Error: Unknown error");
			}
		}

		DCB params = {0};
		//memset(&params, 0, sizeof(params));

		if(!GetCommState(hSerial, &params))
		{
			throw DeviceException("Getting serial parameters error.");
		}

		params.BaudRate    = baudRate;
		params.ByteSize    = byteSize;
		params.StopBits    = stopBits;
		params.Parity      = parity;
		params.fDtrControl = flowControl;

		if(!SetCommState(hSerial, &params))
		{
			throw DeviceException("Setting serial parameters error.");
		}

		PurgeComm(hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR);
		Sleep(SERIAL_PURGE_WAIT_TIME);

		memcpy(&m_params, &params, sizeof(m_params));
		m_hSerial = hSerial;
	}
	catch(DeviceException& ex)
	{
		throw;
	}
}

void WinSerialDevice::Close()
{
	try
	{
		if(m_hSerial)
		{
			if(!CloseHandle(m_hSerial))
			{
				throw DeviceException("Closing serial port error");
			}
			memset(&m_params, 0, sizeof(m_params));
			m_hSerial = NULL;
		}
	}
	catch(DeviceException& ex)
	{
		throw;
	}
}