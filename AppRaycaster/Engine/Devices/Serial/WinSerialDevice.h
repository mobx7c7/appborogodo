#pragma once
#include "SerialDevice.h"
#include <Windows.h>

#define SERIAL_PURGE_WAIT_TIME 250//250

class WinSerialDevice : public SerialDevice
{
private:

	HANDLE	m_hReadThread;
	HANDLE  m_hSerial;
    COMSTAT m_status;
    DWORD   m_errors;
	DCB		m_params;

	static DWORD WINAPI __Callback__(LPVOID lpParam);
	DWORD	Write(const void* data, unsigned size);
	DWORD	Read(void* data, unsigned size);
	DWORD	Peek();
	void	Flush();
	void	Create(const char* portName, int baudRate, int byteSize, int parity, int stopBits, int flowControl);
	void	Close();

public:

	WinSerialDevice();
	~WinSerialDevice();
	void	Initialize();
	void	Terminate();
	void	Start();
	void	Stop();

};
