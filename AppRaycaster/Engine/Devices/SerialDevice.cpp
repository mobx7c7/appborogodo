#include "SerialDevice.h"
#include "Globals.h"
#include "Serial\WinSerialDevice.h"
#include "Serial\NullSerialDevice.h"

SerialDevice::SerialDevice(std::string system_name) 
	: Device("Serial", system_name)
{}

SerialDevice::~SerialDevice()
{}

Device* SerialDevice::CreateCompatibleDevice()
{
#if !defined _WIN32 || defined NULL_DEVICE_SERIAL
	return new NullSerialDevice();
#else
	return new WinSerialDevice();
#endif
}