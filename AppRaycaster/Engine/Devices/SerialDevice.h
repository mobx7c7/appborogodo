#pragma once
#include "Device.h"

class SerialDevice : public Device
{
protected:
	SerialDevice(std::string system_name);
public:
	virtual ~SerialDevice();
	static Device* CreateCompatibleDevice();
};
