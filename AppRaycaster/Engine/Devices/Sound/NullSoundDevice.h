#pragma once
#include "SoundDevice.h"

class NullSoundDevice : public SoundDevice
{
public:
	NullSoundDevice();
	~NullSoundDevice();
};