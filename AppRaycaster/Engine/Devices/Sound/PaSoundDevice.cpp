#include "PaSoundDevice.h"
#include "Globals.h"
#include "DeviceException.h"
#include "App.h"
#include <memory>
#include <iostream>

int PaSoundDevice::SoundCallback(const void *ibuf, void* obuf, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
{
	PaSoundDevice& sndDevice = *(PaSoundDevice*)userData;
	PaStreamParameters& _iParams = sndDevice._iParams;
	PaStreamParameters& _oParams = sndDevice._oParams;
	int& _sampleRate = sndDevice._sampleRate;

	float* iptr_cur = (float*)ibuf;
	//float* iptr_end = iptr_cur + (_iParams.channelCount * (framesPerBuffer-1));
	float* optr_cur = (float*)obuf;
	//float* optr_end = optr_cur + (_oParams.channelCount * (framesPerBuffer-1));

	//////////////////////////////////////////////////////////////
	/*
	while(iptr_cur < iptr_end)
	{
		while(optr_cur < optr_end)
		{
			*optr_cur++ = c < _iParams.channelCount ? *iptr_cur++ : 0;
		}
	}
	*/

	// TODO: Aplicar acelera��o com SIMD ou OpenCL na quest�o dos buffers

	int i, c;

	for(i=0; i < framesPerBuffer; i++)
	{
		for(c=0; c < _oParams.channelCount; c++)
		{
			*optr_cur++ = c < _iParams.channelCount ? *iptr_cur++ : 0;
		}
	}

	//////////////////////////////////////////////////////////////

	if(App::GetCurrent().GetStatus() == AppStatus::STATUS_RUNNING)
	{
		IApp* m_app = sndDevice.m_app;

		if(m_app)
		{
			m_app->SoundListener(obuf, _iParams.channelCount, _oParams.channelCount, framesPerBuffer, _sampleRate);
		}
	}

	return 0;
}

PaSoundDevice::PaSoundDevice()
	: SoundDevice(Pa_GetVersionText())
	, _sampleFormat(SND_FORMAT)
{
	memset(&_iParams, 0, sizeof(_iParams));
	memset(&_oParams, 0, sizeof(_oParams));
}

PaSoundDevice::~PaSoundDevice()
{}

void PaSoundDevice::Initialize()
{
	if((_paErr = Pa_Initialize()) != paNoError)
		throw DeviceException(Pa_GetErrorText(_paErr));

	Create(SND_ICHANNELS, SND_OCHANNELS, SND_SAMPLERATE, SND_BUFSIZE);

	SetStatus(DeviceStatus::DEVICE_STOPPED);
}

void PaSoundDevice::Terminate()
{
	if((_paErr = Pa_Terminate()) != paNoError)
		throw DeviceException(Pa_GetErrorText(_paErr));

	Pa_Sleep(100);

	SetStatus(DeviceStatus::DEVICE_IDLE);
}

void PaSoundDevice::Stop()
{
	if((_paErr = Pa_StopStream(_stream)) != paNoError)
		throw DeviceException(Pa_GetErrorText(_paErr));

	SetStatus(DeviceStatus::DEVICE_STOPPED);
}

void PaSoundDevice::Start()
{
	if((_paErr = Pa_StartStream(_stream)) != paNoError)
		throw DeviceException(Pa_GetErrorText(_paErr));

	SetStatus(DeviceStatus::DEVICE_RUNNING);
}

void PaSoundDevice::Create(int iChannels, int oChannels, int sampleRate, int bufferSize)
{
	PaDeviceIndex deviceIndex;
	const PaDeviceInfo* deviceInfo;

	//////////////////////////////////////////////////////////////

	deviceIndex = Pa_GetDefaultInputDevice();
	deviceInfo  = Pa_GetDeviceInfo(deviceIndex);

	PaStreamParameters iParams =
	{
		deviceIndex,
		iChannels,
		_sampleFormat,
		deviceInfo->defaultLowInputLatency,
		NULL,
	};

	//////////////////////////////////////////////////////////////

	deviceIndex  = Pa_GetDefaultOutputDevice();
	deviceInfo   = Pa_GetDeviceInfo(deviceIndex);

	PaStreamParameters oParams = 
	{
		deviceIndex,
		oChannels,
		_sampleFormat,
		deviceInfo->defaultLowOutputLatency,
		NULL,
	};

	//////////////////////////////////////////////////////////////

	if((_paErr = Pa_OpenStream(&_stream,
                     iChannels > 0 ? &iParams : 0,
                     oChannels > 0 ? &oParams : 0,
                     sampleRate, 
					 bufferSize,
					 0,//paClipOff,
                     SoundCallback,
                     this)) != paNoError)
	{
		throw DeviceException(Pa_GetErrorText(_paErr));
	}

	//////////////////////////////////////////////////////////////

	this->_iParams	= iParams;
	this->_oParams	= oParams;
	this->_sampleRate = sampleRate;
}

int PaSoundDevice::GetInputChannelCount()
{
	return _iParams.channelCount;
}

int PaSoundDevice::GetOutputChannelCount()
{
	return _oParams.channelCount;
}

int PaSoundDevice::GetSampleRate()
{
	return _sampleRate;
}

int PaSoundDevice::GetBufferSize()
{
	return _bufferSize;
}
