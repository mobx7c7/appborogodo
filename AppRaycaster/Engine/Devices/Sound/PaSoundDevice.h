#pragma once
#include "SoundDevice.h"
#include "portaudio.h"

class PaSoundDevice : public SoundDevice
{
private:

	PaError					_paErr;
	PaStream*				_stream;
	PaStreamParameters		_iParams;
	PaStreamParameters		_oParams;
	int						_sampleRate;
	int						_bufferSize;
	int						_sampleFormat;

	static int SoundCallback(const void *inputBuffer,
                             void* outputBuffer,
                             unsigned long framesPerBuffer,
                             const PaStreamCallbackTimeInfo* timeInfo,
                             PaStreamCallbackFlags statusFlags,
                             void *userData);

	void Create(int iChannels, int oChannels, int sampleRate, int bufferSize);

public:

	PaSoundDevice();
	~PaSoundDevice();

	int GetInputChannelCount();
	int GetOutputChannelCount();
	int GetSampleRate();
	int GetBufferSize();

	void Initialize();
	void Terminate();
	void Start();
	void Stop();

};
