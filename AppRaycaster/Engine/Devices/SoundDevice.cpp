#include "SoundDevice.h"
#include "Globals.h"
#include "Sound\PaSoundDevice.h"
#include "Sound\NullSoundDevice.h"

SoundDevice::SoundDevice(std::string system_name) 
	: Device("Sound", system_name)
{}

SoundDevice::~SoundDevice()
{}

Device* SoundDevice::CreateCompatibleDevice()
{
	// Nota: PaSoundDevice (Portaudio) � multi-plataforma!
#ifdef NULL_DEVICE_SOUND
	return new NullSoundDevice();
#else
	return new PaSoundDevice();
#endif
}