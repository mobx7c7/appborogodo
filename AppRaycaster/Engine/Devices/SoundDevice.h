#pragma once
#include "Device.h"

class SoundDevice : public Device
{
protected:
	SoundDevice(std::string system_name);
public:
	virtual ~SoundDevice();
	static Device* CreateCompatibleDevice();
};