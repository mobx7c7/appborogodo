#include "GDIWindow.h"
#include <exception>

#if defined WND_USE_WGL_POWER
#include "GL\glew.h"

void GDIWindow::__InitContextGL__()
{
	HDC    _hDC;
	HGLRC  _hGLRC;

	GLuint pixelFormat;

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));
	pfd.nSize       = sizeof(pfd);
	pfd.nVersion    = 1;
	pfd.dwFlags     = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType  = PFD_TYPE_RGBA;
	pfd.cColorBits  = 32;
	pfd.cDepthBits  = 24;
	pfd.iLayerType  = PFD_MAIN_PLANE;

	if(!(_hDC = GetDC(m_hWND)))
	{
		throw std::exception("Error GetDC");
	}

	if(!(pixelFormat = ChoosePixelFormat(_hDC, &pfd)))
	{
		throw std::exception("Error ChoosePixelFormat");
	}

	if(!SetPixelFormat(_hDC, pixelFormat, &pfd))
	{
		throw std::exception("Error SetPixelFormat");
	}

	if(!(_hGLRC = wglCreateContext(_hDC)))
	{
		throw std::exception("Error wglCreateContext");
	}

	wglMakeCurrent(_hDC, _hGLRC);

	//////////////////////////////////////////////////////////
	
	GLenum glewErr = glewInit();
	if(glewErr != GLEW_OK)
	{
		throw std::exception((char*)glewGetErrorString(glewErr));
	}

	//////////////////////////////////////////////////////////

	m_hDC	= _hDC;
	m_hGLRC	= _hGLRC;
}

void GDIWindow::__DestroyContextGL__()
{
	if(!wglMakeCurrent(m_hDC, NULL))
	{
		throw std::exception("Error wglMakeCurrent");
	}

	if(!wglDeleteContext(m_hGLRC))
	{
		throw std::exception("Error wglDeleteContext");
	}

	if(!ReleaseDC(m_hWND, m_hDC))
	{
		throw std::exception("Error ReleaseDC");
	}
}

#endif

const char GDIWindow::m_ClassName[] = "App01";

const char GDIWindow::m_DefAppTitle[] = "App";

GDIWindow::GDIWindow() 
	: WindowDevice("WGL")
{
#if defined WND_USE_WGL_POWER
	m_app		= NULL;
	m_hWND		= NULL;
	m_hGLRC		= NULL;
	m_hDC		= NULL;
#else
	m_app		= NULL;
	m_hWND		= NULL;
	m_hMemDC	= NULL;
	m_hOldBMP	= NULL;
#endif
}

GDIWindow::~GDIWindow()
{
	if(m_hWND)
	{
		Destroy();
	}
}

LRESULT CALLBACK GDIWindow::InitWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if(message == WM_NCCREATE) // Nonclient create
	{
        LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
        SetWindowLongPtr(hWnd, GWL_USERDATA, reinterpret_cast<LONG_PTR>(lpcs->lpCreateParams));
        return DefWindowProc(hWnd, message, wParam, lParam);
	}

	GDIWindow* thisPtr = reinterpret_cast<GDIWindow*>(GetWindowLongPtr(hWnd, GWL_USERDATA));
	return thisPtr->WndProc(hWnd, message, wParam, lParam);
}

LRESULT CALLBACK GDIWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if(!m_app)
		return -1;

	IApp* app = m_app;

	switch(message)
	{
	case WM_KEYDOWN:
		{
			app->OnKeyPress(wParam & 0xff);
			break;
		}
	case WM_KEYUP:
		{
			app->OnKeyRelease(wParam & 0xff);
			break;
		}
	case WM_CHAR:
		{
			app->OnKeyChar(wParam);
			break;
		}
	case WM_UNICHAR:
		{
			app->OnKeyUChar(wParam);
			break;
		}
	case WM_MOUSEMOVE:
		{
			app->OnMouseMove(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			break;
		}
	case WM_MOUSELEAVE:
		{
			app->OnMouseLeave();
			break;
		}
	//case WM_MOUSEACTIVATE:
	//	{
	//		app->OnMouseActivate();
	//		break;
	//	}
	case WM_LBUTTONDOWN:
		{
			app->OnMousePress(0);
			break;
		}
	case WM_MBUTTONDOWN:
		{
			app->OnMousePress(1);
			break;
		}
	case WM_RBUTTONDOWN:
		{
			app->OnMousePress(2);
			break;
		}
	case WM_XBUTTONDOWN:
		{
			app->OnMousePress((int)(short)HIWORD(wParam));
			break;
		}
	case WM_LBUTTONUP:
		{
			app->OnMouseRelease(0);
			break;
		}
	case WM_MBUTTONUP:
		{
			app->OnMouseRelease(1);
			break;
		}
	case WM_RBUTTONUP:
		{
			app->OnMouseRelease(2);
			break;
		}
	case WM_XBUTTONUP:
		{
			app->OnMouseRelease((int)(short)HIWORD(wParam));
			break;
		}
	case WM_MOUSEWHEEL:
		{
			app->OnMouseScroll((short)HIWORD(wParam) >= WHEEL_DELTA ? 1 : -1);
			break;
		}
	case WM_ACTIVATE:
		{
			switch(LOWORD(wParam))
			{
			case WA_ACTIVE:
				{
					//m_current = this;
					app->OnWindowFocus();
					break;
				}
			//case WA_CLICKACTIVE: // mesmo que WM_MOUSEACTIVATE
			//case WA_INACTIVE:
			}
			break;
		}
	case WM_SIZE:
		{
			switch(wParam)
			{
			case SIZE_RESTORED:
			case SIZE_MAXIMIZED:
			case SIZE_MINIMIZED:
				app->OnWindowResize((int)(short)LOWORD(lParam),(int)(short)HIWORD(lParam));
				break;
			}
		}
	case WM_MOVE:
		{
			app->OnWindowMove((int)(short)LOWORD(lParam),(int)(short)HIWORD(lParam));
			break;
		}
	case WM_CLOSE:
		{
			app->OnWindowClose();
			ShowWindow(hWnd, SW_HIDE);
			break;
		}
	case WM_PAINT:
		{
			
			break;
		}
	case WM_CREATE:
		{
			// Sendo usado em App::Create
			//m_app->OnCreate();
			break;
		}
	case WM_DESTROY:
		{
			break;
		}
	default:
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}

	return 0;
}

void GDIWindow::__CreateWindow__(int width, int height)
{
	if(m_hWND != NULL)
		throw std::exception("m_hWND != NULL");

	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.style			= CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= InitWndProc;
	wc.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hInstance		= GetModuleHandle(NULL);
	wc.hbrBackground	= CreateSolidBrush(RGB(0,0,0));
	//wc.hbrBackground	= (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName	= m_ClassName;

	if(!RegisterClass(&wc))
		throw std::exception("Error RegisterClass");

	////////////////////////////////////////////////////////////////////
#if defined WND_FULLSCREEN == 1

	DEVMODE dm;
	memset(&dm, 0, sizeof(dm));

	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);
	dm.dmPelsWidth	= width;
	dm.dmPelsHeight	= height;
	dm.dmFields     = DM_PELSWIDTH | DM_PELSHEIGHT;

	ChangeDisplaySettings(&dm, CDS_FULLSCREEN);

	HWND hWnd = CreateWindow(m_ClassName, 
							 m_DefAppTitle, 
							 WS_VISIBLE | WS_POPUP,
							 0, 
							 0, 
							 width, 
							 height, 
							 NULL, 
							 NULL, 
							 wc.hInstance, 
							 this);
#else

	////////////////////////////////////////////////////////////////////

	DWORD wndStyle = WS_OVERLAPPEDWINDOW;

	POINT wndPos;
	wndPos.x = (GetSystemMetrics(SM_CXSCREEN) - width) / 2; 
	wndPos.y = (GetSystemMetrics(SM_CYSCREEN) - height) / 2;

	RECT wndArea = {0, 0, width, height};
	AdjustWindowRect(&wndArea, wndStyle, 0);

	HWND hWnd = CreateWindow(m_ClassName, 
							 m_DefAppTitle, 
							 wndStyle,
							 wndPos.x,
							 wndPos.y,
							 wndArea.right - wndArea.left, 
							 wndArea.bottom - wndArea.top, 
							 NULL, 
							 NULL, 
							 wc.hInstance, 
							 this); 
#endif

	if(!hWnd)
		throw std::exception("Error CreateWindow");

#if defined WND_USE_WGL_POWER
	m_hWND = hWnd;
	__InitContextGL__();
	UpdateWindow(hWnd);
#else
	m_hWND = hWnd;
	UpdateWindow(hWnd);
#endif
}

void GDIWindow::__DestroyWindow__()
{
#if defined WND_USE_WGL_POWER
	__DestroyContextGL__();
#endif

	if(!DestroyWindow(m_hWND))
		throw std::exception("Error DestroyWindow");

	if(!UnregisterClass(m_ClassName, GetModuleHandle(NULL)))
		throw std::exception("Error UnregisterClass");
}

void GDIWindow::Create(int width, int height, const char* title)
{
	__CreateWindow__(width, height);
	SetTitle(title);

#if defined WND_USE_WGL_POWER
	// Vazio!
#else
	RECT rect;
	GetClientRect(m_hWND, &rect);
	m_fbuf	= new Framebuffer*[2];
	m_fbuf[0] = new Framebuffer(m_hWND, rect.right-rect.left, rect.bottom-rect.top);
	m_fbuf[1] = NULL;

	// NOTA: usados para trasnferir do Framebuffer para janela
	HDC hDC	= GetDC(m_hWND);
	m_hMemDC = CreateCompatibleDC(hDC);
	m_hOldBMP = (HBITMAP)SelectObject(m_hMemDC, (HBITMAP)m_fbuf[0]->GetHandler());
	DeleteDC(hDC);
#endif;
}

void GDIWindow::Destroy()
{
#if defined WND_USE_WGL_POWER
	__DestroyWindow__();
#else
	SelectObject(m_hMemDC, m_hOldBMP);
	DeleteDC(m_hMemDC);
	__DestroyWindow__();
	delete m_fbuf[0];
	delete[] m_fbuf;
#endif
}

void GDIWindow::Update()
{
	m_app->OnRender();
#if defined WND_USE_WGL_POWER

	SwapBuffers(m_hDC);

#else
	Framebuffer& fbuf = GetFramebuffer();
	BitBlt(GetDC(m_hWND), 0, 0, fbuf.GetWidth(), fbuf.GetHeight(), m_hMemDC, 0, 0, SRCCOPY);
	//if(!UpdateWindow(m_hWND))
		//throw std::exception("Update error");
#endif
}

void GDIWindow::Show()
{
	ShowWindow(m_hWND, SW_SHOW);
}

void GDIWindow::Hide()
{
	ShowWindow(m_hWND, SW_HIDE);
}

void GDIWindow::SetTitle(const char* title)
{
	if(!SetWindowText(m_hWND, title))
		throw std::exception("Error SetTitle");
}

HWND GDIWindow::GetHandler()
{
	return m_hWND;
}

void GDIWindow::SetFramebuffer(Framebuffer* fbuf)
{
	//throw std::exception("Falta implementar");
}

void GDIWindow::SetInterface(IApp* app)
{
	m_app = app;
}

Framebuffer& GDIWindow::GetFramebuffer()
{
#if defined WND_USE_WGL_POWER
	return *(Framebuffer*)nullptr;
#else
	return *(m_fbuf[1] == NULL ? m_fbuf[0] : m_fbuf[1]);
#endif
}
