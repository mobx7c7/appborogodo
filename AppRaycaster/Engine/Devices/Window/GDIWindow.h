#pragma once
#include <Windows.h>
#include <windowsx.h>
#include "WindowDevice.h"
#include "Globals.h"
#include "Framebuffer.h"
#include "IApp.h"

class GDIWindow : public WindowDevice
{
private:

	static const char m_ClassName[]; // Win
	static const char m_DefAppTitle[]; // Win
	static LRESULT CALLBACK InitWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam); // Win
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam); // Win

#if defined WND_USE_WGL_POWER

	HWND	m_hWND;
	HGLRC	m_hGLRC;
	HDC		m_hDC;
	IApp*	m_app;
	void	__InitContextGL__();
	void	__DestroyContextGL__();

#else

	HWND			m_hWND; // Win
	HDC				m_hMemDC; // Win
	HBITMAP			m_hOldBMP; // Win
	Framebuffer**	m_fbuf;
	IApp*			m_app;

#endif
	
	void __CreateWindow__(int width, int height);
	void __DestroyWindow__();

public:

	GDIWindow();
	~GDIWindow();

	void			SetTitle(const char* title); // GDIWindow
	void			Create(int width, int height, const char* title);  // GDIWindow
	void			Destroy();  // GDIWindow
	void			Update();
	void			Show();
	void			Hide();
	void			SetInterface(IApp* app);
	void			SetFramebuffer(Framebuffer* fbuf);
	Framebuffer&	GetFramebuffer();
	HWND			GetHandler(); // Win

};
