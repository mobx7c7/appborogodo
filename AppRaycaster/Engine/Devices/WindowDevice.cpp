#include "WindowDevice.h"
#include "Window\GDIWindow.h"
#include "Window\WGLWindow.h"
#include "Window\NullWindow.h"
#include "Globals.h"

WindowDevice::WindowDevice(std::string system_name) 
	: Device("Window", system_name)
{}

WindowDevice::~WindowDevice()
{}

Device* WindowDevice::CreateCompatibleDevice()
{
#ifdef NULL_DEVICE_WINDOW
	return new NullWindow();
#else
	return new GDIWindow();
#endif
	
}