#pragma once
#include "Device.h"

class WindowDevice : public Device
{
protected:
	WindowDevice(std::string system_name);
public:
	virtual ~WindowDevice();
	static Device* CreateCompatibleDevice();
};