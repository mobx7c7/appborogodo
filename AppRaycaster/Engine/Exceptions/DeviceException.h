#pragma once

#include <exception>
#include <iostream>

class DeviceException : public std::exception
{
public:
	DeviceException(const char* msg) : std::exception(msg){}
};
