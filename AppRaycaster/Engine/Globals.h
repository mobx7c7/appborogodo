#pragma once

// FIXME: Substituir por variaveis carregadas pelo App
#define APP_NAME			"AppBorogodo"
#define WND_TiTLE			APP_NAME
#define WND_WIDTH			1280
#define WND_HEIGHT			720
#define SND_FORMAT			1 // paFloat32 == 1, paInt16 == 8
#define SND_ICHANNELS		2
#define SND_OCHANNELS		2
#define SND_SAMPLERATE		192000
#define SND_BUFSIZE			64
#define MIDI_DEVICE_INDEX	1
#define SERIAL_PORTNAME		"COM7" // 07/02/2016: Arduino est� instalado como COM7 no desktop
#define SERIAL_BAUDRATE		9600 // CBR_9600
#define SERIAL_BYTESIZE		8
#define SERIAL_PARITY		0 // NOPARITY
#define SERIAL_STOPBITS		0 // ONESTOPBIT
#define SERIAL_FLOWCTRL		1 // DTR_CONTROL_ENABLE
#define SERIAL_BUFSIZE		64 // bytes

// NOTA: Flags abaixo servem para tirar elementos da compila��o!
//#define WND_FULLSCREEN
//#define NULL_DEVICE_SOUND
#define NULL_DEVICE_SERIAL
//#define NULL_DEVICE_MIDI
#define NULL_DEVICE_NETWORK
//#define NULL_DEVICE_WINDOW
#define WND_USE_WGL_POWER