#include "IApp.h"

IApp::IApp()
{

}

IApp::IApp(const IApp&)
{

}

IApp::~IApp()
{

}

void IApp::OnCreate()
{

}

void IApp::OnStart()
{

}

void IApp::OnTerminate()
{

}

void IApp::OnUpdate()
{

}

void IApp::OnRender()
{

}

void IApp::OnKeyPress(int key)
{

}

void IApp::OnKeyRelease(int key)
{

}

void IApp::OnKeyChar(char c)
{

}

void IApp::OnKeyUChar(short c)
{

}

void IApp::OnMousePress(int button)
{

}

void IApp::OnMouseRelease(int button)
{

}

void IApp::OnMouseMove(int x, int y)
{

}

void IApp::OnMouseScroll(int delta)
{

}

void IApp::OnMouseLeave()
{

}
/*
void IApp::OnMouseActivate()
{

}
*/
void IApp::OnWindowClose()
{

}

void IApp::OnWindowFocus()
{

}

void IApp::OnWindowMove(int x, int y)
{

}

void IApp::OnWindowResize(int w, int h)
{

}

void IApp::SoundListener(void* data, int ichn, int ochn, int size, int rate)
{

}

void IApp::SerialListener(unsigned char* data, int size)
{

}

void IApp::MIDIListener(unsigned char* data, int size)
{

}