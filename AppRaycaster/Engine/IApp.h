#pragma once
//#include <vector>

//typedef std::vector<IApp*> IAppVector;

class IApp
{
private:

	//IAppVector m_apps;

protected:

	IApp();
	IApp(const IApp&);

	/*
	void AttachInterface(IApp* app)
	{
		m_apps.push_back(app);
	}

	void DetachInterface(IApp* app)
	{
		m_apps.
	}
	*/
public:

	virtual ~IApp();

	virtual void OnCreate(); // na cria��o da janela
	virtual void OnStart(); // na execu��o do loop
	virtual void OnTerminate();
	virtual void OnUpdate(); // tempo fixo
	virtual void OnRender(); // no paint

	virtual void OnKeyPress(int key);
	virtual void OnKeyRelease(int key);
	virtual void OnKeyChar(char c);
	virtual void OnKeyUChar(short c);

	virtual void OnMousePress(int button);
	virtual void OnMouseRelease(int button);
	virtual void OnMouseMove(int x, int y);
	virtual void OnMouseScroll(int delta);
	virtual void OnMouseLeave();
	//virtual void OnMouseActivate(); // bom para atualizar recursos quando voltar ao programa (ex: shader)

	virtual void OnWindowClose();
	virtual void OnWindowFocus();
	virtual void OnWindowMove(int x, int y);
	virtual void OnWindowResize(int w, int h);

	virtual void SoundListener(void* data, int ichn, int ochn, int size, int rate);
	virtual void SerialListener(unsigned char* data, int size);
	virtual void MIDIListener(unsigned char* data, int size);

	// CANDIDATOS:

	//virtual void SoundDeviceCallback(void* buf, int siz, int chn);
	//virtual void SoundDeviceParams();
	//virtual void SoundDeviceStop();
	//virtual void SoundDeviceStart();

	//virtual void SoundDeviceCallback(void* buf, int siz, int chn);
	//virtual void SoundDeviceEvent();

	//virtual void NetworkListener(void* buf, int size){}
};

