#pragma once

// Especificação MIDI:
// https://www.midi.org/specifications/item/table-1-summary-of-midi-message

#define GET_MIDI_MESSAGE(x)		x & 0xf0
#define GET_MIDI_STATUS(x)		x & 0xf
#define GET_MIDI_SYSTEM_TYPE(x) x >> 3 & 1

enum MIDIMsg
{
	MIDI_MSG_KEY_OFF		= 128,
	MIDI_MSG_KEY_ON			= 144,
	MIDI_MSG_KEY_PRESS		= 160,
	MIDI_MSG_CTRL_CHANGE	= 176,
	MIDI_MSG_PROG_CHANGE	= 192,
	MIDI_MSG_CHN_PRESS		= 208,
	MIDI_MSG_PITCH_BEND		= 224,
	MIDI_MSG_SYSTEM			= 240,
};

enum MIDISysType
{
	MIDI_SYS_COMMON,
	MIDI_SYS_REALTIME
};

enum MIDISysMsg
{
	// TODO: Colocar valores de mensagens para sistema (Ver especificação!)
};

/*
#include <memory>

class MIDIMessage
{
private:
unsigned char* m_data;
int		m_size;
public:
MIDIMessage(unsigned char* data, int size) : m_data(0)
{
if(size > 0)
{
m_size = size;
m_data = new unsigned char[size];
memcpy(m_data, data, size);
}
}
~MIDIMessage()
{
if(m_data)
{
delete[] m_data;
}
}
bool IsKeyOff()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) == MIDIStatus::MIDI_KEY_OFF : false;
}
bool IsKeyOn()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) == MIDIStatus::MIDI_KEY_ON : false;
}
bool IsControlChange()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) == MIDIStatus::MIDI_CONTROL_CHANGE: false;
}
bool IsProgramChange()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) == MIDIStatus::MIDI_PROGRAM_CHANGE : false;
}
bool IsPitchBend()
{
return m_data ? GET_MIDI_STATUS(m_data[0])== MIDIStatus::MIDI_PITCH_BEND : false;
}
bool IsSysMessage()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) == MIDIStatus::MIDI_SYSTEM : false;
}
unsigned char GetStatus()
{
return m_data ? GET_MIDI_STATUS(m_data[0]) : 0;
}
unsigned char GetData1()
{
return m_data && m_size > 1 ? m_data[1] : 0;
}
unsigned char GetData2()
{
return m_data && m_size > 2 ? m_data[2] : 0;
}
int GetData(char* data, int size)
{
if(m_data)
{
int _size = size >= m_size ? m_size : size;
memcpy(data, m_data, _size);
return _size;
}
return 0;
}
};
*/