#pragma once

typedef unsigned char	uByte;
typedef unsigned char	uShort;
typedef unsigned int	uInt32;
typedef unsigned long	uInt64;
typedef char			Byte;
typedef short			Short;
typedef int				Int32;
typedef long			Int64;