#include "HarmoSynth.h"
#include "Math\MathEx.h"

HarmoSynth::HarmoSynth(void) : oscs(6)
{
}


HarmoSynth::~HarmoSynth(void)
{
}

void HarmoSynth::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	float* wave = (float*)buf;

	for(int i=0; i<size; i++)
	{
		for(Oscillator& osc : oscs)
		{
			osc.Update((double)rate, 440.0);
		}

		float s = sin(M_PI2 * oscs[0].Theta(0)) * 0.25f;

		*wave++ += s;
		*wave++ += s;
	}
}

void HarmoSynth::MIDIListener(unsigned char* data, int size)
{
		
}