#pragma once

#include "Oscillator.h"
#include <vector>

class HarmoSynth
{
private:

	std::vector<Oscillator> oscs;

public:

	HarmoSynth(void);
	~HarmoSynth(void);
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);
	void MIDIListener(unsigned char* data, int size);

};

