#pragma once

class DSPModule
{
private:

	DSPModule(const DSPModule&){}

protected:

	DSPModule(){}

public:

	virtual ~DSPModule(){}

};