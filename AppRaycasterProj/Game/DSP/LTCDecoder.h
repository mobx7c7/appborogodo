#pragma once

#include "LinearTimecode.h"
#include "Oscillator.h"

class LPF
{
private:
	float k1, k2;
public:
	LPF()
	{
		k1 = k2 = 0.0f;
	}
	float Process(float signal)
	{
		k1 = signal + k2;
		k2 = signal;
		return k1;
	}
};

class PLL
{
	// Compara dois sinais
	// Gera um sinal de corre��o (diferen�a de fase)
};

class LTCDecoder
{
private:
	
	Oscillator osc;

	LPF lpf;

	float	m_min_threshold;
	float	m_max_threshold;
	int		m_holdSamples;
	int		m_sampleCount;
	int		m_samples;
	bool	m_capture;
	bool	m_sync;

	int m_clock, m_state;
	//int proxMsg, ultimaMsg, msgSamplesCount;

	int CheckState(float wave);
	//int CheckTolerance(int sampleCount)
	//void ChechSync();

public:

	LTCDecoder();
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);

};