#include "LTCEncoder.h"
#include <string>

void LTCEncoder::__PrintDecodeBits__(const unsigned char *data)
{
	std::string bytes("");
	std::string bits("");

	int begin = 0;
	int end	= 80;

	for(int i=begin; i<end; i++)
	{
		int byteShift = i/8;
		int bitShift = i%8;

		if(bitShift == 0 && i!=0)
		{
			bytes += ' ';
			bits += ' ';
		}

		bytes += '0' + byteShift;
		bits += '0' + (m_data[byteShift] >> bitShift & 0x1);
	}

	std::string dec("");

	// mascara bits:
	// 0x3 = 2 bits
	// 0x7 = 3 bits
	// 0xf = 4

	dec += '0' + (m_data[0] & 0xf);
	dec += "   ";
	dec += '0' + (m_data[0] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[1] & 0x3);
	dec += " ";
	dec += '0' + (m_data[1] >> 2 & 0x3);
	dec += " ";
	dec += '0' + (m_data[1] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[2] & 0xf);
	dec += "   ";
	dec += '0' + (m_data[2] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[3] & 0x7);
	dec += " ";
	dec += '0' + (m_data[3] >> 3 & 0x1);
	dec += " ";
	dec += '0' + (m_data[3] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[4] & 0xf);
	dec += "   ";
	dec += '0' + (m_data[4] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[5] & 0x7);
	dec += "  ";
	dec += '0' + (m_data[5] >> 3 & 0x1);
	dec += '0' + (m_data[5] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[6] & 0xf);
	dec += "   ";
	dec += '0' + (m_data[6] >> 4 & 0xf);
	dec += "   ";

	dec += " ";

	dec += '0' + (m_data[7] & 0x3);
	dec += std::string(1, ' ');
	dec += '0' + (m_data[7] >> 2 & 0x1);
	dec += '0' + (m_data[7] >> 3 & 0x1);
	dec += '0' + (m_data[7] >> 4 & 0xf);
	dec += std::string(3, ' ');

	dec += " ";

	dec += "-------- --------";

	//std::cout << bytes << std::endl;
	std::cout << bits << std::endl;
	std::cout << dec << std::endl;


	std::cout << std::string(4, '-').assign("a",0,2) << std::endl;
}

char LTCEncoder::__RevertBits__(char c)
{
	return	((c & 0x80) >> 8) | 
		((c & 0x40) >> 7) | 
		((c & 0x20) >> 6) | 
		((c & 0x10) >> 5) | 
		((c & 0x08) >> 4) | 
		((c & 0x04) >> 3) | 
		((c & 0x02) >> 2) | 
		((c & 0x01) >> 1);
}

void LTCEncoder::Encode(const Timecode& tc)
{
	Encode(tc.hour, tc.minute, tc.second, tc.frame);
}

void LTCEncoder::Encode(const long frame)
{
	int secs	= frame/m_framesPerSec;
	int mins	= secs/60;
	int hours	= mins/3600;
	Encode(hours,mins,secs,frame);
}

void LTCEncoder::Encode(int h, int m, int s, int f)
{
	h %= 24;
	m %= 60;
	s %= 60;
	f %= m_framesPerSec;

	int fUnit = f%10;	// 0-9
	int fBase = (f/10)%3; // 0-2
	int sUnit = s%10;	// 0-9
	int sBase = (s/10)%6; // 0-5
	int mUnit = m%10;	// 0-9
	int mBase = (m/10)%6; // 0-5
	int hUnit = h%10;	// 0-9
	int hBase = (h/10)%3; // 0-2

	//memset(data, 0, 10);

	// TODO: Incrementar as flags restantes!
	// esq: tempo, meio: bits userData, dir: restante
	m_data[0] = (fUnit & 0xf) | (0 << 4 & 0xf);
	m_data[1] = (fBase & 0x3) | (0 << 4 & 0xf) | (0 << 2 & 0x3);
	m_data[2] = (sUnit & 0xf) | (0 << 4 & 0xf);
	m_data[3] = (sBase & 0x7) | (0 << 4 & 0xf) | (0 << 3 & 0x1);
	m_data[4] = (mUnit & 0xf) | (0 << 4 & 0xf);
	m_data[5] = (mBase & 0x7) | (0 << 4 & 0xf) | (0 << 3 & 0x1);
	m_data[6] = (hUnit & 0xf) | (0 << 4 & 0xf);
	m_data[7] = (hBase & 0x3) | (0 << 4 & 0xf) | (0 << 2 & 0x1) | (0 << 3 & 0x1);
	// NOTA: sinal de sincronismo (sync word)
	// Bits encontram-se invertido!
	// Por isso � 0xFCBF(lsb) e n�o 0x3FFC(msb) (Ser� que estou certo?)
	// FIXME: Tornar sync em constante!
	m_data[8] = 0xfc;
	m_data[9] = 0xbf;
}

void LTCEncoder::Decode(Timecode& tc)
{
	tc.frame	= (m_data[0] & 0xf) + 10*(m_data[1] & 0x3);
	tc.second	= (m_data[2] & 0xf) + 10*(m_data[3] & 0xf);
	tc.minute	= (m_data[4] & 0xf) + 10*(m_data[5] & 0xf);
	tc.hour		= (m_data[6] & 0xf) + 10*(m_data[7] & 0x3);
}

LTCEncoder::LTCEncoder() 
{
	m_bitsPerFrame	= 80; // 80 bits = 10 bytes!
	m_framesPerSec	= 30;
	m_frameCount	= 0;
	m_sampleCount	= 0;
	m_clockState	= 0;
	m_bitShift		= 0;
	Encode(0);
}

void LTCEncoder::SoundListener(void* buf, int ichn, int ochn, int size, int sampleRate)
{
	m_wave = (float*)buf;

	for(int i=0; i<size; i++)
	{
		switch(-1)
		{
		default:
			m_state = (float)((m_data[m_bitShift/8] >> (m_bitShift%8) & 1) && !m_clockState);
			break;
		case 0:
			m_state = (float)(m_data[m_bitShift/8] >> (m_bitShift%8) & 1);
			break;
		case 1:
			m_state = (float)(m_clockState);
			break;
		}

		//////////////////////////////////////

		switch(-1)
		{
		default: // Bruto
			*m_wave++ += (((m_data[m_bitShift/8] >> (m_bitShift%8) & 1)-0.5f)*2.0f) * m_clockState;
			*m_wave++ += 0.0f;//(float)(m_clockState);
			break;
		case 0: // Bruto + Modula��o FM
			// 1.2Khz = bit 0
			// 2.4Khz = bit 1
			osc.Update((double)sampleRate, 1200.0 + 1200.0 * m_state);
			*m_wave++ += sin(osc.Theta(0)*M_PI2);//*0.5f;
			*m_wave++ += 0.0f;//*m_wave;
			break;
		case 1: // Oscilosc�pio: Bits
			*m_wave++ += (m_state-0.5f)*2.0f;
			*m_wave++ += -1.0f*(((m_sampleCount%m_frameInterval)/(float)m_frameInterval)-0.5f)*2.0f;//state * 0.5f;
			break;
		case 2: // Oscilosc�pio: Ondas sonoras
			*m_wave++ = (m_wave[0] + m_wave[1])/2;
			*m_wave++ = -1.0f*(((m_sampleCount%m_frameInterval)/(float)m_frameInterval)-0.5f)*2.0f;//state * 0.5f;
			break;
		}

		//////////////////////////////////////

		Encode(m_frameCount);

		//////////////////////////////////////

		// Intervalo de quadro dentro de segundo de audio
		m_frameInterval = sampleRate / m_framesPerSec;
		// Intervalo bin�rio dentro de quadro
		m_bitInterval	= m_frameInterval / m_bitsPerFrame;
		// Encoding BMC (2x bitrate)
		m_clockInterval = m_frameInterval / (m_bitsPerFrame * 2);
		///m_clockInterval = m_bitInterval * 2;

		//////////////////////////////////////

		m_sampleCount++;
		long sampleCount = (long)m_sampleCount;//*(0.1f);//0.001f;// / 2048;// / 128;// / 2;
		m_frameCount	= (sampleCount/m_frameInterval);
		m_clockState	= (sampleCount/m_clockInterval) % 2; // Clock BMC
		m_bitShift		= (sampleCount/m_bitInterval) % m_bitsPerFrame;

		//////////////////////////////////////

		if(m_sampleCount % m_frameInterval == 0) // A cada quadro
		{
			Timecode tc;
			Decode(tc);
			//std::cout << " " << tc << std::endl;
		}
	}
}