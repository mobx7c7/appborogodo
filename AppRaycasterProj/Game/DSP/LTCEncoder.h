#pragma once

#include "LinearTimecode.h"
#include "Oscillator.h"

class LTCEncoder
{
private:

	Oscillator osc;

	void __PrintDecodeBits__(const unsigned char *data);
	char __RevertBits__(char c);

	void Encode(const Timecode& tc);
	void Encode(const long frame);
	void Encode(int h, int m, int s, int f);
	void Decode(Timecode& tc);
	
	unsigned char m_data[10]; // 80 bits = 10 bytes!
	long	m_bitsPerFrame;
	long	m_framesPerSec;
	long	m_frameInterval;
	long	m_clockInterval;
	long	m_bitInterval;
	long	m_frameCount;
	long	m_sampleCount;
	long	m_clockState;
	long	m_bitShift;

	float *m_wave, m_state;

public:
	
	LTCEncoder();
	void SoundListener(void* buf, int ichn, int ochn, int size, int sampleRate);

};
