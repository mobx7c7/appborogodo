#pragma once

//#include <memory>
#include <iostream>
#include <iomanip>
//#include <string>
//#include "..\..\Common\MathEx.h"

class Timecode
{
public:
	int hour;
	int minute;
	int second;
	int frame;

	char userData[4];

	friend std::ostream& operator<<(std::ostream& os, const Timecode& tc)
	{
		return os << std::setfill('0') << std::setw(2) << tc.hour << ":" << std::setw(2) << tc.minute << ":" << std::setw(2) << tc.second << ":" << std::setw(2) << tc.frame;
	}
};

/*
	Timecode: 80 bits, lsb;

	msb & lsb:
	[M][-][-][-][-][-][-][L]
	[0][0][0][0][0][0][0][0]

	lsb = entao vai ser [sync -> frames]
	msb = entao vai ser [frames -> sync]

	Fluxo de bits 	:[0000|0000|00|00|0000|0000|0000|000|0|0000|0000|0000|000|0|0000|0000|0000|00|0|0|0000|0000000000000000]
	Legenda			:[AAAA|BBBB|CC|DD|EEEE|FFFF|GGGG|HHH|I|JJJJ|KKKK|LLLL|MMM|N|OOOO|PPPP|QQQQ|RR|S|T|UUUU|VVVVVVVVVVVVVVVV]

	a = frame number unit (0-9)
	b = user bits field 1

	c = frame number base 10 (0-2)
	d = drop frame flag
	e = user bits field 2

	f = seconds unit (0-9)
	g = user bits field 3

	h = seconds base 10 (0-5)
	i = even parity
	j = user bits field 4

	k = minutes unit (0-9)
	l = user bits field 5

	m = minutes base 10 (0-5)
	n = binary group flag
	o = user bits field 6

	p = hours unit (0-9)
	q = user bits field 7

	r = hours base 10 (0-2)
	s = reserved, zero
	t = binary group flag
	u = user bits field 8

	v = sync word (value: 0011111111111101)
*/
