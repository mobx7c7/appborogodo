#pragma once

#include "Math\MathEx.h"

class Oscillator
{
private:

	double theta;

public:

	Oscillator() : theta(0.0f){}

	~Oscillator(){}

	double Theta(double phase)
	{
		double t = theta + phase;
		return t - floor(t);
	}

	void Update(double sampleRate, double frequency)
	{
		theta += (frequency / sampleRate) - floor(theta);
	}

};