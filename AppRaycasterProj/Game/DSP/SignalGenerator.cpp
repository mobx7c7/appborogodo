#include "SignalGenerator.h"

SignalGenerator::SignalGenerator(void) : re(rd()), rnd(-1, 1)
{
}

SignalGenerator::~SignalGenerator(void)
{
}

void SignalGenerator::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	float* wave = (float*)buf;

	for(int i=0; i<size; i++)
	{
		for(int c=0; c<ochn; c++)
		{
			*wave++ += rnd(re);// * 0.01f;
		}
	}
}
