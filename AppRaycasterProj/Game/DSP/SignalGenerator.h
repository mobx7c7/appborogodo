#pragma once

#include "Oscillator.h"
#include <random>

class SignalGenerator
{
private:
	Oscillator osc;
	std::random_device rd;
	std::default_random_engine re;
	std::uniform_real_distribution<float> rnd;
public:
	SignalGenerator(void);
	~SignalGenerator(void);
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);
};

