#include "SoundTestDSP.h"


SoundTestDSP::SoundTestDSP(void)
{
}

SoundTestDSP::~SoundTestDSP(void)
{
}

void SoundTestDSP::OnKeyPress(int key)
{
	//dsp_ltc.OnKeyPress(key);
}

void SoundTestDSP::OnKeyRelease(int key)
{
	//dsp_ltc.OnKeyRelease(key);
}

void SoundTestDSP::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	//hsyn.SoundListener(buf, ichn, ochn, size, rate);
	ltcEnc.SoundListener(buf, ichn, ochn, size, rate);
	//sigGen.SoundListener(buf, ichn, ochn, size, rate);
	ltcDec.SoundListener(buf, ichn, ochn, size, rate);
}

#include "..\..\Engine\Parsers\MIDIMessage.h"

void SoundTestDSP::MIDIListener(unsigned char* data, int size)
{
	if(GET_MIDI_MESSAGE(data[0]) != MIDIMsg::MIDI_MSG_SYSTEM)
	{
		std::cout << "[MIDI] ";
		for(int i=0; i<size; i++)
		{
			std::cout << (int)data[i] << "\t";
		}
		std::cout << std::endl;
	}
}
