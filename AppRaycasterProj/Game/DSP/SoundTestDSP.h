#pragma once

#include "LTCEncoder.h"
#include "LTCDecoder.h"
#include "SignalGenerator.h"
#include "HarmoSynth.h"
#include "..\..\..\AppRaycaster\Engine\IApp.h"

class SoundTestDSP : public IApp
{
private:

	LTCEncoder ltcEnc;
	LTCDecoder ltcDec;
	HarmoSynth hsyn;
	SignalGenerator sigGen;

public:

	SoundTestDSP(void);
	~SoundTestDSP(void);
	void OnKeyPress(int key);
	void OnKeyRelease(int key);
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);
	void MIDIListener(unsigned char* data, int size);

};

