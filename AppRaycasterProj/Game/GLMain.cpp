#include "GLMain.h"
#include <Windows.h> // flags de bot�es para teclado VK_*
#include <memory>
#include <random> // textura com ruido

GLMain::GLMain(void)
{}

GLMain::~GLMain(void)
{}

void GLMain::TestGenerateNoise()
{
	m_videoSurface	= new GL::VideoSurface(GL_RGB, 512, 512);

	std::cout << __FUNCTION__ << ": " << "Gerando imagem..." << std::endl;

	std::random_device rd;
	std::default_random_engine re(rd());
	std::uniform_int_distribution<unsigned char> rnd(0, 255);

	const unsigned	img_size = m_videoSurface->BufferSize();
	unsigned char*	img_data = new unsigned char[img_size];

	///////////////////////////////////////

	unsigned char* cur = img_data;
	unsigned char* end = img_data + (img_size-1);
	
	unsigned char pixel = 0;
	unsigned i=0;

	while(cur < end)
	{
		if(i==0)
		{
			pixel = rnd(rd);
		}
		++i%=3;
		*cur++ = pixel;
	}

	///////////////////////////////////////

	// Nota: GL::VideoSurface possui buffer duplo. 
	// Ter� que escrever duas vezes para aparecer no primeiro uso.
	m_videoSurface->SetData(img_data, img_size);
	m_videoSurface->SetData(img_data, img_size);

	delete[] img_data;
}

void GLMain::TestTextureLoader()
{
	Bitmap bmp;
	ImageLoader& loader = ImageLoader::Instance();

	try
	{
		const char* arquivo = "..\\Data\\Bitmaps\\Ryoko1.png";
		std::cout << arquivo << "...";
		loader.LoadFile(bmp, arquivo);
		std::cout << "OK!" << std::endl;

		unsigned img_size = bmp.Size();
		unsigned char* img_data = new unsigned char[img_size];
		bmp.GetData(img_data, img_size);
		m_videoSurface	= new GL::VideoSurface(GL_BGR, bmp.Width(), bmp.Height());

		// Nota: GL::VideoSurface possui buffer duplo. 
		// Ter� que escrever duas vezes para aparecer no primeiro uso.
		m_videoSurface->SetData(img_data, img_size);
		m_videoSurface->SetData(img_data, img_size);

		delete img_data;
	}
	catch(std::bad_exception& e)
	{
		std::cout << "Merda: " << e.what() << std::endl;
		m_videoSurface	= new GL::VideoSurface(GL_RGBA, 512, 512);
	}
}

void GLMain::LoadTests()
{
	//TestGenerateNoise();
	TestTextureLoader();
}

void GLMain::LoadFonts()
{
	Bitmap bmp;
	ImageLoader& loader = ImageLoader::Instance();

	try
	{
		const char* arquivo = "..\\Data\\Bitmaps\\AmigaTopazFont.png";
		std::cout << arquivo << "...";
		loader.LoadFile(bmp, arquivo);
		std::cout << "OK!" << std::endl;

		unsigned img_size = bmp.Size();
		void* img_data = new unsigned char[img_size];
		bmp.GetData(img_data, img_size);
	
		m_fontAtlas = new GL::FontAtlas(bmp.Width(), bmp.Height(), 16u, 14u, GL_BGR, GL_UNSIGNED_BYTE, img_data);

		delete img_data;
	}
	catch(std::bad_exception& e)
	{
		std::cout << "Merda: " << e.what() << std::endl;
	}

	m_textRenderer = new GL::TextRenderer();
}

void GLMain::OnStart()
{
	m_surface	= new GL::FrameSurface();
	m_fb[0]		= new GL::Framebuffer(1280, 720);
	m_fb[1]		= new GL::Framebuffer(1280, 720);
	m_fb[2]		= new GL::Framebuffer(1280, 720);

	LoadTests();
	LoadFonts();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GLMain::OnTerminate()
{
	delete m_fb[0];
	delete m_fb[1];
	delete m_fb[2];
	delete m_surface;
	delete m_videoSurface;
	delete m_fontAtlas;
}

void GLMain::OnUpdate()
{

}

void GLMain::OnRender()
{
	float k[2] = {abs(m_snd[0]), abs(m_snd[1])};

	m_fb[0]->Begin();
	GL::Framebuffer::Clear(0.85f, 0.0f, 0.0f, 1.0f);
	{
		m_fb[1]->Begin();
		GL::Framebuffer::Clear(0.0f, 0.0f, 0.85f, 1.0f);
		{
			m_fb[2]->Begin();
			GL::Framebuffer::Clear(0.0f, 0.85f, 0.0f, 1.0f);
			{
				m_surface->Draw(0, 0, 0, 1280*0.85f, 720*0.85f);
			}
			m_fb[2]->End();
			m_surface->Draw(m_fb[2]->Output(), 0, 0, 1280*0.85f, 720*0.85f);
		}
		m_fb[1]->End();
		m_surface->Draw(m_fb[1]->Output(), 0, 0, 1280*0.85f, 720*0.85f);
		
		//m_fontRenderer->Print(*m_fontAtlas, 0, 0, "!\"#$%&'()*+,-./0123456789\:\;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
		//*m_textRenderer << *m_fontAtlas << "!\"#$%&'()*+,-./0123456789\:\;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		//*m_textRenderer << *m_fontAtlas << "Eu vou fazer coc� na casa do pedrinho!";
		m_textRenderer->SetFont(*m_fontAtlas);
		m_textRenderer->Print(m_mouse[0], m_mouse[1], "Eu vou fazer coc� na casa do pedrinho!");
	}
	m_fb[0]->End();
	
	// TODO: Objeto para Framebuffer padrao!
	GL::Framebuffer::Clear(k[0], 0.0f, k[1], 1.0f);
	glViewport(0, 0, m_window[0], m_window[1]);

	float margem = 0.0f;
	m_surface->Draw(m_fb[0]->Output(), margem, margem, (float)m_window[0]-(margem*2.0f), (float)m_window[1]-(margem*2.0f));
	
	//m_surface->Draw(m_videoSurface->Output(), 0, 0, m_mouse[0], m_mouse[1]);
	//m_surface->Draw(m_fontAtlas->AtlasTextureName(), 10.0f, 10.0f, m_fontAtlas->AtlasWidth(), m_fontAtlas->AtlasHeight());
}

void GLMain::OnWindowResize(int w, int h)
{
	m_window[0] = w;
	m_window[1] = h;
	GL::TextRenderer::__Generate__();
	std::cout << __FUNCTION__ << ": " << w << " x " << h << std::endl;
}

void GLMain::OnWindowClose()
{
	App::GetCurrent().Terminate();
}

void GLMain::OnKeyPress(int key)
{
	switch(key)
	{
	case VK_F1:
		GL::TextRenderer::__Generate__();
		break;
	case VK_ESCAPE:
		App::GetCurrent().Terminate();
		break;
	}
}

void GLMain::OnKeyRelease(int key)
{

}

void GLMain::OnMousePress(int button)
{
	
}

void GLMain::OnMouseRelease(int button)
{

}

void GLMain::OnMouseMove(int x, int y)
{
	m_mouse[0] = (float)x;
	m_mouse[1] = (float)y;
}

void GLMain::SoundListener(void* data, int ichn, int ochn, int size, int rate)
{
	float* snd = (float*)data;
	float _x[2] = {0.0f, 0.0f};
	int i, c;

	for(i=0; i<size; i++)
	{
		for(c=0; c<ochn; c++)
		{
			_x[c] += snd[c];
		}
		snd += ochn;
	}

	m_snd[0] = _x[0] / size;
	m_snd[1] = _x[1] / size;
}
