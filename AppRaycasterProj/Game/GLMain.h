#pragma once

#include "App.h"
#include "IApp.h"
#include "..\WIP\GL\GLFrameBuffer.h"
#include "..\WIP\GL\GLFrameSurface.h"
#include "..\WIP\GL\GLVideoSurface.h"
#include "..\WIP\GL\GLTextRenderer.h"
#include "..\WIP\Image\ImageLoader.h"

class GLMain : public IApp
{
protected:

	void TestGenerateNoise();
	void TestTextureLoader();
	void LoadTests();
	void LoadFonts();

	GL::Framebuffer*	m_fb[3];
	GL::FrameSurface*	m_surface;
	GL::VideoSurface*	m_videoSurface;
	GL::FontAtlas*		m_fontAtlas;
	GL::TextRenderer*	m_textRenderer;
	
	float				m_snd[2];
	float				m_mouse[2];
	int					m_window[2];

public:

	GLMain(void);
	~GLMain(void);
	void OnStart(); // na execu��o do loop
	void OnTerminate();
	void OnUpdate(); // tempo fixo
	void OnRender();
	void OnWindowResize(int w, int h);
	void OnWindowClose();
	void OnKeyPress(int key);
	void OnKeyRelease(int key);
	void OnMousePress(int button);
	void OnMouseRelease(int button);
	void OnMouseMove(int x, int y);
	void SoundListener(void* data, int ichn, int ochn, int size, int rate);

};

int main(int argc, char** argv)
{
	App app(argc, argv);
	GLMain glMain;	//MainApp mainApp;
	return app.Execute(&glMain);
}
