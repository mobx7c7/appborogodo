#include "Coord2.h"
#include <memory>

Coord3::Coord3(const Coord3& coord) : x(data[0]), y(data[1]), z(data[2])
{
	operator=(coord);
}

Coord3::Coord3(float x, float y, float z) : x(data[0]), y(data[1]), z(data[2])
{
	data[0] = x;
	data[1] = y;
	data[2] = z;
}

Coord3::Coord3() : x(data[0]), y(data[1]), z(data[2])
{
	memset(data, 0, 12);
}

Coord3& Coord3::operator=(const Coord3 &coord)
{
	memcpy(data, coord.data, 12);
	return *this;
}

Coord3 Coord3::operator+(const Coord3 &coord) const
{
	return Coord3(x + coord.x, y + coord.y, z + coord.z);
}

Coord3 Coord3::operator-(const Coord3 &coord) const
{
	return Coord3(x - coord.x, y - coord.y, z - coord.z);
}

Coord3 Coord3::operator*(const Coord3 &coord) const
{
	return Coord3(x * coord.x, y * coord.y, z * coord.z);
}

Coord3  Coord3::operator*(const float &value) const
{
	return Coord3(x * value, y * value, z * value);
}

Coord3 Coord3::operator/(const Coord3 &coord) const
{
	return Coord3(x / coord.x, y / coord.y, z / coord.z);
}

Coord3 Coord3::Lerp(Coord3& p1, Coord3& p2, float x)
{
	return p1+(p2-p1)*x;
}