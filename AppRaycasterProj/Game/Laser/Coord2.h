#pragma once

class Coord3
{
private:

	float data[3];

public:

	float &x, &y, &z;

	Coord3(const Coord3& coord);
	Coord3(float x, float y, float z);
	Coord3();
	Coord3& operator=(const Coord3 &coord);
	Coord3 operator+(const Coord3 &coord) const;
	Coord3 operator-(const Coord3 &coord) const;
	Coord3 operator*(const Coord3 &coord) const;
	Coord3 operator*(const float &value) const;
	Coord3 operator/(const Coord3 &coord) const;
	static Coord3 Lerp(Coord3& p1, Coord3& p2, float x);
};
