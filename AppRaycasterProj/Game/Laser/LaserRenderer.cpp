#include "LaserRenderer.h"
#include <iostream>

const int LEFT = 1;
const int RIGHT = 2;
const int TOP = 4;
const int BOTTOM = 8;


LaserRenderer::LaserRenderer(void)
{
	xmin = ymin = 0.0f;
	xmax = ymax = 1.0f;

	m_hertzRate		= 60.0f;
	//m_sampleRate	= 192000.0f;
	m_sampleTime	= 0.0f;
	m_timeFactor	= 1.0f;

	//m_hertzPeriod	= 1.0f/m_hertzRate;
	//m_samplePeriod	= 1.0f/m_sampleRate;

	m_currentFrame	= 0u;
	m_currentVertex	= 0u;
	m_vertexLength	= 0u;
}

LaserRenderer::~LaserRenderer(void)
{}

int LaserRenderer::DetectOpcode(float x, float y)
{
	int code = 0;

	if(x < xmin)
		code |= LEFT;
	if(x > xmax)
		code |= RIGHT;
	if(y < ymin)
		code |= BOTTOM;
	if(y > ymax)
		code |= TOP;

	return code;
}

// https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
void LaserRenderer::ClipLine(Graphics& g, float x0, float  y0, float x1, float y1)
{
	bool accept = false;

	for(;;)
	{
		int op1 = DetectOpcode(x0, y0);
		int op2 = DetectOpcode(x1, y1);

		if(!(op1 | op2))
		{
			accept = true;
			break;
		}
		else if(op1 & op2)
		{
			break;
		}
		else
		{
			float x, y;

			int opOut = op1 ? op1 : op2;

			if(opOut & TOP)
			{
				x = x0 + (x1 - x0) * (ymax - y0) / (y1 - y0);
				y = ymax;
			}
			else if(opOut & BOTTOM)
			{	
				x = x0 + (x1 - x0) * (ymin - y0) / (y1 - y0);
				y = ymin;
			}
			else if(opOut & LEFT)
			{	
				x = xmin;
				y = y0 + (y1 - y0) * (xmin - x0) / (x1 - x0);
			}	
			else if(opOut & RIGHT)
			{	
				x = xmax;
				y = y0 + (y1 - y0) * (xmax - x0) / (x1 - x0);
			}

			if(opOut == op1)
			{
				x0 = x;
				y0 = y;
			}
			else
			{
				x1 = x;
				y1 = y;
			}
		}
	}

	if(accept)
	{
		g.DrawLine(x0, y0, x1, y1);
	}
}




void LaserRenderer::Render(Graphics& g)
{
	Framebuffer& fbuf = Framebuffer::GetCurrent();

	float w1 = fbuf.GetWidth();
	float h1 = fbuf.GetHeight();
	float w2 = w1 * 0.25f;
	float h2 = h1 * 0.25f;

	xmin = w2;
	ymin = h2;
	xmax = w1-w2;
	ymax = h1-h2;

	g.SetColor(COLOR_RGBA(16,128,64,255));
	g.SetBlendMode(BlendMode::BLEND_FUNC_ADD);
	
	//g.DrawLine(0, ymin, w1, ymin);
	//g.DrawLine(0, ymax, w1, ymax);
	//g.DrawLine(xmin, 0, xmin, h1);
	//g.DrawLine(xmax, 0, xmax, h1);

	g.DrawLine(xmin, ymin, xmax, ymin);
	g.DrawLine(xmin, ymax, xmax, ymax);
	g.DrawLine(xmin, ymin, xmin, ymax);
	g.DrawLine(xmax, ymin, xmax, ymax);

	//ClipLine(g, 100, 100, 700, 700);

	//static float i=0;
	//dspTimecode.Encode((int)i);
	//dspTimecode.TEST_decodePrintBits();
	//i+=1.0;//1.0/60.0f;
}

void LaserRenderer::OnKeyPress(int key)
{
	switch(key)
	{
	case 'Q':
		m_laser_transform.y -= 0.1f;
		break;
	case 'E':
		m_laser_transform.y += 0.1f;
		break;
	case 'W':
		m_laser_transform.z += 0.1f;
		break;
	case 'S':
		m_laser_transform.z -= 0.1f;
		break;
	case 'A':
		m_laser_transform.x -= 0.1f;
		break;
	case 'D':
		m_laser_transform.x += 0.1f;
		break;


	case VK_UP:
		m_timeFactor += 0.01f;
		break;
	case VK_DOWN:
		m_timeFactor -= 0.01f;
		break;
	case VK_NUMPAD0:
		m_timeFactor = 1.0f;
		break;
	case VK_NUMPAD1:
		m_timeFactor = 0.1f;
		break;
	case VK_NUMPAD2:
		m_timeFactor = 0.01f;
		break;
	case VK_NUMPAD3:
		m_timeFactor = 0.0025f;
		break;
	case VK_SPACE:
		m_sampleTime = 0.0f;
		break;
	};
}

void LaserRenderer::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	// Para todo segundo de audio (sampleRate), h� tantos quadros (fps), resultando samplesPerFrame (spf)
	//		Ent�o: spf	= sampleRate / fps
	// Para todo quadro (Hz), h� tantos vertices (vertexCount), resultando vertexPerFrame (vpf)
	//		Ent�o: vpf	= vertexCount / spf;

	// FIXME: Separar em LaserGenerator (Gerador de DC)
	if(ochn == 2) // aplica-se 'z' com 3 canais.
	{
		float* sndBuf = (float*)buf;

		for(unsigned i=0u; i<size; i++)
		{
			Model& model = m_modelQueue.Current();

			// Se n�o houver mais vertices, avan�a modelo!
			if(m_currentVertex > model.PointCount())
			{
				m_modelQueue.ToNext();
				m_currentVertex = 0;
				m_sampleTime = 0;
			}

			m_vertexLength = m_modelQueue.VertexCount();

			if(model.PointCount() > 0)
			{
				// FIXME: colocar constantes fora deste loop em um futuro pr�ximo... (otimiza��o)
				const float samplesPerHzPeriod	= m_hertzRate / rate;	// 192000Hz / 30Hz = 6400Hz
				const float samplesPerHzSample	= rate / m_hertzRate;	// 192000Hz / 30Hz = 6400Hz
				const float vertexPerHzPeriod	= m_vertexLength / samplesPerHzSample;
				const float vertexPerHzSample	= samplesPerHzSample / m_vertexLength;
				
				m_currentFrame	= (unsigned)(m_sampleTime * samplesPerHzPeriod) % (unsigned)m_hertzRate;
				m_currentVertex	= (unsigned)(m_sampleTime * vertexPerHzPeriod) % (unsigned)m_vertexLength;

				const Coord3& coord = model.GetPoint(m_currentVertex);

				// TODO: opera��es como corte de segmento e matriz de transforma��o 
				const Coord3 p = coord - m_laser_transform;

				if(p.z >= 0.001f && p.z < 100.0f)
				{
					*sndBuf++ = p.y / p.z;
					*sndBuf++ = p.x / p.z;
				}
				else
				{
					*sndBuf++ = 0.0f;
					*sndBuf++ = 0.0f;
				}
			}
			else
			{
				*sndBuf++ = 0.0f;
				*sndBuf++ = 0.0f;
			}

			m_sampleTime += m_timeFactor;
		}
	}
}

void LaserRenderer::MIDIListener(unsigned char* data, int size)
{

}
