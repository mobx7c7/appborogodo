#pragma once

#include "..\..\..\AppRaycaster\Common\Graphics.h"
#include "Coord2.h"
#include "ModelQueue.h"

// Fazer codifica��o de intensidade por segmenta��o de linha (+bits = +segmentos)
// Segmento possui velocidade fixa, independente do comprimento
//
//	cada segundo	= possui quadros 'n'
//	cada quadro		= possui segmentos 'n'
//	cada segmento	= possui pontos 'n'
//  
//	1 seg			= x quadros
//	1 quadro		= x segmentos
//	1 segmento		= x pontos

/*
PIPELINE DO GERADOR:

Fases: (baseado em opengl)
	Pr�-requisito (buffer para desenho)
	Processa vertice (transforma��o)
	Processa primitivas (corte)
	P�s-requisito (atualiza constantes)
	Desenhar

Processa modelos
{
	Processa vertices:
	{
		Aplica��o transforma��o de matriz: (proje��o)
		Se formou triangulo:
		{
			Aplica corte em rela��o ao canvas:
				- M�todo Cohen-Sutherland (segmentos)
				- M�todo Weiler-Atherton (pol�gonos)
			Se triangulo estiver dentro:
				- Escreve no buffer de desenho
				- pontos += 3;
		}
		Se nao:
		{
			Acumula ponto:
		}
	}
}

class LaserLine
{
public:
	Coord3 p1, p2;
};

class LaserClipper
{
private:
	float	xmin, ymin, xmax, ymax;
	int		DetectOpcode(float x, float y);
public:
	void	Clip(const Coord3 &p1, const Coord3 &p2, Coord3 &p3, const Coord3 &p4);
};
*/


class LaserRenderer
{
private:

	float	xmin, ymin, xmax, ymax;
	int		DetectOpcode(float x, float y);
	void	ClipLine(Graphics& g, float x0, float  y0, float x1, float y1);

	////////////////////////////////////////////////////

	ModelQueue	m_modelQueue;
	unsigned	m_currentFrame;
	unsigned	m_currentVertex;
	unsigned	m_vertexLength;
	Coord3		m_laser_transform;

	////////////////////////////////////////////////////

	float	m_hertzRate;
	//float	m_sampleRate;
	float	m_sampleTime;
	float	m_timeFactor;
	//float	m_samplePeriod;
	//float	m_hertzPeriod;

public:

	LaserRenderer(void);
	~LaserRenderer(void);
	void Render(Graphics& g);
	void OnKeyPress(int);
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);
	void MIDIListener(unsigned char* data, int size);

};


