#include "Line3.h"
#include "Math\MathEx.h"

Line3::Line3(void){}

Line3::Line3(Coord3 &p1, Coord3 &p2) : p1(p1), p2(p2){}

Line3::~Line3(void){}

Coord3& Line3::Lerp(float x)
{
	return Coord3::Lerp(p1, p2, x);
}
