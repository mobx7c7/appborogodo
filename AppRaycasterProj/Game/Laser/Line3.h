#pragma once

#include "Coord2.h"

class Line3
{
public:

	Coord3 p1, p2;
	Line3(void);
	Line3(Coord3 &p1, Coord3 &p2);
	~Line3(void);
	Coord3& Lerp(float x);

};

