#include "Model.h"

Model::Model() : m_length(0), m_offset(0), m_coords(0)
{}

Model::~Model()
{
	if(m_coords)
		delete[] m_coords;
}

void Model::SetData(Coord3* data, unsigned length)
{
	if(!data || length == 0) return;

	if(m_coords) delete[] m_coords;

	m_length = length;
	m_coords = new Coord3[length];

	for(unsigned i=0u; i<length; i++)
	{
		*&m_coords[i] = *&data[i];
	}
}

void Model::SetOffset(unsigned offset)
{
	m_offset = offset;
}

const Coord3& Model::GetPoint(unsigned index)
{
	return m_coords[(index + m_offset) % m_length];
}

const Line3& Model::GetLine(unsigned index)
{
	return Line3(m_coords[index % m_length], m_coords[(index + 1) % m_length]);
}

unsigned Model::PointCount()
{
	return m_length;
}