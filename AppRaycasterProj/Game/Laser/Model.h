#pragma once

#include "Line3.h"

class Model
{
private:

	Coord3*		m_coords;
	unsigned	m_length;
	unsigned	m_offset;

public:

	Model();
	~Model();
	void			SetData(Coord3* data, unsigned length);
	void			SetOffset(unsigned offset);
	const Coord3&	GetPoint(unsigned index);
	const Line3&	GetLine(unsigned index);
	unsigned		PointCount();

};
