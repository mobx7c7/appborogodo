#pragma once

#include "Model.h"

class ModelQueue
{
private:

	Model*		m_buffer;
	unsigned	m_current;
	unsigned	m_mcount;
	unsigned	m_vcount;

	void TESTE_InitModels();

public:

	ModelQueue();
	~ModelQueue();
	unsigned CurrentModelIndex();
	unsigned ModelCount();
	unsigned VertexCount();
	Model& ToNext();
	Model& Current();

};
