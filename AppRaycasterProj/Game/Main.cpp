#include "Main.h"
#include <iostream>
#include <sstream>
#include "App.h"
#include "Math\MathEx.h"

MainApp::MainApp()
{
	laserRenderer = new LaserRenderer();
	particles = new ParticleGod();
	raycaster = new Raycaster();
}

MainApp::~MainApp()
{
	delete laserRenderer;
	delete particles;
	delete raycaster;
}

void MainApp::OnCreate()
{
	particles->OnCreate();
}

void MainApp::OnStart()
{
	particles->OnStart();
	g = new Graphics(&App::GetCurrent().GetWindow().GetFramebuffer());
}

void MainApp::OnTerminate()
{
	delete g;
}

void MainApp::OnUpdate()
{
	particles->OnUpdate();
}

void MainApp::OnRender()
{
	Framebuffer& fbuf = Framebuffer::GetCurrent();
	// NOTA: Tr�s passos para apagar Framebuffer...
	g->SetColor(COLOR_RGBA(0,0,0,255));
	g->SetBlendMode(BlendMode::BLEND_FUNC_NORMAL);
	g->DrawRect(0, 0, fbuf.GetWidth(), fbuf.GetHeight());

	// FIXME: Quebrando algo dentro do OnRender() no modo Debug!
	particles->OnRender(); 
	//laserRenderer->Render(*g);
	//raycaster->Render(*g);
}

void MainApp::OnKeyPress(int key)
{
	particles->OnKeyPress(key);
	laserRenderer->OnKeyPress(key);

	if(key == VK_ESCAPE)
		App::GetCurrent().Terminate();
}

void MainApp::OnKeyRelease(int key)
{
	particles->OnKeyRelease(key);
}

void MainApp::OnKeyChar(char c)
{
}

void MainApp::OnMousePress(int button)
{
}

void MainApp::OnMouseRelease(int button)
{
}

void MainApp::OnMouseMove(int x, int y)
{
	particles->OnMouseMove(x,y);
}

void MainApp::OnMouseScroll(int delta)
{
}

void MainApp::OnMouseLeave()
{
}

void MainApp::OnMouseActivate()
{
}

void MainApp::OnWindowClose()
{
	App::GetCurrent().Terminate();
}

void MainApp::OnWindowResize(int w, int h)
{
	//std::cout << __FUNCTION__ << " (" << w << ", " << h << ")" << std::endl;
}

void MainApp::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	sndTestDSP.SoundListener(buf, ichn, ochn, size, rate);
	//particles->SoundListener(buf,siz,chn);
	//laserRenderer->SoundListener(buf, ichn, ochn, size, rate);
}

void MainApp::MIDIListener(unsigned char* data, int size)
{
	sndTestDSP.MIDIListener(data, size);
}

void MainApp::SerialListener(unsigned char* data, int size)
{
	//std::cout << "[Serial]" << data;
	//std::cout << data << " : " << size << std::endl;
	std::cout << data;
	//sndTestDSP.SerialListener(data, size);
}
