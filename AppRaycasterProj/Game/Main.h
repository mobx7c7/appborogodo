#pragma once

#include "Particles\Particles.h"
#include "Laser\LaserRenderer.h"
#include "Raycaster\Raycaster.h"
#include "DSP\SoundTestDSP.h"
#include "..\Common\Graphics.h"
#include "..\Engine\IApp.h"

class MainApp : public IApp
{
protected:

	// TODO FUTURO por prioridade:
	// - otimizar/acelerar com SIMD e OpenCL
	// - implementação de backends OpenGL, D3D, Console...

	Graphics*		g;
	LaserRenderer*	laserRenderer;
	ParticleGod*	particles;
	Raycaster*		raycaster;
	SoundTestDSP	sndTestDSP;

public:

	MainApp();

	~MainApp();

	void OnCreate();

	void OnStart();

	void OnTerminate();

	void OnUpdate();

	void OnRender();

	void OnKeyPress(int key);

	void OnKeyRelease(int key);

	void OnKeyChar(char c);

	void OnMousePress(int button);

	void OnMouseRelease(int button);

	void OnMouseMove(int x, int y);

	void OnMouseScroll(int delta);

	void OnMouseLeave();

	void OnMouseActivate();

	void OnWindowClose();

	void OnWindowResize(int w, int h);

	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);

	void MIDIListener(unsigned char* data, int size);

	void SerialListener(unsigned char* data, int size);

};