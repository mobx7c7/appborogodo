#include "Particles.h"
#include <iostream>
#include <random>
#include <Windows.h>
#include "Math\Matrix.h"
#include "Math\MathEx.h"
#include "App.h" // para pegar Framebuffer

void Particle::Update()
{
	//pos.X() += dir.X();
	//pos.Y() += dir.Y();
	//pos.Z() += dir.Z();

	pos += dir;
	
	/*
	if(pos.Z() > 0.0f)
	{
		pos.Z() -= 64.0f;
	}
	*/

	//float deltax = pos['x']-1.0f;
	//float deltay = pos['y']-1.0f;
	//float deltaz = pos['z']-1.0f;
	//double dist = sqrt(deltax*deltax+deltay*deltay+deltaz*deltaz);

	//if(abs(dist) > 1)
	{
		//pos -= dir;
	}
}

void ParticleGod::OnStart()
{
	g = new Graphics(&App::GetCurrent().GetWindow().GetFramebuffer());

	////////////////////////////////////////////

	m_count		= 65536;
	m_stride	= 5; // 4 para vertice, 1 para cor
	m_buffer	= new VertexBuffer(m_count * m_stride, m_stride);
	m_particles = new Particle[m_count];

	////////////////////////////////////////////

	std::random_device rd;
	std::default_random_engine re(rd());
	std::uniform_real_distribution<float>	rnd1(-1, 1);
	std::uniform_real_distribution<float>	rnd2(-1, 0);
	std::uniform_int_distribution<int>		rnd3(0, 100);

	////////////////////////////////////////////

	Particle*	cur = m_particles;
	Particle*	end = m_particles + m_count-1;
	unsigned*	buf_color;
	float*		buf_vert;
	int			i=0;

	while(cur != end)
	{
		Particle& p = *cur;

		// ref ligado a elemento de array possuindo posicao de memorias
		//		N�O (Uma vez ref atribuida, n�o pode ser modificada!)
		// ref ligado a uma matriz de ponteiros onde cada elemento est� ligada a uma posicao de memo
		//		N�O!
		// Atribui por ref do retorno de uma fun��o
		//		SIM! porem fica feio usar "()" toda hora...

		buf_vert	= &m_buffer->FirstPtr()[m_stride * i++];
		p.pos		= VertexRef(buf_vert);

		switch(rnd3(re))
		{
		case 0:
			p.color = COLOR_RGB(255,128,0);
			break;
		default:
			p.color	= COLOR_RGB(16,32,128);
		};
		
		buf_color	= &(unsigned&)buf_vert[4];
		*buf_color	= p.color;

		//p.pos['x'] = rnd1(re);
		//p.pos['y'] = rnd1(re);
		//p.pos['z'] = rnd2(re) * 64;

		//p.dir['x'] = 0.0f;
		//p.dir['y'] = 0.0f;
		//p.dir['z'] = 0.05f;

		p.dir['x'] = rnd1(re)*0.005f;
		p.dir['y'] = rnd1(re)*0.005f;
		p.dir['z'] = rnd1(re)*0.005f;

		cur++;
	}

	////////////////////////////////////////////

	camera.posz = -5.0f;
	mouseX = mouseDX = mouseY = mouseDY = 0;
	memset(keys, 0, sizeof(int)*256);
}

void ParticleGod::OnTerminate()
{
	delete[] m_particles;
	delete[] m_buffer;
}

void ParticleGod::OnMouseMove(int x, int y)
{
	mouseDX = mouseX - x;
	mouseDY = mouseY - y;
	mouseX = x;
	mouseY = y;
	//camera.posx	+= mouseDX/250.0f;
	//camera.posy	+= mouseDY/250.0f;
}

void ParticleGod::OnKeyPress(int key)
{
	keys[key] = true;
}

void ParticleGod::OnKeyRelease(int key)
{
	keys[key] = false;
}

void ParticleGod::OnUpdate()
{
	Camera& cam = Camera::Current();

	int KEY_LEFT	= keys[VK_LEFT];
	int KEY_RIGHT	= keys[VK_RIGHT];
	int KEY_UP		= keys[VK_UP];
	int KEY_DOWN	= keys[VK_DOWN];
	int KEY_W		= keys['W'];
	int KEY_S		= keys['S'];
	int KEY_A		= keys['A'];
	int KEY_D		= keys['D'];

	float player_vel = 0.1f;

	if(!(KEY_W && KEY_S && KEY_A && KEY_D))
	{
		if(!(KEY_LEFT && KEY_RIGHT))
		{
			if(KEY_LEFT)
			{
				cam.roty -= 1.0f;
			}
			if(KEY_RIGHT)
			{
				cam.roty += 1.0f;
			}
		}
		else
		{
			// alguma coisa
		}

		if(!(KEY_UP && KEY_DOWN))
		{
			if(KEY_UP)
			{
				cam.posy += player_vel;
			}
			if(KEY_DOWN)
			{
				cam.posy -= player_vel;
			}
		}
		else
		{
			// alguma coisa
		}
	}
	else
	{

	}

	if(!(KEY_W && KEY_S && KEY_A && KEY_D))
	{
		if(!(KEY_W && KEY_S))
		{
			if(KEY_W)
			{
				cam.posz += player_vel;
			}
			if(KEY_S)
			{
				cam.posz -= player_vel;
			}
		}
		else
		{
			// alguma coisa
		}

		if(!(KEY_A && KEY_D))
		{
			if(KEY_A)
			{
				cam.posx += player_vel;
			}
			if(KEY_D)
			{
				cam.posx -= player_vel;
			}
		}
		else
		{
			// alguma coisa
		}
	}
	else
	{
		// alguma coisa
	}

	///////////////////////////////////////

	Particle* cur = m_particles;
	Particle* end = &m_particles[m_count-1];
	while(cur != end)
	{
		cur->Update();
		cur++;
	}
}

void ParticleGod::OnRender()
{
	Framebuffer& fbuf = Framebuffer::GetCurrent();

	g->SetBlendMode(BlendMode::BLEND_FUNC_ADD);

	////////////////////////////////////////////////

	//Particle* cur = m_particles;
	//Particle* end = m_particles + m_count-1;
	//SetAttributeState(unsigned target) // vertex, color
	//SetAttribufeParams(unsigned target, unsigned size, unsigned align);
	
	float*		_cur = m_buffer->FirstPtr();
	float*		_end = m_buffer->FirstPtr() + (m_stride * (m_count - 1));
	unsigned	_width = fbuf.GetWidth();
	unsigned	_height = fbuf.GetHeight();
	unsigned	_w2 = _width * 0.5f;
	unsigned	_h2 = _height * 0.5f;
	unsigned	_color;
	Vertex		_pos;
	
	Matrix m_model;
	Matrix::Translate(m_model, camera.posx, camera.posy, camera.posz);

	Matrix m_view;
	Matrix::Perspective(m_view, 60.0f, static_cast<float>(fbuf.GetWidth())/fbuf.GetHeight(), 0.001f, 100.0f);

	Matrix m_mvp(m_view);// * m_model;

	while(_cur < _end)
	{
		_color = (unsigned&)_cur[4];
		_pos['x'] = _cur[0];
		_pos['y'] = _cur[1];
		_pos['z'] = _cur[2];

		// View
		_pos['x'] += m_model[0][3];
		_pos['y'] += m_model[1][3];
		_pos['z'] += m_model[2][3];

		// Projection
		_pos['x'] = (_pos['x'] * m_view[0][0]) + (_pos['y'] * m_view[1][0]) + (_pos['z'] * m_view[2][0]);
		_pos['y'] = (_pos['x'] * m_view[0][1]) + (_pos['y'] * m_view[1][1]) + (_pos['z'] * m_view[2][1]);
		_pos['z'] = (_pos['x'] * m_view[0][2]) + (_pos['y'] * m_view[1][2]) + (_pos['z'] * m_view[2][2]);
		//Matrix::__Perspective__(60.0f, static_cast<float>(fbuf.GetWidth())/fbuf.GetHeight(), 0.001f, 100.0f, _pos);

		if(_pos['z'] > 0.001f && _pos['z'] < 100.0f)
		{
			// World to Screen
			// FIXME: Aplicar prorpiedades de viewport 
			// https://forum.beyond3d.com/threads/world-coordinates-to-screen-coordinates.20769/
			_pos['x'] = _w2 + (_pos['x'] / _pos['z']) * _w2;
			_pos['y'] = _h2 - (_pos['y'] / _pos['z']) * _h2; // Y para cima!

			g->SetColor(_color);
			g->__DrawPoint__(_pos['x'], _pos['y']);
		}

		_cur += m_stride;
	}

	/*
	float* cur = m_buffer->FirstPtr();
	float* end = m_buffer->FirstPtr() + (m_stride * (m_count - 1));

	Vertex		_pos[2];
	unsigned	_color[2];
	unsigned	_width = fbuf.GetWidth();
	unsigned	_height = fbuf.GetHeight();
	unsigned	_w2 = _width * 0.5f;
	unsigned	_h2 = _height * 0.5f;

	while(cur < end)
	{
		_color[0] = (unsigned&)cur[4];
		_pos[0].X() = cur[0];
		_pos[0].Y() = cur[1];
		_pos[0].Z() = cur[2];

		// FIXME: Incorporar em matrix MVP
		{
			// View
			_pos[0].X() -= camera.posx;
			_pos[0].Y() -= camera.posy;
			_pos[0].Z() -= camera.posz;
			// Projection
			Matrix::__Perspective__(60.0f, static_cast<float>(fbuf.GetWidth())/fbuf.GetHeight(), 0.001f, 100.0f, _pos[0]);
		}

		cur += m_stride;

		//////////////////////////////////////////////

		_color[1] = (unsigned&)cur[4];
		_pos[1].X() = cur[0];
		_pos[1].Y() = cur[1];
		_pos[1].Z() = cur[2];

		
		// FIXME: Incorporar em matrix MVP
		{
			// View
			_pos[1].X() -= camera.posx;
			_pos[1].Y() -= camera.posy;
			_pos[1].Z() -= camera.posz;
			// Projection
			Matrix::__Perspective__(60.0f, static_cast<float>(fbuf.GetWidth())/fbuf.GetHeight(), 0.001f, 100.0f, _pos[1]);
		}

		cur += m_stride;

		//////////////////////////////////////////////

		if(_pos[0].Z() > 0.001f && _pos[0].Z() < 100.0f && _pos[1].Z() > 0.001f && _pos[1].Z() < 100.0f)
		{
			// World to Screen
			// FIXME: Aplicar prorpiedades de viewport 
			// https://forum.beyond3d.com/threads/world-coordinates-to-screen-coordinates.20769/
			_pos[0].X() = _w2 + (_pos[0].X() / _pos[0].Z()) * _w2;
			_pos[0].Y() = _h2 - (_pos[0].Y() / _pos[0].Z()) * _h2; // Y para cima!

			_pos[1].X() = _w2 + (_pos[1].X() / _pos[1].Z()) * _w2;
			_pos[1].Y() = _h2 - (_pos[1].Y() / _pos[1].Z()) * _h2; // Y para cima!

			g->SetColor(_color[0]);
			g->DrawLine(_pos[0].X(), _pos[0].Y(), _pos[1].X(), _pos[1].Y());
		}
	}
	*/

	/*
	Framebuffer& fbuf = Framebuffer::GetCurrent();

	Camera& cam = Camera::Current();
	Matrix m_cam = cam.GetMatrix();
	
	Matrix m_ortho;
	Matrix::Orthographic(m_ortho, 0.0f, fbuf.GetWidth(), 0.0f, fbuf.GetHeight(), 0.001f, 100.0f);

	g->SetBlendMode(BlendMode::BLEND_FUNC_ADD);

	cam.fov		= 60.0f;
	cam.aspect	= (float)fbuf.GetHeight()/fbuf.GetWidth();
	cam.zMin	= 0.001f;
	cam.zMax	= 100.0f;

	{
		float v[3];
		Particle* src = m_data;
		for(int i=0; i<m_count; i++)
		{
			//memcpy(v, src, 12); // sizeof(float)*3
			memcpy(v, &src->pos, 12); // sizeof(float)*3

			v[0] -= camera.posx;
			v[1] -= camera.posy;
			v[2] -= camera.posz;

			//Matrix::__VertexToPersp__(fov, aspect, zMin, zMax, v);

			v[0] /= m_cam.m[0][0];// * v[2];
			v[1] /= m_cam.m[1][1];// * v[2];
			v[2] /= m_cam.m[2][2];

			v[0] /= v[2];
			v[1] /= v[2];


			//v[0] *= m_ortho.m[0][0];
			//v[1] *= m_ortho.m[1][1];
			//v[2] *= m_ortho.m[0][2];


			Matrix::__VertexToOrtho__(fbuf.GetWidth(), fbuf.GetHeight(), v);

			if(v[2] >= cam.zMin && v[2] < cam.zMax)
			{
				g->SetColor(src->color);
				g->DrawPoint(v[0], v[1]);
			}
			//src+=3;
			src++;
		}
	}
	*/
}

void ParticleGod::SoundListener(void* buf, int ichn, int ochn, int size, int rate)
{
	float* in = (float*)buf;

	if(ochn == 2)
	{
		float outMS[2]={0};
		for(int i=0; i<size; i++)
		{
			if(ochn == 2)
			{
				float lef = in[0];
				float rig = in[1];
				float mid = lef + rig;
				float sid = lef - rig;
				outMS[0] += mid;
				outMS[1] += sid;
				in[0] = mid;
				in[1] = sid;
				in+=ochn;
			}
		}
		camera.pos[0] = outMS[1] / size;
		camera.pos[1] = outMS[0] / size;
		camera.pos[0] *= 2.0f;
		camera.pos[1] *= 2.0f;
	}
	else
	{
		for(int i=0; i<size; i++)
		{
			for(int c=0; c<ochn || c<3; c++)
			{
				camera.pos[c] = *in++ * 2.0f;
			}
		}
	}
}
