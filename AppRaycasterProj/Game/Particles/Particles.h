#pragma once

#include "Graphics.h"
#include "Camera.h"
#include "IApp.h"
#include "Math\Matrix.h"
#include "Math\Vec3.h"
#include "Buffer\VertexBuffer.h"

class Particle
{
public:

	Particle()
	{
		color = ~0;
		//memset(pos,0,12);
		//memset(dir,0,12);
	}
	
	//__Vec3__ _pos, _dir;
	//float pos[3];
	//float dir[3];
	//float t_start;
	//float t_interval;

	unsigned color;
	Vertex pos;
	Vertex dir;
	void Update();
};

class ParticleGod : public IApp
{
private:

	Graphics* g;
	Camera camera;
	Particle* m_data;
	
	int mouseX, mouseY, mouseDX, mouseDY;
	int keys[256];

	Matrix m_view, m_proj;
	Vec3 _v;

	unsigned		m_count;
	unsigned		m_stride;
	VertexBuffer*	m_buffer;
	Particle*		m_particles;

public:

	//ParticleGod(void);
	//~ParticleGod(void);

	void OnStart();
	void OnTerminate();
	void OnUpdate();
	void OnRender();
	void OnMouseMove(int x, int y);
	void OnKeyPress(int key);
	void OnKeyRelease(int key);
	void SoundListener(void* buf, int ichn, int ochn, int size, int rate);

};

