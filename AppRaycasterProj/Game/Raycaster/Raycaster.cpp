#include "Raycaster.h"
#include <iostream>
#include "Framebuffer.h"
#include "Math\MathEx.h"

//int mapUnitX	= 64; // Escala de 1:64 pixels
//int mapUnitY	= 64; // Escala de 1:64 pixels

int mapHeight	= 32;
int mapUnit		= 64; 
int mapTileW	= 11;
int mapTileH	= 7;
int mapW		= mapTileW * mapUnit;
int mapH		= mapTileH * mapUnit;

//mapPW, mapTW, mapPH, mapTH

int map[7][11] =
{
	{2,2,2,1,1,1,1,1,1,1,1},
	{2,0,0,0,0,0,0,0,0,0,1},
	{2,2,2,0,0,0,0,0,0,0,0},
	{2,0,2,0,0,0,0,0,0,0,0},
	{2,0,0,0,0,0,0,0,0,0,1},
	{2,2,1,0,0,0,0,0,0,0,1},
	{1,1,1,0,0,1,1,1,1,1,1},
};

int IsTileInsideMap(int x, int y)
{
	return x >= 0 && x < mapTileW && y >= 0 && y < mapTileH;
}

int IsCoordInsideMap(float x, float y)
{
	return x >= 0 && x < mapW && y >= 0 && y < mapH;
}

int GetMapTileState(int x, int y)
{
	return x >= 0 && x < mapTileW && y >= 0 && y < mapTileH ? map[y][x] : -1;
}

int GetMapStateColor(int state)
{
	switch(state)
	{
	case 1:
		return COLOR_RGB(0,255,0); // verde
	case 2:
		return COLOR_RGB(0,0,255); // azul
	default:
		return COLOR_RGB(255,0,0); // vermelho
	}
}








Raycaster::Raycaster(void)
{
	player_fov		= 60;
	player_half_fov	= player_fov/2.0f;
	player_ang		= 0;
	player_pos[0]	= 300;
	player_pos[1]	= 300;
}

Raycaster::~Raycaster(void)
{

}


int Raycaster::DoTheCast(Graphics& g, const float ray_ang, const float* ray_pos, float* ray_hit, float* ray_dist)
{
	if(GetMapTileState(ray_pos[0]/mapUnit, ray_pos[1]/mapUnit) != 0)
		return -1;

	const float t		= tan(ray_ang * M_PI_180);

	const float &rayX	= ray_pos[0];
	const float &rayY	= ray_pos[1];
	
	int state, x, y, i, stateH, stateV;

	float distH, distV;
	float posH[2]={0};
	float posV[2]={0};
	float grid[2]={0};
	float step[2]={0};
	float dist[2]={0};

	//////////////////////////////////////////////////////////////
	// INTERSEC��O HORIZONTAL
	//////////////////////////////////////////////////////////////

	grid[1] = floor(rayY / mapUnit) * mapUnit;

	if(ray_ang >= 0.0f && ray_ang < 180.0f) // abaixo
	{
		grid[1] += (float)mapUnit;
		step[1] = (float)mapUnit;
		grid[0] = (float)(rayX + (grid[1] - rayY) / t);
	}
	else // acima
	{ 
		// grid[1]--; // N�O COLOCAR AQUI!
		step[1] = (float)-mapUnit;
		grid[0] = (float)(rayX + (grid[1] - rayY) / t);
		grid[1]	-= 1.0f; // AQUI SIM!
	}

	step[0] = step[1] / t;

	//for(x=y=i=0; i<=mapTileW; i++)
	for(;;)
	{
		//g.SetColor(COLOR_RGB(0,255,0));
		//g.DrawPoint(gridX, gridY);

		x = grid[0] / mapUnit;
		y = grid[1] / mapUnit;

		state = GetMapTileState(x,y);
		
		if(state != 0)
		{
			stateH	= state;
			distH	= hypot(rayX - grid[0],  rayY - grid[1]);
			memcpy(posH, grid, 8);
			//std::cout << "HORIZONTAL\t: " << x << ", " << y << " (state:" << state << ", dist:" << distH << ")" << std::endl;
			break;
		}
		
		grid[0] += step[0];
		grid[1] += step[1];
	}

	//////////////////////////////////////////////////////////////
	// INTERSEC��O VERTICAL
	//////////////////////////////////////////////////////////////

	grid[0] = floor(rayX / mapUnit) * mapUnit;

	if(ray_ang >= 90.0f && ray_ang < 270.0f) // esquerda
	{
		// grid[0]--; // N�O COLOCAR AQUI!
		step[0] = -mapUnit;
		grid[1] = rayY + (grid[0] - rayX) * t;
		grid[0]--; // AQUI SIM!
	}
	else // direita
	{
		grid[0] += mapUnit;	
		step[0] = mapUnit;
		grid[1] = rayY + (grid[0] - rayX) * t;
	}

	step[1] = step[0] * t;

	//for(x=y=i=0; i<=mapTileH; i++)
	for(;;)
	{
		//g.SetColor(COLOR_RGB(255,0,0));
		//g.DrawPoint(gridX, gridY);

		x = grid[0] / mapUnit;
		y = grid[1] / mapUnit;

		state = GetMapTileState(x,y);

		if(state != 0)
		{
			stateV	= state;
			distV	= hypot(rayX - grid[0],  rayY - grid[1]);
			memcpy(posV, grid, 8);
			//std::cout << "VERTICAL\t: " << x << ", " << y << " (state:" << state << ", dist:" << distV << ")" << std::endl;
			break;
		}
		
		grid[0] += step[0];
		grid[1] += step[1];
	}

	//////////////////////////////////////////////////////////////
	// ESCOLHER MENOR DIST�NCIA 
	//////////////////////////////////////////////////////////////

	if(distV > distH)
	{
		memcpy(ray_hit,posH,8);
		*ray_dist = distH;
		state = stateH;
	}
	else
	{
		memcpy(ray_hit,posV,8);
		*ray_dist = distV;
		state = stateV;
	}

	return state;
}

void Raycaster::RenderScene(Graphics& g)
{
	//////////////////////////////////////////////////////////////
	// CONSTANTES
	//////////////////////////////////////////////////////////////

	const int		plane_width		= Framebuffer::GetCurrent().GetWidth();
	const int		plane_height	= Framebuffer::GetCurrent().GetHeight();
	const int		plane_center_x	= plane_width/2;
	const int		plane_center_y	= plane_height/2;
	const float		plane_dist		= plane_center_x / tan(player_half_fov * M_PI_180);
	const float		ray_ang_step	= player_fov / plane_width;

	//////////////////////////////////////////////////////////////
	// VARIAVEIS
	//////////////////////////////////////////////////////////////

	int				ray_state		= 0;
	int				player_state	= GetMapTileState((int)(player_pos[0]/mapUnit), (int)(player_pos[1]/mapUnit));
	float			ray_ang_view	= -player_half_fov;
	float			ray_dist		= 0;
	float			ray_hit[2]		= {0};
	float			slice_height;

	//////////////////////////////////////////////////////////////
	// PAREDES
	//////////////////////////////////////////////////////////////
	
	g.SetBlendMode(BlendMode::BLEND_FUNC_ADD);

	for(int i=0; i<plane_width; i++)
	{
		ray_state = DoTheCast(g, Wrap(player_ang + ray_ang_view, 360.0f), player_pos, ray_hit, &ray_dist);

		if(player_state == 0)
		{
			// NOTA: Arruma distor��o "esf�rica"
			ray_dist *= cos(ray_ang_view * M_PI_180);

			slice_height = mapHeight / ray_dist * plane_dist;
			
			g.SetColor(GetMapStateColor(ray_state));
			g.DrawLine((float)i, (float)(plane_center_y-slice_height), (float)i, (float)(plane_center_y+slice_height));
		}

		ray_ang_view += ray_ang_step;
		ray_ang_view = Wrap(ray_ang_view, 360.0f);
	}
	
	//////////////////////////////////////////////////////////////
	// RAIOS
	//////////////////////////////////////////////////////////////

	ray_ang_view = -player_half_fov;

	g.SetBlendMode(BlendMode::BLEND_FUNC_ADD);
	g.SetColor(COLOR_RGB(32,32,32));

	for(int i=0; i<plane_width; i++)
	{
		DoTheCast(g, Wrap(player_ang + ray_ang_view, 360.0f), player_pos, ray_hit, &ray_dist);

		if(player_state == 0)
		{
			g.DrawLine(player_pos[0], player_pos[1], ray_hit[0], ray_hit[1]);
		}

		ray_ang_view += ray_ang_step;
		ray_ang_view = Wrap(ray_ang_view, 360.0f);
	}

	//////////////////////////////////////////////////////////////
	// RESTO
	//////////////////////////////////////////////////////////////

	g.SetColor(COLOR_RGB(255,255,255)); // branco
	g.DrawPoint(player_pos[0], player_pos[1]);
}

void Raycaster::RenderTiles(Graphics& g)
{
	Framebuffer& fb = Framebuffer::GetCurrent();
	unsigned* data = (unsigned*)fb.GetData();

	

	int px, py, x, y, state;
	unsigned color;

	g.SetBlendMode(BlendMode::BLEND_FUNC_ADD);

	for(y=0; y<fb.GetHeight(); y++)
	{
		for(x=0; x<fb.GetWidth(); x++)
		{
			px = (x / mapUnit);// - player_pos[0];
			py = (y / mapUnit);// - player_pos[1];

			state = GetMapTileState(px,py);

			switch(state)
			{
			case 0:
				color = COLOR_ARGB(72,64,64,0);
				break;
			case 1:
				color = COLOR_ARGB(144,128,128,0);
				break;
			case 2:
				color = COLOR_ARGB(128,128,144,0);
				break;
			default:
				color = COLOR_ARGB(16,8,8,0);
			}

			
			//g.SetColor(color);
			//g.DrawPoint(x, y);

			*data++ = color;
		}
	}
}

void Raycaster::RenderGrid(Graphics& g)
{
	int px, py, x, y;

	g.SetBlendMode(BlendMode::BLEND_FUNC_NORMAL);
	g.SetColor(COLOR_ARGB(32,16,0,255));

	for(x=0; x<=mapTileW; x++)
	{
		px = (x * mapUnit);
		g.DrawLine(px, 0, px, mapH);
	}

	for(y=0; y<=mapTileH; y++)
	{
		py = (y * mapUnit);
		g.DrawLine(0, py, mapW, py);
	}
}


void Raycaster::Render(Graphics& g)
{
	short KEY_LEFT = GetAsyncKeyState(VK_LEFT);
	short KEY_RIGHT = GetAsyncKeyState(VK_RIGHT);
	short KEY_W = GetAsyncKeyState('W');
	short KEY_S = GetAsyncKeyState('S');
	short KEY_A = GetAsyncKeyState('A');
	short KEY_D = GetAsyncKeyState('D');

	float player_vel = 1.0f;

	if(!(KEY_LEFT && KEY_RIGHT))
	{
		if(KEY_LEFT)
		{
			player_ang -= player_vel;
		}
		if(KEY_RIGHT)
		{
			player_ang += player_vel;
		}
	}
	else
	{
		// alguma coisa
	}

	player_ang = Wrap(player_ang, 360.0f);

	{
		float theta = player_ang * M_PI_180;
		float c = cos(theta) * player_vel;
		float s = sin(theta) * player_vel;

		if(!(KEY_W && KEY_S && KEY_A && KEY_D))
		{
			if(!(KEY_W && KEY_S))
			{
				if(KEY_W)
				{
					player_pos[0] += c;
					player_pos[1] += s;
				}
				if(KEY_S)
				{
					player_pos[0] -= c;
					player_pos[1] -= s;
				}
			}
			else
			{
				// alguma coisa
			}

			if(!(KEY_A && KEY_D))
			{
				if(KEY_A)
				{
					player_pos[0] += s;
					player_pos[1] -= c;
				}
				if(KEY_D)
				{
					player_pos[0] -= s;
					player_pos[1] += c;
				}
			}
			else
			{
				// alguma coisa
			}
		}
		else
		{
			// alguma coisa
		}
	}


	/********************************/

	RenderTiles(g);
	RenderScene(g);
	
	RenderGrid(g);
}
