#pragma once

#include "Graphics.h"

class Raycaster
{
private:

	class Ray
	{
		float ro[2];
		float rd[2];
	};

	void	RenderTiles(Graphics& g);
	void	RenderGrid(Graphics& g);
	void	RenderScene(Graphics& g);
	int		DoTheCast(Graphics& g, const float ray_ang, const float* ray_pos, float* ray_hit, float* ray_dist);

	float player_fov;
	float player_half_fov;
	float player_ang;
	float player_pos[2];

public:

	Raycaster(void);
	~Raycaster(void);
	void Render(Graphics& g);

};

