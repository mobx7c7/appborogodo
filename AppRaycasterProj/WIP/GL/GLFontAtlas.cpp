#include "GLFontAtlas.h"

namespace GL
{
	FontAtlas::FontAtlas(unsigned atlasWidth, unsigned atlasHeight, unsigned tileCountX, unsigned tileCountY, GLenum dataFormat, GLenum dataType, const void* data)
		: m_texName(0)
		, m_texTarget(GL_TEXTURE_2D)
		//, m_texInternalFormat(GL_RED) // para fontes monocromáticas
		, m_texInternalFormat(GL_RGBA) // para fontes coloridas
		, m_texDataFormat(dataFormat)
		, m_texDataType(dataType)
		, m_filter(GL_NEAREST) // para fontes pixel perfect
		//, m_filter(GL_LINEAR) // para uso geral
		, m_tileCountX(tileCountX)
		, m_tileCountY(tileCountY)
		, m_tileWidth(atlasWidth/tileCountX)
		, m_tileHeight(atlasHeight/tileCountY)
		, m_atlasWidth(atlasWidth)
		, m_atlasHeight(atlasHeight)
	{
		glGenTextures(1, &m_texName);
		glBindTexture(m_texTarget, m_texName);
		glTexParameteri(m_texTarget, GL_TEXTURE_MIN_FILTER, m_filter);
		glTexParameteri(m_texTarget, GL_TEXTURE_MAG_FILTER, m_filter);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexImage2D(m_texTarget, 0, m_texInternalFormat, m_atlasWidth, m_atlasHeight, 0, m_texDataFormat, m_texDataType, data);
		glBindTexture(m_texTarget, 0);
	}

	FontAtlas::~FontAtlas()
	{
		glDeleteTextures(1, &m_texName);
	}

	unsigned FontAtlas::TileCountX()
	{
		return m_tileCountX;
	}

	unsigned FontAtlas::TileCountY()
	{
		return m_tileCountY;
	}

	unsigned FontAtlas::TileWidth()
	{
		return m_tileWidth;
	}

	unsigned FontAtlas::TileHeight()
	{
		return m_tileHeight;
	}

	unsigned FontAtlas::AtlasWidth()
	{
		return m_atlasWidth;
	}

	unsigned FontAtlas::AtlasHeight()
	{
		return m_atlasHeight;
	}

	GLuint FontAtlas::AtlasTextureTarget()
	{
		return m_texTarget;
	}

	GLuint FontAtlas::AtlasTextureName()
	{
		return m_texName;
	}
}