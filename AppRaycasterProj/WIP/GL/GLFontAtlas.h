#pragma once

#include "GL\glew.h"
#include "..\Image\Bitmap.h"

namespace GL
{
	class FontAtlas
	{
	protected:

		//////////////////////////////////////////////
		// OpenGL
		//////////////////////////////////////////////

		GLuint	m_texName, m_texTarget, m_texInternalFormat, m_texDataFormat, m_texDataType;
		GLuint	m_filter;

		//////////////////////////////////////////////
		// Host
		//////////////////////////////////////////////

		unsigned m_atlasWidth, m_atlasHeight, m_tileWidth, m_tileHeight, m_tileCountX, m_tileCountY;

	public:

		FontAtlas(unsigned atlasWidth, unsigned atlasHeight, unsigned tileCountX, unsigned tileCountY, GLenum dataFormat, GLenum dataType, const void* data);
		~FontAtlas();
		unsigned	TileCountX();
		unsigned	TileCountY();
		unsigned	TileWidth();
		unsigned	TileHeight();
		unsigned	AtlasWidth();
		unsigned	AtlasHeight();
		GLuint		AtlasTextureTarget();
		GLuint		AtlasTextureName();

	};
}
