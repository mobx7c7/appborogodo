#include "GLFrameBuffer.h"
#include <memory>
#include <iostream>
#include <random>

namespace GL
{
	Framebuffer* Framebuffer::m_current = nullptr;

	void Framebuffer::__CreateObjects__()
	{
		glGenTextures(1, &m_texName);
		glGenFramebuffers(1, &m_fboName);
		glGenRenderbuffers(1, &m_rboName);
	}

	void Framebuffer::__DestroyObjects__()
	{
		m_frameWidth		= 0;
		m_frameHeight		= 0;
		m_framePixelSize	= 0;
		m_frameBufferSize	= 0;

		glDeleteTextures(1, &m_texName);
		glDeleteFramebuffers(1, &m_fboName);
		glDeleteRenderbuffers(1, &m_rboName);
	}

	void Framebuffer::__SetupObjects__(unsigned width, unsigned height)
	{
		m_frameWidth		= width;
		m_frameHeight		= height;
		m_framePixelSize	= 4; // sizeof(unsigned char) * 4; // RGBA
		m_frameBufferSize	= m_frameWidth * m_frameHeight * m_framePixelSize;

		//////////////////////////////////////////////////////////////////////
		// Texture
		//////////////////////////////////////////////////////////////////////

		glBindTexture(m_texTarget, m_texName);
		glTexParameteri(m_texTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(m_texTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(m_texTarget, 0, m_texInternalFormat, m_frameWidth, m_frameHeight, 0, m_texDataFormat, m_texDataType, nullptr);
		glBindTexture(m_texTarget, 0);
	
		//////////////////////////////////////////////////////////////////////
		// RBO
		//////////////////////////////////////////////////////////////////////

		glBindRenderbuffer(m_rboTarget, m_rboName);
		glRenderbufferStorage(m_rboTarget, m_rboInternalFormat, m_frameWidth, m_frameHeight);
		glBindRenderbuffer(m_rboTarget, 0);
					
		//////////////////////////////////////////////////////////////////////
		// FBO
		//////////////////////////////////////////////////////////////////////

		glBindFramebuffer(m_fboTarget, m_fboName);
		glFramebufferTexture2D(m_fboTarget, m_fboColorAtt, m_texTarget, m_texName, 0);
		glFramebufferRenderbuffer(m_fboTarget, m_fboDepthAtt, m_rboTarget, m_rboName);
		
		if(!glCheckFramebufferStatus(m_fboTarget))
		{
			// TODO: algo com aviso de erro 
		}

		glBindFramebuffer(m_fboTarget, 0);
	}

	void Framebuffer::__Bind__()
	{
		glBindFramebuffer(m_fboTarget, m_fboName);
		glViewport(0, 0, m_frameWidth, m_frameHeight);
		glDrawBuffer(m_fboColorAtt);
	}

	Framebuffer::Framebuffer(unsigned width, unsigned height)
		: m_texTarget(GL_TEXTURE_2D)
		, m_texInternalFormat(GL_RGBA)
		, m_texDataFormat(GL_RGBA)
		, m_texDataType(GL_UNSIGNED_BYTE)
		, m_fboTarget(GL_DRAW_FRAMEBUFFER)
		, m_fboColorAtt(GL_COLOR_ATTACHMENT0)
		, m_fboDepthAtt(GL_DEPTH_ATTACHMENT)
		, m_rboTarget(GL_RENDERBUFFER)
		, m_rboInternalFormat(GL_DEPTH_COMPONENT)
		, m_frameWidth(0)
		, m_frameHeight(0)
		, m_framePixelSize(0) // RGBA
		, m_frameBufferSize(0)
		, m_frameBufferData(nullptr)
		, m_parent(nullptr)
		, m_beginFlag(false)
	{
		__CreateObjects__();
		__SetupObjects__(width, height);
	}

	Framebuffer::~Framebuffer()
	{
		__DestroyObjects__();
	}

	void Framebuffer::Resize(unsigned width, unsigned height)
	{
		__SetupObjects__(width, height);
	}

	void Framebuffer::Clear(float r, float g, float b, float a)
	{
		glClearColor(r,g,b,a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void Framebuffer::Begin()
	{
		if(!m_beginFlag)
		{
			m_parent	= m_current;
			m_current	= this;
			m_beginFlag = true;
			__Bind__();
		}
	}

	void Framebuffer::End()
	{
		if(m_beginFlag)
		{
			if(m_parent)
			{
				m_current = m_parent;
				if(m_current != this)
				{
					m_current->__Bind__();
				}
			}
			else
			{
				m_current = nullptr;
				glBindFramebuffer(m_fboTarget, 0);
				//glViewport(0, 0, m_window[0], m_window[1]);
			}
			m_beginFlag = false;
		}
	}
	
	unsigned Framebuffer::Width()
	{
		return m_frameWidth;
	}

	unsigned Framebuffer::Height()
	{
		return m_frameHeight;
	}

	GLuint Framebuffer::Output()
	{
		return m_texName;
	}
}