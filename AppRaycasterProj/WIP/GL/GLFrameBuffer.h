#pragma once

#include "..\..\AppRaycaster\Libs\glew\include\GL\glew.h"
#define GLM_FORCE_RADIANS
#include "..\..\AppRaycaster\Libs\glm\gtc\matrix_transform.hpp"

class IFramebuffer
{
protected:
	IFramebuffer(const IFramebuffer&){}
	IFramebuffer(){}
public:
	virtual ~IFramebuffer(){}
};



namespace GL
{
	class Framebuffer : public IFramebuffer
	{
	protected:

		//////////////////////////////////////////////
		// Uso pelo OpenGL
		//////////////////////////////////////////////

		GLuint			m_texName, m_texTarget, m_texInternalFormat, m_texDataFormat, m_texDataType;
		GLuint			m_fboName, m_fboTarget, m_fboColorAtt, m_fboDepthAtt;
		GLuint			m_rboName, m_rboTarget, m_rboInternalFormat;

		void			__CreateObjects__();
		void			__DestroyObjects__();
		void			__SetupObjects__(unsigned width, unsigned height);
		virtual void	__Bind__();

		//////////////////////////////////////////////
		// Uso pelo Host
		//////////////////////////////////////////////

		Framebuffer*		m_parent;
		static Framebuffer*	m_current;
		void*				m_frameBufferData;
		unsigned			m_frameWidth, m_frameHeight, m_framePixelSize, m_frameBufferSize;
		bool				m_beginFlag;
		
	public:

		// Candidatos para nome:
		// activate, deativate
		// push, pop
		// begin, end

		Framebuffer(unsigned width, unsigned height);
		~Framebuffer();

		void		Resize(unsigned width, unsigned height);
		void		Begin();
		void		End();
		unsigned	Width();
		unsigned	Height();
		GLuint		Output();

		static void	Clear(float r, float g, float b, float a);

	};


	class DefaultFrameBuffer : public Framebuffer
	{
	protected:
		void __Bind__()
		{
			//glBindFramebuffer(m_fboTarget, 0);
		}

	public:


	};

}