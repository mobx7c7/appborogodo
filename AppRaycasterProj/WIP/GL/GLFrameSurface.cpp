#include "GLFrameSurface.h"
#include <string>
#include <iostream>

namespace GL
{
	unsigned FrameSurface::m_count(0);
	unsigned FrameSurface::m_vaoName(0);
	unsigned FrameSurface::m_progName(0);
	GL::SharedQuadMesh* FrameSurface::m_quad(nullptr);

	// TODO: Carregar atrav�s de um arquivo.
	const char* FrameSurface::m_srcs[] =
	{
		// Vertex 
		"#version 400\n"
		"in vec2 v_coord;\n"
		"in vec2 t_coord;\n"
		"uniform mat4 mmvp;\n"
		"out vec2 texCoord;\n"
		"void main(){\n"
		"gl_Position = mmvp * vec4(v_coord, 0.0, 1.0);\n"
		"texCoord = t_coord;\n"
		"}",

		// Fragment
		"#version 400\n"
		"in vec2 texCoord;\n"
		"uniform sampler2D tex0;\n"
		"out vec4 fragOut;\n"
		"void main(){\n"
		"fragOut = texture(tex0, texCoord);\n"
		"}"
	};

	void FrameSurface::__Destroy__()
	{
		GLint shaderCount = 0;
		glGetProgramiv(m_progName, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* shaderNames = new GLuint[shaderCount];
		glGetAttachedShaders(m_progName, shaderCount, nullptr, shaderNames);
		{
			GLuint* cur = shaderNames;
			GLuint* end = shaderNames + shaderCount;
			while(cur < end)
			{
				glDeleteShader(*cur++);
			}
		}
		glDeleteProgram(m_progName);
		delete[] shaderNames;

		////////////////////////////////////////

		glDeleteVertexArrays(1, &m_vaoName);
		delete m_quad;
	}

	void FrameSurface::__Generate__()
	{
		//////////////////////////////////////////////////////////////////////
		// Generate Objects
		//////////////////////////////////////////////////////////////////////

		m_quad = new GL::SharedQuadMesh();
		glGenVertexArrays(1, &m_vaoName);
		m_progName = glCreateProgram();

		//////////////////////////////////////////////////////////////////////
		// Shaders
		//////////////////////////////////////////////////////////////////////

		// TODO: Carregar shaders atrav�s de um arquivo.

		GLuint vs = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs, 1, &m_srcs[0], nullptr);
		glCompileShader(vs);

		GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fs, 1, &m_srcs[1], nullptr);
		glCompileShader(fs);

		//////////////////////////////////////////////////////////////////////
		// Program
		//////////////////////////////////////////////////////////////////////

		glAttachShader(m_progName, vs);
		glAttachShader(m_progName, fs);
		glLinkProgram(m_progName);

		glBindAttribLocation(m_progName, 0, "v_coord");
		glBindAttribLocation(m_progName, 1, "t_coord");

		//GLuint loc_v_coord = glGetAttribLocation(m_progName, "v_coord");
		//GLuint loc_t_coord = glGetAttribLocation(m_progName, "t_coord");

		GLint result = 0;
		glGetProgramiv(m_progName, GL_LINK_STATUS, &result);
		if(!result)
		{
			GLint length = 0;
			glGetProgramiv(m_progName, GL_INFO_LOG_LENGTH, &length);
			if(length > 0)
			{
				char* log = new char[length];
				GLint written = 0;
				glGetProgramInfoLog(m_progName, length, &written, log);
				std::cout << __FUNCTION__ << ":\n" << log << std::endl;
			}
		}

		//////////////////////////////////////////////////////////////////////
		// VAO
		//////////////////////////////////////////////////////////////////////

		glBindVertexArray(m_vaoName);
		{
			m_quad->Bind();
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(float)*2, 0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(float)*2, 0);
		}
		glBindVertexArray(0);
	}

	FrameSurface::FrameSurface()
	{
		if(m_count++ == 0)
		{
			__Generate__();
		}
	}

	FrameSurface::~FrameSurface()
	{
		if(--m_count == 0)
		{
			__Destroy__();
		}
	}

	void FrameSurface::Draw(GLuint tex, float x, float y, float w, float h)
	{
		GLint currentViewport[4]; // x, y, w, h
		glGetIntegerv(GL_VIEWPORT, currentViewport);

		float srcx = x;
		float srcy = y;
		float srcw = currentViewport[2];
		float srch = currentViewport[3];

		GLint currentFBO;
		glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO); // GL_FRAMEBUFFER_BINDING: mesma coisa, por�m ultrapassado...
		
		if(currentFBO == 0) // padr�o
		{
			// NOTA: Imagem do Framebuffer invertida devido a conven��o de coordenada do OpenGL!
			srcy -= currentViewport[3];
			srch = -currentViewport[3];
		}

		Draw(tex, srcx, srcy, srcw, srch, 0, 0, w, h);
	}

	void FrameSurface::Draw(GLuint tex, float srcx, float srcy, float srcw, float srch, float dstx, float dsty, float dstw, float dsth)
	{
		m_mvp = glm::ortho(0.0f, srcw, 0.0f, srch);
		m_mvp = glm::translate(m_mvp, glm::vec3(srcx, srcy, 0.0f));
		m_mvp = glm::scale(m_mvp, glm::vec3(dstw, dsth, 1.0f));

		///////////////////////////////////////////////

		GLuint loc_mmvp		= glGetUniformLocation(m_progName, "mmvp");
		GLuint loc_tex0		= glGetUniformLocation(m_progName, "tex0");
		GLenum texTarget	= GL_TEXTURE_2D;

		//glDepthMask(GL_FALSE);
		//glDisable(GL_CULL_FACE);
		//glEnable(texTarget); // n�o precisa ativar quando usado por shader.

		glBindVertexArray(m_vaoName);
		{
			// TODO: ativar mais de uma textura
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(texTarget, tex);

			{
				glUseProgram(m_progName);
				glUniformMatrix4fv(loc_mmvp, 1, false, &m_mvp[0][0]);
				glUniform1i(loc_tex0, 0);
				m_quad->Draw();
				glUseProgram(0);
			}

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(texTarget, 0);
		}
		glBindVertexArray(0);
	}
}