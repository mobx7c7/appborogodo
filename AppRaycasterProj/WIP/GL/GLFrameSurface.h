#pragma once

#include "GL\glew.h"
#define GLM_FORCE_RADIANS
#include "glm\gtc\matrix_transform.hpp"

#include "GLFrameBuffer.h"
#include "GLSharedQuadMesh.h"

namespace GL
{
	class FrameSurface
	{
	private:

		static GLuint				m_vaoName;
		static GLuint				m_progName;
		static GL::SharedQuadMesh*	m_quad;
		static const char*			m_srcs[];
		static void					__Generate__();
		static void					__Destroy__();
		static unsigned				m_count;

		glm::mat4					m_mvp;

	public:

		FrameSurface();
		~FrameSurface();
		void Draw(GLuint tex, float x, float y, float w, float h);
		void Draw(GLuint tex, float srcx, float srcy, float srcw, float srch, float dstx, float dsty, float dstw, float dsth);

	};

	class DefaultFrameSurface : public FrameSurface
	{
	public:
		void Draw(Framebuffer* fb, float x, float y, float srcw, float srch, float dstw, float dsth)
		{

		}
	};
}