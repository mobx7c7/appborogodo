#include "GLSharedQuadMesh.h"

namespace GL
{
	unsigned SharedQuadMesh::m_count = 0;

	GLuint SharedQuadMesh::m_bufferName[2];

	const float SharedQuadMesh::m_quad_vert[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};

	const unsigned SharedQuadMesh::m_quad_indx[] =
	{
		0, 1, 3, 1, 3, 2
	};

	void SharedQuadMesh::__Generate__()
	{
		//////////////////////////////////////////////////////////////////////
		// Generate Objects
		//////////////////////////////////////////////////////////////////////

		glGenBuffers(2, m_bufferName);

		//////////////////////////////////////////////////////////////////////
		// VBO
		//////////////////////////////////////////////////////////////////////

		glBindBuffer(GL_ARRAY_BUFFER, m_bufferName[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*8, m_quad_vert, GL_STATIC_DRAW);

		//////////////////////////////////////////////////////////////////////
		// IBO
		//////////////////////////////////////////////////////////////////////

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferName[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned)*6, m_quad_indx, GL_STATIC_DRAW);
	}

	void SharedQuadMesh::__Destroy__()
	{
		glDeleteBuffers(2, m_bufferName);
	}

	SharedQuadMesh::SharedQuadMesh(void)
	{
		if(m_count++ == 0)
		{
			__Generate__();
		}
	}

	SharedQuadMesh::~SharedQuadMesh(void)
	{
		if(--m_count == 0)
		{
			__Destroy__();
		}
	}

	void SharedQuadMesh::Bind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_bufferName[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferName[1]);
	}

	void SharedQuadMesh::Unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void SharedQuadMesh::Draw()
	{
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0); 
	}
	/*
	GLuint SharedQuadMesh::VertexBufferName()
	{
		return m_bufferName[0];
	}

	GLuint SharedQuadMesh::ElementBufferName()
	{
		return m_bufferName[1];
	}
	*/
}