#pragma once
#include "GL\glew.h"

namespace GL
{
	class SharedQuadMesh
	{
	private:

		static unsigned			m_count;
		static const float		m_quad_vert[];
		static const unsigned	m_quad_indx[];
		static GLuint			m_bufferName[2];
		static void				__Generate__();
		static void				__Destroy__();

	public:

		SharedQuadMesh(void);
		~SharedQuadMesh(void);
		void Bind();
		void Unbind();
		void Draw();
		/*
		GLuint VertexBufferName();
		GLuint ElementBufferName();
		*/
	};
}