#include "GLTextRenderer.h"
#include <fstream>
//#include <streambuf>
//#include <iomanip>

#define GLM_FORCE_RADIANS
#include "glm\gtc\matrix_transform.hpp"

namespace GL
{
	unsigned	TextRenderer::m_count(0);
	GLuint		TextRenderer::m_progName(0);
	GLuint		TextRenderer::m_loc_atlasTex(0); 
	GLuint		TextRenderer::m_loc_atlasSize(0);
	GLuint		TextRenderer::m_loc_tileCount(0);
	GLuint		TextRenderer::m_loc_tileSize(0);
	GLuint		TextRenderer::m_loc_position(0);
	GLuint		TextRenderer::m_loc_tileChar(0);
	GLuint		TextRenderer::m_loc_screenSize(0);
	GLuint		TextRenderer::m_loc_m_mvp(0);

	/*
	const char* TextRenderer::m_srcs[] =
	{
	// Vertex 
	"#version 400\n"
	"in int character;\n"
	//"uniform ivec2 position;\n"
	"out int tileChar;\n"
	"out int tileIndex;\n"
	"out ivec2 tilePosition;\n"
	"void main(){\n"
	"tileChar = character;\n"
	"tilePosition = position;\n"
	"tileIndex = gl_VertexID;\n"
	"}\n",


	// Geometry 
	"#version 400\n"

	"in int tileChar[1];\n"
	"in int tileIndex[1];\n"
	//"uniform ivec2 tilePosition[1
	//"uniform ivec2 tileCount[1];\n"
	"uniform ivec2 tileSize[1];\n"
	"uniform vec2 tileScale[1];\n"
	"uniform ivec2 atlasSize[1];\n"

	"uniform mat4 m_mvp;\n"
	"out vec2 texCoord;\n"

	"void main(){\n"

	"int currentChar = clamp(tileChar[0]-32, 0, 223);\n"

	"int tileY = letter / atlasSize[0].x + 1;\n"
	"int tileX = letter % atlasSize[0].x;\n"
	"int tileW = 1.0/atlasSize[0].x;\n"
	"int tileH = 1.0/atlasSize[0].y;\n"

	"float s0 = tileW * tileX;\n"
	"float s1 = s0 + tileW;\n"
	"float t0 = 1.0 - tileH * tileY;\n"
	"float t1 = t0 + tileH;\n"

	// Incorporar na matriz m_mvp
	//"float scaleX = tileSize[0].x * tileScale[0].x;"
	//"float scaleY = tileSize[0].y * tileScale[0].y;"

	"texCoord = vec2(s0, t1);\n"
	"gl_Position = m_mvp * vec4(0,0,0,1);\n"
	"EmitVertex();\n"

	"texCoord = vec2(s1, t1);\n"
	"gl_Position = m_mvp * vec4(1,0,0,1);\n"
	"EmitVertex();\n"

	"texCoord = vec2(s0, t0);\n"
	"gl_Position = m_mvp * vec4(0,1,0,1);\n"
	"EmitVertex();\n"

	"texCoord = vec2(s1, t0);\n"
	"gl_Position = m_mvp * vec4(1,1,0,1);\n"
	"EmitVertex();\n"

	"EndPrimitive();\n"

	"}\n",


	// Fragment
	"#version 400\n"
	"in vec2 texCoord;\n"
	"uniform sampler2D atlasTex;\n"
	"out vec4 fragOut;\n"
	"void main(){\n"
	"fragOut = texture(atlasTex, texCoord);\n"
	"}"
	};
	*/

	void LoadShaderFromFile(const char* path, GLuint& shader)
	{
		std::cout << __FUNCTION__ << ": " << path << "... ";

		std::ifstream ifs(path, std::fstream::binary);
		if(ifs)
		{

			ifs.seekg(0, ifs.end);
			int size = ifs.tellg();
			ifs.seekg(0, ifs.beg);
			char* data = new char[size+1];
			ifs.read(data, size);
			ifs.close();
			data[size] = '\0';

			glShaderSource(shader, 1, &data, 0);
			glCompileShader(shader);
			delete data;
			std::cout << "OK!" << std::endl;
		}
		else
		{
			std::cout << "Erro!" << std::endl;
		}
	}

	void TextRenderer::__Generate__()
	{
		//////////////////////////////////////////////////////////////////////
		// Shaders
		//////////////////////////////////////////////////////////////////////

		GLuint shader[3] = 
		{
			glCreateShader(GL_VERTEX_SHADER),
			glCreateShader(GL_GEOMETRY_SHADER),
			glCreateShader(GL_FRAGMENT_SHADER),
		};

		LoadShaderFromFile("..\\Data\\Shaders\\FontTile.vs.glsl", shader[0]);
		LoadShaderFromFile("..\\Data\\Shaders\\FontTile.gs.glsl", shader[1]);
		LoadShaderFromFile("..\\Data\\Shaders\\FontTile.fs.glsl", shader[2]);

		//glShaderSource(vs, 1, &m_srcs[0], nullptr);
		//glShaderSource(gs, 1, &m_srcs[1], nullptr);
		//glShaderSource(fs, 1, &m_srcs[2], nullptr);

		//glCompileShader(vs);
		//glCompileShader(gs);
		//glCompileShader(fs);

		//////////////////////////////////////////////////////////////////////
		// Program
		//////////////////////////////////////////////////////////////////////

		m_progName = glCreateProgram();
		glAttachShader(m_progName, shader[0]);
		glAttachShader(m_progName, shader[1]);
		glAttachShader(m_progName, shader[2]);
		glLinkProgram(m_progName);

		//glBindAttribLocation(m_progName, 0, "tileChar");

		GLint result = 0;
		glGetProgramiv(m_progName, GL_LINK_STATUS, &result);
		if(!result)
		{
			GLint length = 0;
			glGetProgramiv(m_progName, GL_INFO_LOG_LENGTH, &length);
			if(length > 0)
			{
				char* log = new char[length];
				GLint written = 0;
				glGetProgramInfoLog(m_progName, length, &written, log);
				std::cout << "\n" << __FUNCTION__ << ":\n" << log << "\n\n";
				return;
			}
		}

		m_loc_tileChar		= glGetAttribLocation(m_progName, "tileChar");
		m_loc_screenSize	= glGetUniformLocation(m_progName, "screenSize");
		m_loc_atlasTex		= glGetUniformLocation(m_progName, "atlasTex");
		m_loc_atlasSize		= glGetUniformLocation(m_progName, "atlasSize");
		m_loc_tileCount		= glGetUniformLocation(m_progName, "tileCount");
		m_loc_tileSize		= glGetUniformLocation(m_progName, "tileSize");
		m_loc_m_mvp			= glGetUniformLocation(m_progName, "m_mvp");

		//////////////////////////////////////////////////////////////////////
		// UBO
		//////////////////////////////////////////////////////////////////////

		//m_loc_atlasTex	= glGetUniformLocation(m_progName, "atlasTex");
		//m_loc_atlasSize	= glGetUniformLocation(m_progName, "atlasSize");
		//m_loc_tileSize	= glGetUniformLocation(m_progName, "tileSize");
		//m_loc_m_mvp		= glGetUniformLocation(m_progName, "m_mvp");

		//m_loc_position	= glGetUniformLocation(m_progName, "position");
		//m_loc_tileCount	= glGetUniformLocation(m_progName, "tileCount");
	}

	void TextRenderer::__Destroy__()
	{
		GLint shaderCount = 0;
		glGetProgramiv(m_progName, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* shaderNames = new GLuint[shaderCount];
		glGetAttachedShaders(m_progName, shaderCount, nullptr, shaderNames);

		{
			GLuint* cur = shaderNames;
			GLuint* end = shaderNames + shaderCount;
			while(cur < end)
			{
				glDeleteShader(*cur++);
			}
		}

		glDeleteProgram(m_progName);
		delete[] shaderNames;
	}

	TextRenderer::TextRenderer()
	{
		if(m_count++ == 0)
		{
			__Generate__();
		}
	}

	TextRenderer::~TextRenderer()
	{
		if(--m_count == 0)
		{
			__Destroy__();
		}
	}

	void TextRenderer::SetFont(FontAtlas& font)
	{
		m_font = &font;
	}

	void TextRenderer::Print(unsigned x, unsigned y, const char* txt)
	{
		if(m_font)
		{
			Print(*m_font, x, y, txt);
		}
	}

	void TextRenderer::Print(FontAtlas& font, unsigned x, unsigned y, const char* txt)
	{
		GLint currentViewport[4]; // x, y, w, h
		glGetIntegerv(GL_VIEWPORT, currentViewport);
		float src[4] = {0, 0, currentViewport[2], currentViewport[3]};
		float dst[4] = {0, 0, currentViewport[2], currentViewport[3]};

		GLint currentFBO;
		glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &currentFBO); // GL_FRAMEBUFFER_BINDING: mesma coisa, por�m ultrapassado...
		if(currentFBO == 0) // padr�o
		{
			src[1] -= currentViewport[3];
			src[3] = -currentViewport[3];
		}

		/////////////////////////////////////////////////////

		glm::mat4 m_mvp;
		m_mvp = glm::ortho(0.0f, src[2], 0.0f, src[3]);
		m_mvp = glm::translate(m_mvp, glm::vec3(x, y, 0.0f));
		m_mvp = glm::scale(m_mvp, glm::vec3(dst[2], dst[3], 1.0f));

		/////////////////////////////////////////////////////

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(font.AtlasTextureTarget(), font.AtlasTextureName());
		{
			glUseProgram(m_progName);
			{
				glBindBuffer(GL_ARRAY_BUFFER, 0); // Necess�rio!!
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				glEnableVertexAttribArray(m_loc_tileChar);
				glVertexAttribIPointer(m_loc_tileChar, 1, GL_UNSIGNED_BYTE, 1, txt);

				glUniform1i(m_loc_atlasTex, 0);
				glUniform2i(m_loc_screenSize, (int)src[2], (int)src[3]);
				glUniform2i(m_loc_atlasSize, font.AtlasWidth(), font.AtlasHeight());
				glUniform2i(m_loc_tileCount, font.TileCountX(), font.TileCountY());
				glUniform2i(m_loc_tileSize, font.TileWidth(), font.TileHeight());
				glUniformMatrix4fv(m_loc_m_mvp, 1, false, &m_mvp[0][0]);

				glDrawArrays(GL_POINTS, 0, strlen(txt));
			}
			glUseProgram(0);
		}
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(font.AtlasTextureTarget(), 0);
	}

	TextRenderer& TextRenderer::operator<<(FontAtlas& font)
	{
		SetFont(font);
		return *this;
	}

	TextRenderer& TextRenderer::operator<<(const char* txt)
	{
		Print(0,0,txt);
		return *this;
	}
}