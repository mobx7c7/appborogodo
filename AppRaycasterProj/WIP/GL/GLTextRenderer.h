#pragma once
#include "GL\glew.h"
#include "GLFontAtlas.h"
#include <memory>
#include <iostream>

namespace GL
{
	class TextRenderer
	{
	private:

		//////////////////////////////////////////////
		// OpenGL
		//////////////////////////////////////////////
		
		static GLuint		m_progName;
		static GLuint		m_loc_atlasTex, m_loc_atlasSize, m_loc_tileCount, m_loc_tileSize;
		static GLuint		m_loc_position, m_loc_m_mvp, m_loc_screenSize, m_loc_tileChar;
		static void			__Destroy__();
		
		//////////////////////////////////////////////
		// Host
		//////////////////////////////////////////////
		
		static unsigned m_count;
		FontAtlas* m_font;
		void Print(FontAtlas& font, unsigned x, unsigned y, const char* txt);

	public:

		static void	__Generate__();
		TextRenderer();
		~TextRenderer();
		void SetFont(FontAtlas& font);
		void Print(unsigned x, unsigned y, const char* txt);
		TextRenderer& operator<<(FontAtlas& font);
		TextRenderer& operator<<(const char* txt);

	};
}