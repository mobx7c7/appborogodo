#include "GLVideoSurface.h"
#include <memory>

namespace GL
{
	void VideoSurface::__CreateObjects__()
	{
		glGenTextures(1, &m_texName);
		glGenBuffers(2, m_pboName);
	}

	void VideoSurface::__DestroyObjects__()
	{
		m_frameWidth		= 0;
		m_frameHeight		= 0;
		m_framePixelSize	= 0;
		m_frameBufferSize	= 0;

		glDeleteTextures(1, &m_texName);
		glDeleteBuffers(2, m_pboName);
	}

	void VideoSurface::__SetupObjects__(unsigned width, unsigned height)
	{
		// FIXME: Suportar mais formatos de pixels;
		m_frameWidth		= width;
		m_frameHeight		= height;
		m_framePixelSize	= 3; // RGBA
		m_frameBufferSize	= width * height * 3;

		//////////////////////////////////////////////////////////////////////
		// Texture
		//////////////////////////////////////////////////////////////////////

		glBindTexture(m_texTarget, m_texName);
		glTexParameteri(m_texTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(m_texTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(m_texTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(m_texTarget, 0, m_texInternalFormat, m_frameWidth, m_frameHeight, 0, m_texDataFormat, m_texDataType, nullptr);
		glBindTexture(m_texTarget, 0);

		//////////////////////////////////////////////////////////////////////
		// PBO
		//////////////////////////////////////////////////////////////////////

		for(int i=0; i<2; i++)
		{
			glBindBuffer(m_pboTarget, m_pboName[i]);
			glBufferData(m_pboTarget, m_frameBufferSize, nullptr, GL_STREAM_DRAW);
		}
		glBindBuffer(m_pboTarget, 0);
	}

	VideoSurface::VideoSurface(unsigned dataFormat, unsigned width, unsigned height)
		// Nota GL_TEXTURE_RECTANGLE: (https://www.opengl.org/wiki/Rectangle_Texture)
		// Ao usa-lo, ter� que adaptar todos os locais que for desenhar a textura.
		// Lembrando tamb�m que inclui shader, onde ter� que usar sampler2DRect.
		//: m_texTarget(GL_TEXTURE_RECTANGLE) // 2D sem mipmaps e sem limite de resolu��o multipla de 2
		: m_texTarget(GL_TEXTURE_2D)
		, m_texInternalFormat(GL_RGB)
		, m_texDataFormat(dataFormat)
		, m_texDataType(GL_UNSIGNED_BYTE)
		, m_pboTarget(GL_PIXEL_UNPACK_BUFFER)
		, m_pboSwapIndex(0)
		, m_frameBufferData(nullptr)
	{
		__CreateObjects__();
		__SetupObjects__(width, height);
	}

	VideoSurface::~VideoSurface()
	{
		__DestroyObjects__();
	}

	// Sem PBO
	//void VideoSurface::SetPixels(const char* data, unsigned size)
	//{
	//	glBindTexture(m_texTarget, m_texName);
	//	glTexSubImage2D(m_texTarget, 0, 0, 0, m_frameWidth, m_frameHeight, m_texDataFormat, m_texDataType, data);
	//	glBindTexture(m_texTarget, 0);
	//}
	//

	// Com PBO
	void VideoSurface::SetData(const void* data, unsigned size)
	{
		// Leitura:
		glBindBuffer(m_pboTarget, m_pboName[m_pboSwapIndex]);
		glBindTexture(m_texTarget, m_texName);
		glTexSubImage2D(m_texTarget, 0, 0, 0, m_frameWidth, m_frameHeight, m_texDataFormat, m_texDataType, nullptr);
		m_pboSwapIndex = (m_pboSwapIndex+1)%2;

		// Escrita:
		glBindBuffer(m_pboTarget, m_pboName[m_pboSwapIndex]);
		glBufferData(m_pboTarget, m_frameBufferSize, nullptr, GL_STREAM_DRAW);
		m_frameBufferData = glMapBuffer(m_pboTarget, GL_WRITE_ONLY);
		if(m_frameBufferData)
		{
			memcpy(m_frameBufferData, data, size < m_frameBufferSize ? size : m_frameBufferSize);
			glUnmapBuffer(m_pboTarget);
		}
		glBindTexture(m_texTarget, 0);
		glBindBuffer(m_pboTarget, 0);
	}

	unsigned VideoSurface::Width()
	{
		return m_frameWidth;
	}

	unsigned VideoSurface::Height()
	{
		return m_frameHeight;
	}

	unsigned VideoSurface::PixelSize()
	{
		return m_framePixelSize;
	}

	unsigned VideoSurface::BufferSize()
	{
		return m_frameBufferSize;
	}

	GLuint VideoSurface::Output()
	{
		return m_texName;
	}
};