#pragma once

#include "GL\glew.h"

namespace GL
{
	enum PixelFormat
	{
		PIXEL_FORMAT_RGB,
		PIXEL_FORMAT_BGR,
	};

	class VideoSurface
	{
	protected:

		//////////////////////////////////////////////
		// OpenGL
		//////////////////////////////////////////////

		static GLenum PixelFormatToGLEnum(GLenum format)
		{
			switch(format)
			{
			default:
			case PixelFormat::PIXEL_FORMAT_RGB:
				return GL_RGB;
			case PixelFormat::PIXEL_FORMAT_BGR:
				return GL_BGR;
			}
		}

		GLuint		m_texName, m_texTarget, m_texInternalFormat, m_texDataFormat, m_texDataType;
		GLuint		m_pboName[2], m_pboTarget;

		void		__CreateObjects__();
		void		__DestroyObjects__();
		void		__SetupObjects__(unsigned width, unsigned height);
		
		//////////////////////////////////////////////
		// Host
		//////////////////////////////////////////////

		unsigned	m_pboSwapIndex;
		unsigned	m_frameWidth, m_frameHeight, m_framePixelSize, m_frameBufferSize;
		void*		m_frameBufferData;

	public:

		VideoSurface(unsigned format, unsigned width, unsigned height);
		~VideoSurface();
		void		SetData(const void* data, unsigned size);
		unsigned	Width();
		unsigned	Height();
		unsigned	PixelSize();
		unsigned	BufferSize();

		//////////////////////////////////////////////
		// OpenGL
		//////////////////////////////////////////////

		GLuint		Output();
	};
}