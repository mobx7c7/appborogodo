#include "Bitmap.h"
#include <memory>

Bitmap::Bitmap()
	: m_width(0)
	, m_height(0)
	, m_pixelFormat(0)
	, m_bufferSize(0)
	, m_bufferData(0)
{
	// Vazio!
}

Bitmap::Bitmap(const Bitmap& bitmap)
	: m_width(0)
	, m_height(0)
	, m_pixelFormat(0)
	, m_bufferSize(0)
	, m_bufferData(0)
{
	SetBitmap(bitmap.m_pixelFormat, bitmap.m_width, bitmap.m_height, bitmap.m_bufferSize, bitmap.m_bufferData);
}

Bitmap::Bitmap(unsigned format, unsigned width, unsigned height, unsigned size, const void* data)
	: m_width(0)
	, m_height(0)
	, m_pixelFormat(0)
	, m_bufferSize(0)
	, m_bufferData(0)
{
	SetBitmap(format, width, height, size, data);
}

Bitmap::~Bitmap()
{
	if(m_bufferData && m_bufferSize > 0)
	{
		delete m_bufferData;
	}
}

void Bitmap::SetBitmap(unsigned format, unsigned width, unsigned height, unsigned size, const void* data)
{
	if(m_bufferData && m_bufferSize > 0)
	{
		delete m_bufferData;
		m_width	=
		m_height = 
		m_pixelFormat =
		m_bufferSize = 0;
		m_bufferData = nullptr;
	}

	if(data && size > 0)
	{
		m_width			= width;
		m_height		= height;
		m_pixelFormat	= format;
		m_bufferSize	= size;
		m_bufferData	= new unsigned char[size];
		memcpy(m_bufferData, data, size);
	}
}

unsigned Bitmap::SetData(const void* data, unsigned size)
{
	if(m_bufferData && m_bufferSize > 0)
	{
		unsigned _size = m_bufferSize;//size > m_bufferSize ? m_bufferSize : size;
		memcpy(m_bufferData, data, _size);
		return _size;
	}
	return 0;
}

unsigned Bitmap::GetData(void* data, unsigned size)
{
	if(m_bufferData && m_bufferSize > 0)
	{
		unsigned _size = m_bufferSize;//size > m_bufferSize ? m_bufferSize : size;
		memcpy(data, m_bufferData, _size);
		return _size;
	}
	return 0;
}

unsigned Bitmap::Width()
{
	return m_width;
}

unsigned Bitmap::Height()
{
	return m_height;
}

unsigned Bitmap::Format()
{
	return m_pixelFormat;
}

unsigned Bitmap::Size()
{
	return m_bufferSize;
}