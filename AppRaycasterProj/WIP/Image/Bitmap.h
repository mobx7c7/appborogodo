#pragma once

enum BitmapFormat
{
	BITMAP_RGB,
	BITMAP_RGBA,
	BITMAP_BGR,
	BITMAP_BGRA,
	BITMAP_ARGB,
};

class Bitmap
{
protected:

	unsigned	m_width;
	unsigned	m_height;
	unsigned	m_pixelFormat;
	unsigned	m_bufferSize;
	unsigned char*	m_bufferData;

public:

	Bitmap();
	Bitmap(const Bitmap& bitmap);
	Bitmap(unsigned format, unsigned width, unsigned height, unsigned size, const void* data);
	~Bitmap();
	void		SetBitmap(unsigned format, unsigned width, unsigned height, unsigned size, const void* data);
	unsigned	SetData(const void* data, unsigned size);
	unsigned	GetData(void* data, unsigned size);
	unsigned	Width();
	unsigned	Height();
	unsigned	Format();
	unsigned	Size();

};
