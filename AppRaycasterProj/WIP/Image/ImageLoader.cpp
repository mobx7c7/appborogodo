#include "ImageLoader.h"
#include <memory>
#include <sstream>
#include <iostream>
#include <exception>
#include "FreeImage\Dist\x32\FreeImage.h"

void ImageLoader::__Initialize__()
{
	FreeImage_Initialise();
}

void ImageLoader::__Destroy__()
{
	FreeImage_DeInitialise();
}

ImageLoader::ImageLoader(void)
{
	__Initialize__();
}

ImageLoader::~ImageLoader(void)
{
	__Destroy__();
}

void ImageLoader::LoadFile(Bitmap& bmp, const char* file)
{
	FREE_IMAGE_FORMAT		fileFormat(FIF_UNKNOWN);
	FREE_IMAGE_COLOR_TYPE	colorType;
	FIBITMAP*				dib(0);
	BYTE*					bits(0);
	unsigned				width(0), height(0), size(0);

	fileFormat = FreeImage_GetFileType(file, 0);  // pega formato pela extens�o do arquivo.

	if(fileFormat == FIF_UNKNOWN) // se desconhecido, tentar advinhar o formato
	{
		fileFormat = FreeImage_GetFIFFromFilename(file);
		if(fileFormat == FIF_UNKNOWN) // se ainda desconhecido, erro!
		{
			//std::ostringstream oss;
			//oss << __FUNCTION__ << ": Impossivel abrir arquivo";
			//throw std::bad_exception(oss.str().c_str());
			throw std::bad_exception("N�o foi poss�vel abrir o arquivo");
		}
	}

	if(!FreeImage_FIFSupportsReading(fileFormat))
	{
		throw std::bad_exception("N�o h� suporte ao formato");
	}
	
	if(!(dib = FreeImage_Load(fileFormat, file)))
	{
		throw std::bad_exception("Erro de leitura");
	}

	bits		= FreeImage_GetBits(dib);
	width		= FreeImage_GetWidth(dib);
	height		= FreeImage_GetHeight(dib);
	colorType	= FreeImage_GetColorType(dib);
	size		= FreeImage_GetMemorySize(dib);

	if(!bits || !width || !height)
	{
		throw std::bad_exception("Erro desconhecido");
	}

	// Convers�es para OpenGL:
	// TODO: Convers�o BGR -> RGB
	FreeImage_FlipVertical(dib);

	switch(colorType)
	{
	default:
	case FIC_RGB:
		//bmp = Bitmap(BitmapFormat::BITMAP_RGB, width, height, size, bits); // T� quebrando o programa!
		bmp.SetBitmap(BitmapFormat::BITMAP_RGB, width, height, width*height*3, bits);
		break;
	case FIC_RGBALPHA:
		//bmp = Bitmap(BitmapFormat::BITMAP_RGBA, width, height, size, bits); // Ta quebrando o programa!
		bmp.SetBitmap(BitmapFormat::BITMAP_RGBA, width, height, width*height*4, bits);
		break;
	}

	FreeImage_Unload(dib);
}

ImageLoader& ImageLoader::Instance()
{
	static ImageLoader instance;
	return instance;
}