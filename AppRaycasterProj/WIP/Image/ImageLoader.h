#pragma once
#include "Bitmap.h"

class ImageLoader
{
private:

	void __Initialize__();
	void __Destroy__();
	//ImageLoader(const ImageLoader&);
	ImageLoader(void);
	~ImageLoader(void);

public:

	void ImageLoader::LoadFile(Bitmap& bmp, const char* file);
	static ImageLoader& Instance();

};