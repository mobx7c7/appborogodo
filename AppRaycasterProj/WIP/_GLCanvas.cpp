#include "_GLCanvas.h"
#include <iostream>
#include <memory>
#include <random> // textura com ruido

namespace GL
{
	Canvas::Canvas()
	{
		//////////////////////////////////////////////
		// Dados de entrada
		//////////////////////////////////////////////

		// TODO: Model loader
		static float quad_vert[] =
		{
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
		};

		static unsigned char quad_indx[] =
		{
			0, 1, 3, 1, 3, 2
		};

		// TODO: Shader loader
		static const char* quad_srcs[] =
		{
			// Vertex 
			"#version 400\n"
			"in vec2 v_coord;\n"
			"in vec2 t_coord;\n"
			"uniform mat4 m_mvp;\n"
			"out vec2 texCoord;\n"
			"void main(){\n"
			"gl_Position = m_mvp * vec4(v_coord, 0.0, 1.0);\n"
			"texCoord = t_coord;\n"
			"}",

			// Fragment
			"#version 400\n"
			"in vec2 texCoord;\n"
			"uniform vec4 color;\n"
			"uniform sampler2D tex0;\n"
			"uniform sampler2D tex1;\n"
			"uniform sampler2D tex2;\n"
			"uniform sampler2D tex3;\n"
			"out vec4 fragOut;\n"
			"void main(){\n"
			"vec2 k1 = texCoord * 4.0;\n"
			"fragOut += texture2D(tex0, k1-vec2(0.0, 0.0));\n"
			"fragOut += texture2D(tex1, k1-vec2(1.0, 0.0));\n"
			"fragOut += texture2D(tex2, k1-vec2(2.0, 0.0));\n"
			"fragOut += texture2D(tex3, k1-vec2(3.0, 0.0));\n"
			"}"
		};

		//////////////////////////////////////////////
		// Pr�-ajustes
		//////////////////////////////////////////////

		glEnable(GL_TEXTURE_2D);

		//////////////////////////////////////////////
		// Constru��o
		//////////////////////////////////////////////

		m_tex		= new GL::Texture*[4];
		std::fill(m_tex, m_tex+4, nullptr);

		m_fbo		= new GL::FBO*[4];
		std::fill(m_fbo, m_fbo+4, nullptr);

		m_rbo		= new GL::RBO*[4];
		std::fill(m_rbo, m_rbo+4, nullptr);

		m_tex[0]	= new GL::Texture(GL_TEXTURE_2D);
		m_tex[1]	= new GL::Texture(GL_TEXTURE_2D);
		m_tex[2]	= new GL::Texture(GL_TEXTURE_2D);
		m_tex[3]	= new GL::Texture(GL_TEXTURE_2D);
		m_vbo		= new GL::Buffer(GL_ARRAY_BUFFER);
		m_ibo		= new GL::Buffer(GL_ELEMENT_ARRAY_BUFFER);
		m_prg		= new GL::Program();
		m_vao		= new GL::VAO();

		//////////////////////////////////////////////
		// Ajustes
		//////////////////////////////////////////////
		{
			std::cout << __FUNCTION__ << ": " << "Gerando imagem..." << std::endl;
			std::random_device rd;
			std::default_random_engine re(rd());
			std::uniform_real_distribution<float> rnd(0.0f, 1.0f);
			const unsigned	img_width	= 128;
			const unsigned	img_height	= 128;
			const unsigned	img_size	= img_width*img_height*4;
			float*			img_data	= new float[img_size];
			for(int i=0; i<img_size; i++)
				img_data[i] = i%3 == 3 ? 1.0f : rnd(rd);

			std::cout << __FUNCTION__ << ": " << "Ajustando texturas..." << std::endl;
			TextureParams texParams;
			memset(&texParams, 0, sizeof texParams);
			texParams.minFilter			= GL_LINEAR;
			texParams.magFilter			= GL_LINEAR;
			texParams.wrapS				= GL_CLAMP_TO_BORDER;
			texParams.wrapT				= GL_CLAMP_TO_BORDER;
			texParams.internalFormat	= GL_RGBA;
			texParams.dataFormat		= GL_RGBA;
			texParams.dataType			= GL_FLOAT;

			texParams.width				= img_width;
			texParams.height			= img_height;
			texParams.data				= img_data;
			m_tex[0]->Initialize(texParams);

			texParams.width				= img_width;
			texParams.height			= img_height;
			texParams.data				= img_data;
			m_tex[1]->Initialize(texParams);

			texParams.width				= img_width;
			texParams.height			= img_height;
			texParams.data				= img_data;
			m_tex[2]->Initialize(texParams);

			texParams.width				= img_width;
			texParams.height			= img_height;
			texParams.data				= img_data;
			m_tex[3]->Initialize(texParams);


			delete[] img_data;

		}
		//////////////////////////////////////////////
		{
			_fbo = new GLuint[4];

		}
		//////////////////////////////////////////////
		{
			std::cout << __FUNCTION__ << ": " << "Ajustando buffers..." << std::endl;
			BufferParams bufParams;
			memset(&bufParams, 0, sizeof bufParams);
			bufParams.usage		= GL_STATIC_DRAW;
			bufParams.size		= sizeof(float)*8;
			bufParams.data		= quad_vert;
			m_vbo->Initialize(bufParams);
			bufParams.size		= sizeof(unsigned char)*6;
			bufParams.data		= quad_indx;
			m_ibo->Initialize(bufParams);
		}
		//////////////////////////////////////////////
		{
			std::cout << __FUNCTION__ << ": " << "Ajustando programas..." << std::endl;
			m_prg->AttachShader(GL::Shader(quad_srcs[0], GL_VERTEX_SHADER));
			m_prg->AttachShader(GL::Shader(quad_srcs[1], GL_FRAGMENT_SHADER));
			if(!m_prg->Link())
			{
				std::cout << m_prg->Log() << std::endl;
			}
		}
		//////////////////////////////////////////////
		// VAO
		//////////////////////////////////////////////

		std::cout << __FUNCTION__ << ": " << "Ajustando VAOs..." << std::endl;
		GLuint loc_v_coord = m_prg->AtributeLocation("v_coord");
		GLuint loc_t_coord = m_prg->AtributeLocation("t_coord");

		m_vao->Bind();
		m_vbo->Bind();
		m_vao->SetVertexAttribState(loc_v_coord, true);
		m_vao->SetVertexAttribPointer(loc_v_coord, 2, GL_FLOAT, false, sizeof(float)*2, (void*)0);
		m_vao->SetVertexAttribState(loc_t_coord, true);
		m_vao->SetVertexAttribPointer(loc_t_coord, 2, GL_FLOAT, false, sizeof(float)*2, (void*)0);
		m_ibo->Bind();
		m_vao->Unbind();

		std::cout << __FUNCTION__ << ": " << "Tudo pronto!" << std::endl;
	}

	Canvas::~Canvas()
	{
		delete m_vao;
		delete m_vbo;
		delete m_ibo;
		delete m_prg;

		GL::Texture** cur = m_tex;
		GL::Texture** end = m_tex + 4;
		while(cur < end)
		{
			if(*cur)
			{
				delete *cur;
				*cur = nullptr;
			}
			cur++;
		}
		delete[] m_tex;
	}

	void Canvas::Render()
	{
		int i;
		// TODO: uniform & attribute buffer opengl

		GLuint loc_color	= m_prg->UniformLocation("color");
		GLuint loc_m_mvp	= m_prg->UniformLocation("m_mvp");
		GLuint loc_tex0		= m_prg->UniformLocation("tex0");
		GLuint loc_tex1		= m_prg->UniformLocation("tex1");

		for(i=0; i<4; i++)
		{
			GL::Texture* tex = m_tex[i];
			if(tex)
			{
				glActiveTexture(GL_TEXTURE0+i);
				tex->Bind();
			}
		}

		m_vao->Bind();
		m_prg->Bind();
		m_prg->Uniform1i(loc_tex0, 0);
		m_prg->Uniform1i(loc_tex1, 1);
		m_prg->Uniform4f(loc_color, 0.0f, 1.0f, 1.0f, 1.0f);
		m_prg->UniformMatrix4f(loc_m_mvp, 1, false, &m_mvp[0][0]);
		//glBindFragDataLocation(m_prg->GetName(), m_prg->FragDataLocation("fragOut"), "fragOut");
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0); 
		// glDrawArrays(GL_QUADS, 0, 8);
		m_prg->Unbind();
		m_vao->Unbind();

		for(i=0; i<4; i++)
		{
			GL::Texture* tex = m_tex[i];
			if(tex)
			{
				glActiveTexture(GL_TEXTURE0+i);
				tex->Unbind();
			}
		}
	}

	void Canvas::OnWindowResize(int w, int h)
	{
		float width = (float)w;
		float height = (float)h;
		//std::cout << __FUNCTION__ << ": " << width << "x" << height << std::endl;
		m_mvp = glm::ortho(0.0f, width, height, 0.0f);
		m_mvp = glm::scale(m_mvp, glm::vec3(width, height, 1.0f));
		//m_mvp = glm::perspective(60.0f, 1.0f, 0.001f, 100.0f);
		//m_mvp = glm::translate(m_mvp, glm::vec3(0.0f, 0.0f, -0.5f));
	}

}