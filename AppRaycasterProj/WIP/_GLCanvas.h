#pragma once

#include "GL\Buffer.h"
#include "GL\Shader.h"
#include "GL\Program.h"
#include "GL\Texture.h"
#include "GL\VAO.h"
#include "GL\FBO.h"
#include "GL\RBO.h"

#define GLM_FORCE_RADIANS
//#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
//#include "glm\gtc\type_ptr.hpp"

namespace GL
{
	class Canvas
	{
	protected:

		GL::VAO		*m_vao;
		GL::Buffer	*m_vbo, *m_ibo;
		GL::Program	*m_prg;
		GL::Texture **m_tex;
		GL::FBO		**m_fbo;
		GL::RBO		**m_rbo;
		glm::mat4	m_mvp;

		GLuint *_fbo, *_tex;

	public:
	
		Canvas();
		~Canvas();
		void Render();
		void OnWindowResize(int w, int h);

	};

}
