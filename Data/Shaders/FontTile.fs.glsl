#version 400

in vec2				m_texCoord;
uniform sampler2D	atlasTex;
out vec4			fragOut;

void main()
{
	vec3 color = texture(atlasTex, m_texCoord).rgb;
	float luma = (color.r + color.g + color.b) / 3.0;
	fragOut = texture(atlasTex, m_texCoord);
	fragOut.a = luma;
};