#version 400
layout(points) in;
layout(triangle_strip, max_vertices = 6) out;

in int			m_tileIndex[1];
in int			m_tileChar[1];
in ivec2		m_tileCount[1];
in ivec2		m_tileSize[1];
in ivec2		m_atlasSize[1];
in ivec2		m_screenSize[1];
uniform mat4	m_mvp;
out vec2		m_texCoord;

void main()
{
	
	int		charIndex		= clamp(m_tileChar[0], 32, 255)-32;
	ivec2	tileCoord		= ivec2(charIndex % m_tileCount[0].x, charIndex / m_tileCount[0].x);

	// VALORES NORMALIZADOS
	vec2	tileCountUV		= 1.0 / m_tileCount[0];
	//vec2	tileSizeUV		= 1.0 / m_tileSize[0];
	//vec2	atlasSizeUV		= 1.0 / m_atlasSize[0];
	//vec2	screenSizeUV	= 1.0 / m_screenSize[0];

	// ALINHAMENTO DA TEXTURA
	vec2 st0 = tileCountUV * tileCoord;
	vec2 st1 = tileCountUV + st0;

	// TRANSFORMACAO TILE
	mat4 m1 = mat4(1);
	vec2 tileScreenAspectUV = 1.0/(vec2(m_screenSize[0])/vec2(m_tileSize[0])); // aspecto normalizado do tile na janela
	m1[0][0] = tileScreenAspectUV.x;
	m1[1][1] = tileScreenAspectUV.y;
	m1[3][0] = m1[0][0] * m_tileIndex[0];
	m1[3][1] = 0;

	// TRANSFORMACAO JANELA
	mat4 m2 = m_mvp * m1;

	// NOVOS VERTICES
	vec2 k = vec2(0.0, 1.0);
	m_texCoord = vec2(st0.x, st0.y); gl_Position = m2 * k.xxxy; EmitVertex();
	m_texCoord = vec2(st1.x, st0.y); gl_Position = m2 * k.yxxy; EmitVertex();
	m_texCoord = vec2(st0.x, st1.y); gl_Position = m2 * k.xyxy; EmitVertex();
	m_texCoord = vec2(st0.x, st1.y); gl_Position = m2 * k.xyxy; EmitVertex();
	m_texCoord = vec2(st1.x, st0.y); gl_Position = m2 * k.yxxy; EmitVertex();
	m_texCoord = vec2(st1.x, st1.y); gl_Position = m2 * k.yyxy; EmitVertex();
	EndPrimitive();
};