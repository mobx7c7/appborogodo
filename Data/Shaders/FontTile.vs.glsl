#version 400

in int			tileChar;
uniform ivec2	tileCount;
uniform ivec2	tileSize;
uniform ivec2	atlasSize;
uniform ivec2	screenSize;

out int			m_tileIndex;
out int			m_tileChar;
out ivec2		m_tileCount;
out ivec2		m_tileSize;
out ivec2		m_atlasSize;
out ivec2		m_screenSize;

void main()
{
	m_tileIndex		= gl_VertexID;
	m_tileChar		= tileChar;
	m_tileCount		= tileCount;
	m_tileSize		= tileSize;
	m_atlasSize		= atlasSize;
	m_screenSize	= screenSize;
}